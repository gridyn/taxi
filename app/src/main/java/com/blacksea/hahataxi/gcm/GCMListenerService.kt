package com.blacksea.hahataxi.gcm

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v4.content.LocalBroadcastManager
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.TaxiApplication
import com.blacksea.hahataxi.activities.chat.ChatActivity
import com.blacksea.hahataxi.activities.voip_call.VoipCallActivity
import com.blacksea.hahataxi.models.CallAction
import com.blacksea.hahataxi.models.CallType
import com.blacksea.hahataxi.models.Notification
import com.blacksea.hahataxi.util.PreferencesHelper
import com.blacksea.hahataxi.util.TWILIO_VIDEO_UNIQUE
import com.google.android.gms.gcm.GcmListenerService
import javax.inject.Inject

class GcmListenerService : GcmListenerService() {

    /* Example Bundle
    {
        channel_id=CH427099b81fee44a48f2679bb4f681d4d,
        message_id=IM6439d22afed746a6972525b6d67f69bf,
        google.sent_time=1489588795996,
        author=andriicsharp,
        message_sid=IM6439d22afed746a6972525b6d67f69bf,
        twi_message_type=twilio.channel.new_message,
        channel_sid=CH427099b81fee44a48f2679bb4f681d4d,
        google.message_id=0:1489588796005950%af3519e1f9fd7ecd,
        twi_message_id=GCM591822af51d941a0a14c9e22dac18f59,
        twi_body=message,
        channel_title=rpugaaa.andriicsharp
     }
     */

    private val TWILIO_MESSAGE = "twi_body"
    private val TWILIO_AUTHOR = "author"
    private val TWILIO_CHANNEL_ID = "channel_id"
    private val TWILIO_CHANNEL_TITLE = "channel_title"

    companion object {
        const val EVENT_CALL_STATE = "com.blacksea.hahataxi.gcm.event_call_state"
        const val EXTRA_CALL_STATE = "extra_call_state"
    }

    @Inject lateinit var preferences: PreferencesHelper
    @Inject lateinit var localBroadcastManager: LocalBroadcastManager

    override fun onCreate() {
        super.onCreate()
        TaxiApplication[this].appComponent.inject(this)
    }

    override fun onMessageReceived(from: String?, data: Bundle?) {
        super.onMessageReceived(from, data)
        showNotificationNewMessage(Notification(data!!.getString(TWILIO_CHANNEL_ID), data.getString(TWILIO_AUTHOR),
                data.getString(TWILIO_MESSAGE), data.getString(TWILIO_CHANNEL_TITLE), true))
    }

    private fun showNotificationNewMessage(notification: Notification) {
        if (notification.channelTitle.split(".")[0] == TWILIO_VIDEO_UNIQUE) {
            if (!preferences.isStartedCall && notification.message == CallAction.CALL.name) {
                startActivity(Intent(this, VoipCallActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra(VoipCallActivity.EXTRA_CALL_TYPE, CallType.INCOMING)
                        .putExtra(VoipCallActivity.EXTRA_USER_NAME, notification.author)
                        .putExtra(VoipCallActivity.EXTRA_CHANNEL_NAME, notification.channelTitle))
            } else {
                localBroadcastManager.sendBroadcast(Intent(EVENT_CALL_STATE).putExtra(EXTRA_CALL_STATE, notification.message))
                if (notification.message == CallAction.NOT_WAIT.name) {
                    showNotificationMissedCall(notification)
                }
            }
        } else if (!preferences.isStartedChat) {
            preferences.lastNotification = notification
            val intent = Intent(this, ChatActivity::class.java)
                    .putExtra(ChatActivity.EXTRA_NAME, notification.author)
                    .putExtra(ChatActivity.EXTRA_CHANNEL_ID, notification.channelId)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

            val notificationCompat = NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.marker_taxi)
                    .setContentTitle(notification.author)
                    .setContentText(notification.message)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .build()

            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(0, notificationCompat)
        }
    }

    private fun showNotificationMissedCall(notification: Notification) {
        val callIntent = Intent(this, VoipCallActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra(VoipCallActivity.EXTRA_CALL_TYPE, CallType.OUTGOING)
                .putExtra(VoipCallActivity.EXTRA_USER_NAME, notification.author)
                .putExtra(VoipCallActivity.EXTRA_CHANNEL_NAME, notification.channelTitle)

        val cancelIntent = Intent(this, CancelNotificationBroadcast::class.java)

        val callPendingIntent = PendingIntent.getActivity(this, 0, callIntent, PendingIntent.FLAG_ONE_SHOT)
        val cancelPendingIntent = PendingIntent.getBroadcast(this, 0, cancelIntent.setAction(CancelNotificationBroadcast.ACTION_CANCEL_NOTIFICATION), PendingIntent.FLAG_ONE_SHOT)

        val notificationCompat = NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.marker_taxi)
                .setContentTitle(notification.author)
                .setContentText(getString(R.string.notification_missed_call))
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .addAction(R.drawable.ic_call, getString(R.string.notification_call), callPendingIntent)
                .addAction(R.drawable.ic_clear_white_24dp, getString(R.string.notification_cancel), cancelPendingIntent)
                .build()

        (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(0, notificationCompat)
    }
}
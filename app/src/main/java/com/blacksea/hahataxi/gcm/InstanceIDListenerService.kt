package com.blacksea.hahataxi.gcm

import android.content.Intent
import com.google.android.gms.iid.InstanceIDListenerService


class InstanceIDListenerService : InstanceIDListenerService() {

    override fun onTokenRefresh() {
        startService(Intent(this, RegistrationIntentService::class.java))
    }
}
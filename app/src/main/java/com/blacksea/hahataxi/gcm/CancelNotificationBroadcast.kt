package com.blacksea.hahataxi.gcm

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class CancelNotificationBroadcast : BroadcastReceiver() {

    companion object {
        const val ACTION_CANCEL_NOTIFICATION = "com.blacksea.hahataxi.CANCEL_NOTIFICATION"
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        (context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).cancel(0)
        context.sendBroadcast(Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS))
    }
}
package com.blacksea.hahataxi.gcm

import android.app.IntentService
import android.content.Intent
import android.util.Log
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.TaxiApplication
import com.blacksea.hahataxi.twilio.ChatClientManager
import com.google.android.gms.gcm.GoogleCloudMessaging
import com.google.android.gms.iid.InstanceID
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class RegistrationIntentService(TAG: String = "RegIntentService") : IntentService(TAG) {

    @Inject lateinit var chatClientManager: ChatClientManager

    override fun onCreate() {
        super.onCreate()
        TaxiApplication[this].appComponent.inject(this)
    }

    override fun onHandleIntent(intent: Intent?) {
        val instanceID = InstanceID.getInstance(this)
        val token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                GoogleCloudMessaging.INSTANCE_ID_SCOPE, null)

        sendRegistrationToServer(token)
    }

    private fun sendRegistrationToServer(token: String) {
        Log.i("log", "sendRegToServer $token")
        Observable.just(chatClientManager.setupGcmToken(token))
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
    }
}
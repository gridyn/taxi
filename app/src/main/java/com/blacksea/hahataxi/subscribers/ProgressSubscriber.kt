package com.blacksea.hahataxi.subscribers

import com.blacksea.hahataxi.ProgressView
import rx.subscriptions.Subscriptions

abstract class ProgressSubscriber<T>(private val progressView: ProgressView, private val dialogMessage: String? = null) : ErrorSubscriber<T>(progressView) {

    init {
        this.add(Subscriptions.create({ progressView.hideRefreshing() })) //on unsubscribe
    }

    override fun onStart() {
        super.onStart()
        if(dialogMessage != null) {
            progressView.showRefreshing(dialogMessage)
        } else {
            progressView.showRefreshing()
        }
    }

    override fun onCompleted() { }
}
package com.blacksea.hahataxi.subscribers

import android.util.Log
import com.blacksea.hahataxi.BaseView
import com.blacksea.hahataxi.R
import org.json.JSONObject
import retrofit2.adapter.rxjava.HttpException
import rx.Subscriber
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

abstract class ErrorSubscriber<T>(private val baseView: BaseView) : Subscriber<T>() {

    override fun onError(e: Throwable?) {
        if(e is ConnectException || e is SocketTimeoutException || e is UnknownHostException) {
            baseView.showMessage(R.string.error_network)
        } else if (e.toString().contains("400")) {
            baseView.showMessage(JSONObject((e as HttpException).response().errorBody().string()).getString("error_description"))
            baseView.startActivityAfterTokenExpiration()
        } else {
            baseView.showMessage(R.string.error_unknown)
        }
        Log.i("logger", e.toString())
    }
}
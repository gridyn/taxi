package com.blacksea.hahataxi.util

import com.blacksea.hahataxi.models.Driver
import com.blacksea.hahataxi.models.MoveEvent
import com.blacksea.hahataxi.models.Passenger
import com.google.gson.JsonObject
import rx.subjects.PublishSubject

class SignalRBus {

    val matchedDrivers = mutableListOf<Driver>()
    val matchedPassengers = mutableListOf<Passenger>()

    val matchedDriverBus: PublishSubject<Driver> = PublishSubject.create()
    val unmatchedDriverBus: PublishSubject<String> = PublishSubject.create()
    val driverDisconnectedBus: PublishSubject<String> = PublishSubject.create()
    val driverReconnectedBus: PublishSubject<JsonObject> = PublishSubject.create()
    val driverMovedBus: PublishSubject<MoveEvent> = PublishSubject.create()

    val matchedPassengerBus: PublishSubject<Passenger> = PublishSubject.create()
    val unmatchedPassengerBus: PublishSubject<String> = PublishSubject.create()
    val passengerDisconnectedBus: PublishSubject<String> = PublishSubject.create()
    val passengerReconnectedBus: PublishSubject<JsonObject> = PublishSubject.create()
    val passengerMovedBus: PublishSubject<MoveEvent> = PublishSubject.create()

    val signalRStoppedBus: PublishSubject<Void> = PublishSubject.create()

    fun sendMatchedDriver(driver: Driver) {
        matchedDriverBus.onNext(driver)
        matchedDrivers.add(driver)
    }

    fun sendUnmatchedDriver(driverId: String) {
        unmatchedDriverBus.onNext(driverId)
        matchedDrivers.removeAll { it.id == driverId }
    }

    fun sendDisconnectedDriver(driverId: String) {
        driverDisconnectedBus.onNext(driverId)
    }

    fun sendReconnectedDriver(driver: JsonObject) {
        driverReconnectedBus.onNext(driver)
    }

    fun sendMovedDriver(event: MoveEvent) {
        driverMovedBus.onNext(event)
        val driver = matchedDrivers.filter { it.id == event.id }.first()
        matchedDrivers.remove(driver)
        matchedDrivers.add(driver.copy(location = event.location, distanceInMeters = event.distanceInMeters))
    }

    //Passengers

    fun sendMatchedPassenger(passenger: Passenger) {
        matchedPassengerBus.onNext(passenger)
        matchedPassengers.add(passenger)
    }

    fun sendUnmatchedPassenger(passengerId: String) {
        unmatchedPassengerBus.onNext(passengerId)
        matchedPassengers.removeAll { it.id == passengerId }
    }

    fun sendDisconnectedPassenger(passengerId: String) {
        passengerDisconnectedBus.onNext(passengerId)
    }

    fun sendReconnectedPassenger(passenger: JsonObject) {
        passengerReconnectedBus.onNext(passenger)
    }

    fun sendMovedPassenger(event: MoveEvent) {
        passengerMovedBus.onNext(event)
        val passenger = matchedPassengers.filter { it.id == event.id }.first()
        matchedPassengers.remove(passenger)
        matchedPassengers.add(passenger.copy(location = event.location, distanceInMeters = event.distanceInMeters))
    }

    fun sendUnmatchedAllPassenger() {
        matchedPassengers.forEach { unmatchedPassengerBus.onNext(it.id) }
        matchedPassengers.removeAll { true }
    }
}
package com.blacksea.hahataxi.util

import com.blacksea.hahataxi.R

object ValidationUtil {

    //Minimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character
    //private val regExpPassword = Regex("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@!%*?&-_])[A-Za-z\\d$@!%*?&-_]{8,}")
    private val regExpEmail = Regex("\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,7}\\b")
    private val regExpPhone = Regex("\\+(9[976]\\d|8[987530]\\d|6[987]\\d|5[90]\\d|42\\d|3[875]\\d|2[98654321]\\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\\d{8,14}$")

    //fun isValidPassword(password: CharSequence) = regExpPassword.matches(password)

    fun isValidEmail(email: CharSequence) = regExpEmail.matches(email)

    fun isValidPhone(phone: CharSequence) = regExpPhone.matches(phone)

    fun isContainDigit(charSequence: CharSequence) = charSequence.any { isDigit(it) }

    fun isContainUpperCase(charSequence: CharSequence) = charSequence.any(Char::isUpperCase)

    fun isContainLowerCase(charSequence: CharSequence) = charSequence.any(Char::isLowerCase)

    fun isContainNonAlphanumeric(charSequence: CharSequence) = charSequence.any { !isLetterOrDigit(it) }

    fun isDigit(char: Char) =  char >= '0' && char <= '9'

    fun isLetterOrDigit(char: Char) = isDigit(char) || char.isLowerCase() || char.isUpperCase()

    fun isPasswordNotTooShort(charSequence: CharSequence) = charSequence.length >= PasswordValidator.REQUIRED_LENGTH

    fun  isValidPassword(password: String, stringProvider: IStringProvider) : String? {

        if (!ValidationUtil.isPasswordNotTooShort(password)) {
            return stringProvider.getStringRes(R.string.password_too_short)
        }

        val messageBuilder = StringBuilder()
        if (PasswordValidator.REQUIRE_DIGIT && !ValidationUtil.isContainDigit(password)) {
            messageBuilder.append(stringProvider.getStringRes(R.string.password_must_contain_digits))
        }
        if (PasswordValidator.REQUIRE_UPPERCASE && !ValidationUtil.isContainUpperCase(password)) {
            addComaIfNeeded(messageBuilder, stringProvider)
            messageBuilder.append(stringProvider.getStringRes(R.string.password_must_contain_upper_case))
        }
        if (PasswordValidator.REQUIRE_LOWERCASE && !ValidationUtil.isContainLowerCase(password)) {
            addComaIfNeeded(messageBuilder, stringProvider)
            messageBuilder.append(stringProvider.getStringRes(R.string.password_must_contain_lower_case))
        }
        if (PasswordValidator.REQUIRE_NON_ALPHANUMERIC && !ValidationUtil.isContainNonAlphanumeric(password)) {
            addComaIfNeeded(messageBuilder, stringProvider)
            messageBuilder.append(stringProvider.getStringRes(R.string.password_must_contain_non_alphanumeric))
        }
        if (!messageBuilder.isEmpty()) {
            return String.format(stringProvider.getStringRes(R.string.password_must_contain_template), messageBuilder.toString())
        }
        return null
    }

    fun addComaIfNeeded(messageBuilder: StringBuilder, stringProvider: IStringProvider) {
        if (!messageBuilder.isEmpty()) {
            messageBuilder.append(stringProvider.getStringRes(R.string.coma))
        }
    }
}

package com.blacksea.hahataxi.util

interface IStringProvider {
    fun getStringRes(resId: Int) : String
}
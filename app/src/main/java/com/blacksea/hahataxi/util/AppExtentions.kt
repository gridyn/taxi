package com.blacksea.hahataxi.util

import android.content.Context
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import java.text.SimpleDateFormat
import java.util.*

val TWILIO_VIDEO_UNIQUE = "vVideOcHanNneLlL3255qejdszd"

fun getDeviceId(context: Context): String = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)

fun String.getTimeFromTwilio(): Date {
    return if (this.isNotEmpty())
        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(this)
    else
        Date()
}

fun EditText.watchText(body: (text: String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            body.invoke(s.toString())
        }
    })
}
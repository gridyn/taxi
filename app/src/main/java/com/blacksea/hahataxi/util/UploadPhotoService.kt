package com.blacksea.hahataxi.util

import android.app.Service
import android.content.Intent
import android.net.Uri
import android.os.IBinder
import com.blacksea.hahataxi.TaxiApplication
import com.blacksea.hahataxi.rest.ProfileApi
import com.blacksea.hahataxi.rest.ServerConstants
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import rx.android.schedulers.AndroidSchedulers
import java.io.File
import javax.inject.Inject

class UploadPhotoService : Service() {

    @Inject lateinit var profileApi: ProfileApi
    @Inject lateinit var preferences: PreferencesHelper

    override fun onCreate() {
        super.onCreate()
        TaxiApplication[this].appComponent.inject(this)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null && intent.hasExtra(EXTRA_URI)) {
            profileApi.postAccountPicture(getPhotoPart(intent.getParcelableExtra<Uri>(EXTRA_URI)))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        updateToken(it.headers().get("Location").split("/").last())
                        stopSelf()
                    }, {
                        stopSelf()
                    })
        } else stopSelf()


        return START_NOT_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    private fun getPhotoPart(uri: Uri): MultipartBody.Part {
        val file = File(uri.path)
        return MultipartBody.Part.createFormData("file", file.name, RequestBody.create(MediaType.parse("image/*"), file))
    }

    private fun updateToken(picture: String) {
        val newToken = preferences.jwtToken
        newToken?.changeValueInIdToken(ServerConstants.KEY_PICTURE, picture)
        preferences.jwtToken = newToken
    }

    companion object {
        const val EXTRA_URI = "EXTRA_URI"
    }
}
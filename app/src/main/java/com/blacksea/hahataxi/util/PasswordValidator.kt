package com.blacksea.hahataxi.util

import android.content.res.Resources
import com.blacksea.hahataxi.R

object PasswordValidator {
    const val REQUIRED_LENGTH = 8

    const val REQUIRE_DIGIT = true

    const val REQUIRE_NON_ALPHANUMERIC = false

    const val REQUIRE_LOWERCASE = true

    const val REQUIRE_UPPERCASE = false

    fun checkPassword(resources: Resources, messageBuilder: StringBuilder, password: String?) {

        if (password == null || password.isBlank()) {
            messageBuilder.append(getStringRes(R.string.blank_password, resources))
            return
        }

        if (!ValidationUtil.isPasswordNotTooShort(password)) {
            messageBuilder.append(getStringRes(R.string.password_too_short, resources))
            return
        }

        if (PasswordValidator.REQUIRE_DIGIT && !ValidationUtil.isContainDigit(password)) {
            messageBuilder.append(getStringRes(R.string.password_must_contain_digits, resources)).append(" ")
        }
        if (PasswordValidator.REQUIRE_UPPERCASE && !ValidationUtil.isContainUpperCase(password)) {
            addComaIfNeeded(messageBuilder, resources)
            messageBuilder.append(getStringRes(R.string.password_must_contain_upper_case, resources)).append(" ")
        }
        if (PasswordValidator.REQUIRE_LOWERCASE && !ValidationUtil.isContainLowerCase(password)) {
            addComaIfNeeded(messageBuilder, resources)
            messageBuilder.append(getStringRes(R.string.password_must_contain_lower_case, resources)).append(" ")
        }
        if (PasswordValidator.REQUIRE_NON_ALPHANUMERIC && !ValidationUtil.isContainNonAlphanumeric(password)) {
            addComaIfNeeded(messageBuilder, resources)
            messageBuilder.append(getStringRes(R.string.password_must_contain_non_alphanumeric, resources)).append(" ")
        }

        if (!messageBuilder.isEmpty()) {
            val msg = String.format(resources.getString(R.string.password_must_contain_template), messageBuilder.toString())
            messageBuilder.delete(0, messageBuilder.length)
            messageBuilder.append(msg)
        }
    }

    private fun addComaIfNeeded(messageBuilder: StringBuilder, resources: Resources) {
        if (!messageBuilder.isEmpty()) {
            messageBuilder.append(getStringRes(R.string.coma, resources))
        }
    }

    fun getStringRes(resId: Int, resources: Resources): String = resources.getString(resId)

}
package com.blacksea.hahataxi.util

import android.content.SharedPreferences
import com.blacksea.hahataxi.models.JsonWebToken
import com.blacksea.hahataxi.models.Notification
import com.google.gson.Gson

class PreferencesHelper(private val preferences: SharedPreferences, private val gson: Gson) {

    private val KEY_LOGIN = "login"
    private val KEY_JWT = "jwt"
    private val KEY_TWILIO = "tw_access_token"
    private val KEY_STARTED_CHAT = "is_started_chat"
    private val KEY_STARTED_CALL = "is_started_call"
    private val KEY_LAST_NOTIFICATION = "last_notification"
    private val KEY_SEARCH_TERM = "searchTerm"
    private val IS_MAIL_CONFIRMED = "IS_MAIL_CONFIRMED"
    private val IS_WAITING_FOR_MAIL_CONFIRMATION_CODE = "IS_WAITING_FOR_MAIL_CONFIRMATION_CODE"
    private val REFRESH_TOKEN_EXPIRE_TIME_MILLIS = "REFRESH_TOKEN_EXPIRE_TIME_MILLIS"

    var jwtToken: JsonWebToken?
        get() {
            return if (preferences.contains(KEY_JWT)) gson.fromJson(preferences.getString(KEY_JWT, null), JsonWebToken::class.java) else null
        }
        set(value) {
            preferences.edit().putString(KEY_JWT, gson.toJson(value)).apply()
        }

    var login: String?
        get() = preferences.getString(KEY_LOGIN, null)
        set(value) {
            preferences.edit()
                    .putString(KEY_LOGIN, value)
                    .apply()
        }

    var twilioAccessToken: String?
        get() {
            return if (preferences.contains(KEY_TWILIO)) preferences.getString(KEY_TWILIO, null) else null
        }
        set(value) {
            preferences.edit()
                    .putString(KEY_TWILIO, value)
                    .apply()
        }

    var isStartedChat: Boolean
        get() = preferences.getBoolean(KEY_STARTED_CHAT, false)
        set(value) {
            preferences.edit().putBoolean(KEY_STARTED_CHAT, value).apply()
        }

    var isStartedCall: Boolean
        get() = preferences.getBoolean(KEY_STARTED_CALL, false)
        set(value) {
            preferences.edit().putBoolean(KEY_STARTED_CALL, value).apply()
        }

    var lastNotification: Notification?
        get() = if (preferences.contains(KEY_LAST_NOTIFICATION))
            gson.fromJson(preferences.getString(KEY_LAST_NOTIFICATION, null), Notification::class.java) else null
        set(value) {
            preferences.edit().putString(KEY_LAST_NOTIFICATION, gson.toJson(value)).apply()
        }

    var searchTerm: String
        get() {
            return preferences.getString(KEY_SEARCH_TERM, "")
        }
        set(value) {
            preferences.edit()
                    .putString(KEY_SEARCH_TERM, value)
                    .commit()
        }

    var isMailConfirmed: Boolean
        get() {
            if (preferences.contains(IS_MAIL_CONFIRMED)) {
                return preferences.getBoolean(IS_MAIL_CONFIRMED, false)
            } else return false
        }
        set(value) {
            preferences.edit().putBoolean(IS_MAIL_CONFIRMED, value).apply()
        }

    var isWaitingForConformationCode: Boolean
        get() {
            if (preferences.contains(IS_WAITING_FOR_MAIL_CONFIRMATION_CODE)) {
                return preferences.getBoolean(IS_WAITING_FOR_MAIL_CONFIRMATION_CODE, false)
            } else return false
        }
        set(value) {
            preferences.edit().putBoolean(IS_WAITING_FOR_MAIL_CONFIRMATION_CODE, value).apply()
        }

    var accessTokenExpireTimeMillis: Long
        get() = preferences.getLong(REFRESH_TOKEN_EXPIRE_TIME_MILLIS, 0)
        set(value) = preferences.edit().putLong(REFRESH_TOKEN_EXPIRE_TIME_MILLIS, value).apply()
}
package com.blacksea.hahataxi.twilio.channels

import android.content.Context
import android.util.Log
import com.blacksea.hahataxi.twilio.ChatClientManager
import com.blacksea.hahataxi.twilio.listeners.LoadChannelListener
import com.twilio.chat.*
import java.util.*

class ChannelManager(val context: Context, val chatClientManager: ChatClientManager) : ChatClientListener {
    var generalChannel: Channel? = null
    var channels: MutableList<Channel> = ArrayList()
    var channelsObject: Channels? = null
    private var listener: ChatClientListener = this

    fun leaveChannelWithHandler(channel: Channel, handler: StatusListener) {
        channel.leave(handler)
    }

    fun deleteChannelWithHandler(channel: Channel, handler: StatusListener) {
        channel.destroy(handler)
    }

    fun initChannelsObject() {
        channelsObject = chatClientManager.getChatClient()?.channels
    }

    fun populateChannels(listener: LoadChannelListener) {

        if (channelsObject == null) {
            initChannelsObject()
        }

        channelsObject!!.getUserChannels(object : CallbackListener<Paginator<Channel>>() {
            override fun onSuccess(channelsPaginator: Paginator<Channel>) {
                channels.clear()
                channels.addAll(channelsPaginator.items)
                Collections.sort(channels, CustomChannelComparator())
                listener.onChannelsFinishedLoading(channelsPaginator.items)
            }

            override fun onError(errorInfo: ErrorInfo?) {
                Log.i("log", "ChannelManager populateChannel ${errorInfo!!.errorText}")
            }
        })
    }

    fun joinOrCreateGeneralChannelWithCompletion(userName: String, channelName: String, listener: StatusListener) {
        channelsObject!!.getChannel(channelName, object : CallbackListener<Channel>() {
            override fun onSuccess(channel: Channel?) {
                this@ChannelManager.generalChannel = channel
                joinGeneralChannelWithCompletion(listener)

            }

            override fun onError(errorInfo: ErrorInfo?) {
                listener.onError(errorInfo)
                createGeneralChannelWithCompletion(userName, channelName, listener)
                Log.i("log", "joinOrCreateGeneralChannelWithCompletion error: ${errorInfo!!.errorCode}")
            }
        })
    }

    private fun joinGeneralChannelWithCompletion(listener: StatusListener) {
        generalChannel!!.join(object : StatusListener() {
            override fun onSuccess() {
                listener.onSuccess()
                Log.i("log", "joinGeneralChannelWithCompletion success")
            }

            override fun onError(errorInfo: ErrorInfo?) {
                listener.onError(errorInfo)
                Log.i("log", "joinGeneralChannelWithCompletion error: ${errorInfo!!.errorText}")
            }
        })
    }

    private fun createGeneralChannelWithCompletion(userName: String, channelName: String, listener: StatusListener) {
        channelsObject!!
                .channelBuilder()
                .withUniqueName(channelName)
                .withType(Channel.ChannelType.PUBLIC)
                .build(object : CallbackListener<Channel>() {
                    override fun onSuccess(channel: Channel) {
                        channel.members.addByIdentity(userName, object : StatusListener() {
                            override fun onSuccess() {
                                Log.i("log", "$userName added to channel")
                            }

                            override fun onError(errorInfo: ErrorInfo?) {
                                Log.i("log", "$userName did not add to channel")
                            }
                        })

                        this@ChannelManager.generalChannel = channel
                        this@ChannelManager.channels.add(channel)
                        joinGeneralChannelWithCompletion(listener)
                        Log.i("log", "channel sid: ${channel.sid}")
                    }

                    override fun onError(errorInfo: ErrorInfo?) {
                        listener.onError(errorInfo)
                        Log.i("log", "createGeneralChannelWithCompletion error: ${errorInfo!!.errorText}")
                    }
                })


    }

    fun getChannel(channelSidOrUniqueName: String, answer: (channel: Channel?) -> Unit) {
        channelsObject?.getChannel(channelSidOrUniqueName, object : CallbackListener<Channel>() {
            override fun onSuccess(p0: Channel?) {
                answer.invoke(p0)
            }

            override fun onError(errorInfo: ErrorInfo?) {
                super.onError(errorInfo)
                Log.i("log", "ChannelManager connectChannel ${errorInfo?.errorText}")
            }
        })
    }

    fun setChannelListener(listener: ChatClientListener) {
        this.listener = listener
    }

    override fun onChannelAdd(channel: Channel) {
        listener.onChannelAdd(channel)
    }

    override fun onChannelInvite(channel: Channel) {

    }

    override fun onChannelChange(channel: Channel) {
        listener.onChannelChange(channel)

    }

    override fun onChannelDelete(channel: Channel) {
        listener.onChannelDelete(channel)
    }

    override fun onChannelSynchronizationChange(channel: Channel) {
        listener.onChannelSynchronizationChange(channel)
    }

    override fun onError(errorInfo: ErrorInfo) {
        listener.onError(errorInfo)
    }

    override fun onUserInfoChange(userInfo: UserInfo, updateReason: UserInfo.UpdateReason) {
        listener.onUserInfoChange(userInfo, updateReason)
    }

    override fun onClientSynchronization(synchronizationStatus: ChatClient.SynchronizationStatus) {

    }

    override fun onToastNotification(s: String, s1: String) {

    }

    override fun onToastSubscribed() {

    }

    override fun onToastFailed(errorInfo: ErrorInfo) {

    }

    override fun onConnectionStateChange(connectionState: ChatClient.ConnectionState) {

    }
}

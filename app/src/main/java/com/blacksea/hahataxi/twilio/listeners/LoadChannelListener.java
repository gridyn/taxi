package com.blacksea.hahataxi.twilio.listeners;

import com.twilio.chat.Channel;

import java.util.List;

public interface LoadChannelListener {

    void onChannelsFinishedLoading(List<Channel> channels);

}

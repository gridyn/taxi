package com.blacksea.hahataxi.twilio

import android.content.Context
import android.util.Log
import com.blacksea.hahataxi.rest.TwilioApi
import com.blacksea.hahataxi.twilio.listeners.TaskCompletionListener
import com.blacksea.hahataxi.util.getDeviceId
import com.google.gson.Gson
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class AccessTokenFetcher(val context: Context, val twilioApi: TwilioApi, gson: Gson) {

    fun fetch(listener: TaskCompletionListener<String, String>) {
        twilioApi.getAccessToken("text/plain", getDeviceId(context))
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe({
                    Log.i("log", it)
                    listener.onSuccess(it)
                }, {
                    listener.onError(it.message)
                })
    }
}

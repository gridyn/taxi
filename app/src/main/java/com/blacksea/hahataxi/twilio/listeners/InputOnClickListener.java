package com.blacksea.hahataxi.twilio.listeners;

public interface InputOnClickListener {

    void onClick(String input);

}

package com.blacksea.hahataxi.twilio

import android.content.Context

import com.blacksea.hahataxi.twilio.listeners.TaskCompletionListener
import com.twilio.chat.CallbackListener
import com.twilio.chat.ChatClient
import com.twilio.chat.ErrorInfo

class ChatClientBuilder(private val context: Context) : CallbackListener<ChatClient>() {

    /**
     * @link https://www.twilio.com/docs/api/client/regions - for more information
     */
    private val REGION = "us1"
    private lateinit var buildListener: TaskCompletionListener<ChatClient, String>

    fun build(token: String, listener: TaskCompletionListener<ChatClient, String>) {
        val props = ChatClient.Properties.Builder()
                .setSynchronizationStrategy(ChatClient.SynchronizationStrategy.CHANNELS_LIST)
                .setRegion(REGION)
                .createProperties()

        buildListener = listener
        ChatClient.create(context.applicationContext, token, props, this)
    }


    override fun onSuccess(chatClient: ChatClient) {
        buildListener.onSuccess(chatClient)
    }

    override fun onError(errorInfo: ErrorInfo?) {
        buildListener.onError(errorInfo!!.errorText)
    }
}

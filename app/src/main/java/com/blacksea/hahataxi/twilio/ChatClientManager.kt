package com.blacksea.hahataxi.twilio

import android.content.Context
import android.util.Log
import com.blacksea.hahataxi.twilio.listeners.TaskCompletionListener
import com.blacksea.hahataxi.util.PreferencesHelper
import com.twilio.accessmanager.AccessManager
import com.twilio.chat.ChatClient
import com.twilio.chat.ChatClientListener
import com.twilio.chat.ErrorInfo
import com.twilio.chat.StatusListener

class ChatClientManager(val context: Context, val preferences: PreferencesHelper, val chatClientBuilder: ChatClientBuilder, val accessTokenFetcher: AccessTokenFetcher) : AccessManager.Listener, AccessManager.TokenUpdateListener {
    private var chatClient: ChatClient? = null
    private lateinit var accessManager: AccessManager

    fun setClientListener(listener: ChatClientListener) {
        this.chatClient!!.setListener(listener)
    }

    fun getChatClient(): ChatClient? = chatClient

    fun setupGcmToken(token: String) {
        Log.i("log", "ChatClientManager setupGcmToken")
        chatClient!!.registerGCMToken(token, object : StatusListener() {
            override fun onSuccess() {
                Log.i("log", "GCM registered")
            }

            override fun onError(errorInfo: ErrorInfo?) {
                Log.i("log", "GCM no registered: ${errorInfo!!.errorText}")
            }
        })
    }

    fun connectClient(listener: TaskCompletionListener<Void, String>) {
        accessTokenFetcher.fetch(object : TaskCompletionListener<String, String> {
            override fun onSuccess(token: String) {
                preferences.twilioAccessToken = token
                createAccessManager()
                buildClient(listener)
                Log.i("log", "token fetched")
            }

            override fun onError(message: String) {
                Log.i("log", "ChatClientManager connectClient $message")
            }
        })
    }

    fun buildClient(listener: TaskCompletionListener<Void, String>) {
        chatClientBuilder.build(preferences.twilioAccessToken!!, object : TaskCompletionListener<ChatClient, String> {
            override fun onSuccess(chatClient: ChatClient) {
                this@ChatClientManager.chatClient = chatClient
                if (this@ChatClientManager.chatClient == null) {
                    Log.i("log", "ChatClientManager buildClient: chatClient is null")
                }
                listener.onSuccess(null)
            }

            override fun onError(message: String) {
//                Log.i("log", "ChatClientManager buildClient $message")
//                accessTokenFetcher.fetch(object : TaskCompletionListener<String, String> {
//                    override fun onSuccess(t: String?) {
//                        Log.i("log", "кароч вот не ошибка")
//                    }
//
//                    override fun onError(u: String?) {
//                        Log.i("log", "ChatClientManager buildClient $u")
//                    }
//                })
            }
        })
    }

    private fun createAccessManager() {
        this.accessManager = AccessManager(preferences.twilioAccessToken, this)
        accessManager.addTokenUpdateListener(this)
    }

    override fun onTokenWillExpire(accessManager: AccessManager) {
        updateToken(accessManager)
    }

    override fun onTokenExpired(accessManager: AccessManager) {
        updateToken(accessManager)
    }

    override fun onError(accessManager: AccessManager, s: String) {
        Log.i("log", "token error: " + s)
    }

    override fun onTokenUpdated(token: String) {
        chatClient?.updateToken(token, object : StatusListener() {
            override fun onSuccess() {
                Log.i("log", "token updated")
            }

            override fun onError(errorInfo: ErrorInfo?) {
                Log.i("log", errorInfo?.errorText)
            }
        })
    }

    private fun updateToken(accessManager: AccessManager) {
        accessTokenFetcher.fetch(object : TaskCompletionListener<String, String> {
            override fun onSuccess(token: String) {
                accessManager.updateToken(token)
                preferences.twilioAccessToken = token
            }

            override fun onError(message: String) {
                Log.i("log", "Error trying to fetch token: " + message)
            }
        })
    }

    fun shutdown() {
        if (chatClient != null) {
            chatClient!!.shutdown()
        }
    }
}

package com.blacksea.hahataxi.di

import android.content.ContentResolver
import android.content.Context
import android.support.v4.content.LocalBroadcastManager
import com.blacksea.hahataxi.TaxiApplication
import com.blacksea.hahataxi.gcm.GcmListenerService
import com.blacksea.hahataxi.gcm.RegistrationIntentService
import com.blacksea.hahataxi.rest.*
import com.blacksea.hahataxi.signalr.DriverSignalRService
import com.blacksea.hahataxi.signalr.PassengerSignalRService
import com.blacksea.hahataxi.twilio.AccessTokenFetcher
import com.blacksea.hahataxi.twilio.ChatClientManager
import com.blacksea.hahataxi.twilio.channels.ChannelManager
import com.blacksea.hahataxi.util.PreferencesHelper
import com.blacksea.hahataxi.util.SignalRBus
import com.blacksea.hahataxi.util.UploadPhotoService
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import dagger.Component
import okhttp3.OkHttpClient
import pl.charmas.android.reactivelocation.ReactiveLocationProvider
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {

    fun preferencesHelper(): PreferencesHelper

    fun context(): Context

    fun loginApi(): LoginApi

    fun updateApi(): UpdateApi

    fun profileApi(): ProfileApi

    fun passengerApi(): PassengerApi

    fun driverApi(): DriverApi

    fun twilioApi(): TwilioApi

    fun locationProvider(): ReactiveLocationProvider

    fun picasso(): Picasso

    fun gson(): Gson

    fun rxBus(): SignalRBus

    fun chatClientManager(): ChatClientManager

    fun channelManager(): ChannelManager

    fun localBroadcastManager(): LocalBroadcastManager

    fun okHttpBuilder(): OkHttpClient.Builder

    fun contentResolver(): ContentResolver

    fun inject(taxiApplication: TaxiApplication)

    fun inject(driverSignalRService: DriverSignalRService)

    fun inject(passengerSignalRService: PassengerSignalRService)

    fun inject(chatClientManager: ChatClientManager)

    fun inject(accessTokenFetcher: AccessTokenFetcher)

    fun inject(registrationIntentService: RegistrationIntentService)

    fun inject(gcmListenerService: GcmListenerService)

    fun inject(uploadPhotoService: UploadPhotoService)
}
package com.blacksea.hahataxi.di

import javax.inject.Scope

@Scope
annotation class FragmentScope
package com.blacksea.hahataxi.di

import android.content.ContentResolver
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.support.v4.content.LocalBroadcastManager
import com.blacksea.hahataxi.TaxiApplication
import com.blacksea.hahataxi.rest.*
import com.blacksea.hahataxi.twilio.AccessTokenFetcher
import com.blacksea.hahataxi.twilio.ChatClientBuilder
import com.blacksea.hahataxi.twilio.ChatClientManager
import com.blacksea.hahataxi.twilio.channels.ChannelManager
import com.blacksea.hahataxi.util.PreferencesHelper
import com.blacksea.hahataxi.util.SignalRBus
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.github.simonpercic.oklog3.OkLogInterceptor
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import pl.charmas.android.reactivelocation.ReactiveLocationProvider
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class AppModule(private val taxiApplication: TaxiApplication) {

    @Singleton
    @Provides fun context(): Context {
        return taxiApplication.applicationContext
    }

    @Singleton
    @Provides fun sharedPreferences(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    @Singleton
    @Provides fun gson(): Gson {
        return GsonBuilder()
                .registerTypeAdapter(LatLng::class.java, JsonDeserializer<LatLng> { json, typeOfT, context -> LatLng(json.asJsonObject["Latitude"].asDouble, json.asJsonObject["Longitude"].asDouble) })
                .setLenient()
                .create()
    }

    @Singleton
    @Provides fun preferencesHelper(preferences: SharedPreferences, gson: Gson): PreferencesHelper {
        return PreferencesHelper(preferences, gson)
    }

    @Singleton
    @Provides fun locationProvider(context: Context): ReactiveLocationProvider {
        return ReactiveLocationProvider(context.applicationContext)
    }

    @Singleton
    @Provides fun okHttpLogged(): OkHttpClient.Builder {
//        val fullLogInterceptor = HttpLoggingInterceptor()
//        fullLogInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder().addInterceptor(OkLogInterceptor.builder().build())
    }

    @Singleton
    @Provides fun loginApi(okHttpBuilder: OkHttpClient.Builder, gson: Gson): LoginApi {
        val httpClient = okHttpBuilder.readTimeout(60, TimeUnit.SECONDS).connectTimeout(60, TimeUnit.SECONDS).build()

        return Retrofit.Builder()
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .baseUrl("${ServerConstants.SERVER_URL}/connect/")
                .build().create(LoginApi::class.java)
    }

    @Singleton
    @Provides fun updateApi(okHttpBuilder: OkHttpClient.Builder, gson: Gson): UpdateApi {
        val httpClient = okHttpBuilder.readTimeout(60, TimeUnit.SECONDS).connectTimeout(60, TimeUnit.SECONDS).build()

        return Retrofit.Builder()
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .baseUrl("${ServerConstants.SERVER_URL}/1/")
                .build().create(UpdateApi::class.java)
    }

    @Singleton
    @Provides fun profileApi(tokenInterceptor: TokenInterceptor, tokenAuthenticator: TokenAuthenticator, gson: Gson): ProfileApi {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .client(OkHttpClient.Builder().addInterceptor(tokenInterceptor)
                        .followRedirects(false)
                        .addNetworkInterceptor(StethoInterceptor())
                        .authenticator(tokenAuthenticator).build())
                .baseUrl("${ServerConstants.SERVER_URL}/1/")
                .build().create(ProfileApi::class.java)
    }

    @Singleton
    @Provides fun tokenInterceptor(preferencesHelper: PreferencesHelper): TokenInterceptor {
        return TokenInterceptor(preferencesHelper)
    }

    @Singleton
    @Provides fun tokenAuthenticator(preferencesHelper: PreferencesHelper, loginApi: LoginApi, context: Context): TokenAuthenticator {
        return TokenAuthenticator(preferencesHelper, loginApi, context)
    }

    @Singleton
    @Provides fun retrofitV1withTokenAuthenticator(okHttpBuilder: OkHttpClient.Builder,
                             tokenInterceptor: TokenInterceptor,
                             tokenAuthenticator: TokenAuthenticator,
                             gson: Gson): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .client(okHttpBuilder.addInterceptor(tokenInterceptor).authenticator(tokenAuthenticator).build())
                .baseUrl("${ServerConstants.SERVER_URL}/1/")
                .build()
    }

    @Singleton
    @Provides fun passengerApi(tokenInterceptor: TokenInterceptor, tokenAuthenticator: TokenAuthenticator, gson: Gson): PassengerApi {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .client(OkHttpClient.Builder().addInterceptor(tokenInterceptor).authenticator(tokenAuthenticator).build())
                .baseUrl("${ServerConstants.SERVER_URL}/1/")
                .build().create(PassengerApi::class.java)
    }

    @Singleton
    @Provides fun driverApi(tokenInterceptor: TokenInterceptor, tokenAuthenticator: TokenAuthenticator, gson: Gson): DriverApi {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .client(OkHttpClient.Builder().addInterceptor(tokenInterceptor).authenticator(tokenAuthenticator).build())
                .baseUrl("${ServerConstants.SERVER_URL}/1/")
                .build().create(DriverApi::class.java)
    }

    @Singleton
    @Provides fun twilioApi(tokenInterceptor: TokenInterceptor, tokenAuthenticator: TokenAuthenticator, gson: Gson): TwilioApi {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .client(OkHttpClient.Builder().addInterceptor(tokenInterceptor).authenticator(tokenAuthenticator).build())
                .baseUrl("${ServerConstants.SERVER_URL}/1/Account/Twilio/")
                .build().create(TwilioApi::class.java)
    }

    @Singleton
    @Provides fun picasso(context: Context): Picasso {
        return Picasso.Builder(context)
                .indicatorsEnabled(false)
                .build()
    }

    @Singleton
    @Provides fun rxBus(): SignalRBus {
        return SignalRBus()
    }

    @Singleton
    @Provides fun provideLocalBroadcastManager(context: Context): LocalBroadcastManager {
        return LocalBroadcastManager.getInstance(context)
    }

    @Singleton
    @Provides fun provideChatClientManager(context: Context, preferences: PreferencesHelper, chatClientBuilder: ChatClientBuilder, accessTokenFetcher: AccessTokenFetcher): ChatClientManager {
        return ChatClientManager(context, preferences, chatClientBuilder, accessTokenFetcher)
    }

    @Singleton
    @Provides fun provideChannelManager(context: Context, chatClientManager: ChatClientManager): ChannelManager {
        return ChannelManager(context, chatClientManager)
    }

    @Singleton
    @Provides fun provideAccessTokenFetcher(context: Context, twilioApi: TwilioApi, gson: Gson): AccessTokenFetcher {
        return AccessTokenFetcher(context, twilioApi, gson)
    }

    @Singleton
    @Provides fun provideChatClientBuilder(context: Context): ChatClientBuilder {
        return ChatClientBuilder(context)
    }

    @Singleton
    @Provides fun getContentResolver(context: Context): ContentResolver {
        return context.contentResolver
    }
}
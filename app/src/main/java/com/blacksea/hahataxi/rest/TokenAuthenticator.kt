package com.blacksea.hahataxi.rest

import android.content.Context
import android.content.Intent
import android.provider.Settings
import android.util.Log
import com.blacksea.hahataxi.activities.login.LoginActivity
import com.blacksea.hahataxi.util.PreferencesHelper
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route

class TokenAuthenticator(private val preferences: PreferencesHelper, private val api: LoginApi, private val context: Context) : Authenticator {

    override fun authenticate(route: Route?, response: Response): Request {
        val refreshToken = preferences.jwtToken?.refreshToken
        if (refreshToken != null) {
            val jwt = api.signInWithRefreshTokenSync(refreshToken,
                    Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)).execute().body()
            if (jwt != null)
                preferences.jwtToken = jwt
            else {
                context.startActivity(Intent(context, LoginActivity::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
            }
            Log.i("log", "access token = ${jwt.idToken}")
            return response.request().newBuilder()
                    .removeHeader("Authorization")
                    .addHeader("Authorization", "${jwt.tokenType} ${jwt.accessToken}").build()
        } else {
            throw Exception("Refresh token wasn't found")
        }
    }
}
package com.blacksea.hahataxi.rest

import com.blacksea.hahataxi.util.PreferencesHelper
import okhttp3.Interceptor
import okhttp3.Response

class TokenInterceptor(private val preferences: PreferencesHelper) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val accessToken = preferences.jwtToken?.accessToken
        return chain.proceed(chain.request().newBuilder()
                .addHeader("Authorization", "Bearer $accessToken")
                .build())
    }
}
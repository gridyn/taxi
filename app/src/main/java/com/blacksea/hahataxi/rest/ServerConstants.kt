package com.blacksea.hahataxi.rest

object ServerConstants {

    const val SERVER_URL = "http://103.7.253.137:8088"
//    const val SERVER_URL = "http://10.0.2.2:5000"
//    const val SERVER_URL = "http://169.254.80.80:1966"
//    const val SERVER_URL = "http://Andrii-Surf:1966"

    const val KEY_NAME = "unique_name"
    const val KEY_EMAIL = "email"
    const val KEY_PHONE = "phone_number"
    const val KEY_IS_DRIVER = "is_driver"
    const val KEY_PICTURE = "picture"

    const val DEFAULT_IMAGE_URI = "file:///android_asset/contact.png" //not server
    const val IMAGE_URL = "$SERVER_URL/ProfilePictures/"

    const val REQUESTED_CAR_TYPE_STANDARD = "Standard"
    const val REQUESTED_CAR_TYPE_VAN = "Van"
    const val REQUESTED_CAR_TYPE_BUSINESS = "Business"

    const val PASSENGER_HUB = "PassengerHub"
    const val DRIVER_HUB = "DriverHub"

    //method for passenger and driver
    const val METHOD_MOVE = "Move"

    //IDriverClient.cs proxy methods
    const val METHOD_MATCHED_PASSENGER = "MatchedPassenger"
    const val METHOD_UNMATCHED_PASSENGER = "UnmatchedPassenger"
    const val METHOD_PASSENGER_DISCONNECTED = "PassengerDisconnected"
    const val METHOD_PASSENGER_RECONNECTED = "PassengerReconnected"
    const val METHOD_PASSENGER_MOVED = "PassengerMoved"
    //DriverHub.cs 
    const val METHOD_DRIVER_PUBLISH_LOCATION = "PublishLocation"
    //const val METHOD_DRIVER_CHANGE_RADIUS = "ChangeRadius"

    //IPassengerClient.cs proxy methods
    const val METHOD_MATCHED_DRIVER = "MatchedDriver"
    const val METHOD_UNMATCHED_DRIVER = "UnmatchedDriver"
    const val METHOD_DRIVER_DISCONNECTED = "DriverDisconnected"
    const val METHOD_DRIVER_RECONNECTED = "DriverReconnected"
    const val METHOD_DRIVER_MOVED = "DriverMoved"
    //PassengerHub.cs 
    const val METHOD_PASSENGER_PUBLISH_LOCATION = "PublishLocation"
    // const val METHOD_PASSENGER_CHANGE_MAP_VIEW = "ChangeMapView"
}
package com.blacksea.hahataxi.rest

import com.google.gson.JsonObject
import retrofit2.http.GET
import rx.Observable

interface DriverApi {

    @GET("Driver/Account/UserInfo")
    fun getProfileInfo(): Observable<JsonObject>
}
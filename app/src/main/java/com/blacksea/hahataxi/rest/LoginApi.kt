package com.blacksea.hahataxi.rest

import com.blacksea.hahataxi.models.JsonWebToken
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import rx.Observable

interface LoginApi {

    @POST("token")
    @FormUrlEncoded
    fun signInWithLoginAndPassword(
            @Field("username") username: String,
            @Field("password") password: String,
            @Field("device_id") deviceId: String,
            @Field("client_id") clientId: String = "HahaAndroid",
            @Field("grant_type") grantType: String = "password",
            @Field("scope") scope: String = "openid email phone profile qb tw offline_access"
    ): Observable<JsonWebToken>

    @POST("token")
    @FormUrlEncoded
    fun signInWithRefreshToken(
            @Field("refresh_token") refreshToken: String?,
            @Field("device_id") deviceId: String,
            @Field("client_id") clientId: String = "HahaAndroid",
            @Field("grant_type") grantType: String = "refresh_token",
            @Field("scope") scope: String = "openid email phone profile qb tw offline_access"
    ): Observable<JsonWebToken>

    @POST("token")
    @FormUrlEncoded
    fun signInWithRefreshTokenSync(
            @Field("refresh_token") refreshToken: String?,
            @Field("device_id") deviceId: String,
            @Field("client_id") clientId: String = "HahaAndroid",
            @Field("grant_type") grantType: String = "refresh_token",
            @Field("scope") scope: String = "openid email phone profile qb tw offline_access"
    ): Call<JsonWebToken>
}
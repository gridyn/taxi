package com.blacksea.hahataxi.rest

import com.blacksea.hahataxi.BuildConfig
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

interface UpdateApi {

    @GET("CheckForUpdate")
    fun сheckForUpdate(
            @Query("clientVersion") clientVersion: String = BuildConfig.VERSION_NAME,
            @Query("clientId") clientId: String = "HahaAndroid"
    ): Observable<Any>

}
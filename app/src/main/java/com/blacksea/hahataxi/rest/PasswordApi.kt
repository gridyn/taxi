package com.blacksea.hahataxi.rest

import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import rx.Observable

interface PasswordApi {
    @POST("Account/Password/ResetByEmail")
    fun resetByEmail(@Body searchTerm: RequestBody): Observable<Response<Void>>

    @POST("Account/Password/ConfirmResetByEmail")
    @FormUrlEncoded
    fun confirmResetByEmail(
            @Field("searchTerm") searchTerm: String,
            @Field("code") code: String,
            @Field("password") password: String): Observable<Response<Void>>
}




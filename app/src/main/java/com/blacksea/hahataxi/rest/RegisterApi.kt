package com.blacksea.hahataxi.rest

import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*
import rx.Observable

interface RegisterApi {

    @POST("Passenger/Account/Register")
    @FormUrlEncoded
    fun registerPassenger(
            @Field("userName") userName: String,
            @Field("password") password: String,
            @Field("email") email: String,
            @Field("phone") phone: String
    ): Observable<Response<Void>>

    @POST("Driver/Account/Register")
    @FormUrlEncoded
    fun registerDriver(
            @Field("userName") userName: String,
            @Field("password") password: String,
            @Field("license") license: String,
            @Field("email") email: String,
            @Field("phone") phone: String,
            @Field("district") district: String,
            @Field("carManufacturer") carManufacturer: String,
            @Field("carModel") carModel: String,
            @Field("carBodyStyle") carBodyStyle: String,
            @Field("carMaxPassengers") carMaxPassengers: Int
    ): Observable<Response<Void>>

    @POST("Passenger/Account/Register")
    @Multipart
    fun registerPassenger(
            @Part("userName") userName: String,
            @Part("password") password: String,
            @Part("email") email: String,
            @Part("phone") phone: String,
            @Part photo: MultipartBody.Part
    ): Observable<Response<Void>>

    @POST("Driver/Account/Register")
    @Multipart
    fun registerDriver(
            @Part("userName") userName: String,
            @Part("password") password: String,
            @Part("license") license: String,
            @Part("email") email: String,
            @Part("phone") phone: String,
            @Part("district") district: String,
            @Part("carManufacturer") carManufacturer: String,
            @Part("carModel") carModel: String,
            @Part("carBodyStyle") carBodyStyle: String,
            @Part("carMaxPassengers") carMaxPassengers: Int,
            @Part photo: MultipartBody.Part
    ): Observable<Response<Void>>
}
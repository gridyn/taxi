package com.blacksea.hahataxi.rest

import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.http.GET
import rx.Observable

interface PassengerApi {

    @GET("Passenger/Account/UserInfo")
    fun getProfileInfo(): Observable<Response<JsonObject>>
}
package com.blacksea.hahataxi.rest

import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import rx.Observable

interface TwilioApi {

    @POST("Token")
    fun getAccessToken(@Header("Content-Type") content_type: String, @Body deviceId: String): Observable<String>

    @POST("RegisterUser")
    fun registerUser(): Observable<Response<Unit>>

}
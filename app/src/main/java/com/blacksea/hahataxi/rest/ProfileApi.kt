package com.blacksea.hahataxi.rest

import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*
import rx.Observable

interface ProfileApi {

    @GET("Accounts/{userName}/Picture")
    fun getAccountPicture(@Path("userName") userName: String): Observable<Response<Void>>

    @POST("Account/Picture")
    @Multipart
    fun postAccountPicture(
            @Part file: MultipartBody.Part
    ): Observable<Response<Void>>

    @DELETE("Account/Picture")
    fun deleteAccountPicture(
    ) :Observable<Response<Void>>

    @POST("Account/VerifyEmail")
    @FormUrlEncoded
    fun verifyMail(
            @Field("email") email: String,
            @Field("code") code: String
    ): Observable<Response<Void>>

    @POST("Account/Email/SendConfirmation")
    fun sendEmailConfirmation(
    ): Observable<Response<Void>>

    @POST("Account/Email/Confirm")
    fun confirmEmail(
            @Body code: String
    ): Observable<Response<Void>>

    @PUT("Account/Email")
    @FormUrlEncoded
    fun changeEmail(
            @Field("password") password: String,
            @Field("email") email: String
    ): Observable<Response<Void>>
}
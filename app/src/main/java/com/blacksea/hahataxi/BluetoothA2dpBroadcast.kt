package com.blacksea.hahataxi

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.widget.Toast


class BluetoothA2dpBroadcast : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent?) {
        Toast.makeText(context, "A2DP connected", Toast.LENGTH_LONG).show()
        val myAudioManager = context.getSystemService("audio") as AudioManager
        myAudioManager.isBluetoothScoOn = true
        myAudioManager.startBluetoothSco()
        myAudioManager.mode = AudioManager.MODE_IN_COMMUNICATION
    }
}
package com.blacksea.hahataxi

import android.support.annotation.StringRes

interface BaseView {

    fun showMessage(message: String)

    fun showMessage(@StringRes stringId: Int)

    fun startActivityAfterTokenExpiration()
}
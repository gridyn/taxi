package com.blacksea.hahataxi

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import com.blacksea.hahataxi.di.AppComponent
import com.blacksea.hahataxi.di.AppModule
import com.blacksea.hahataxi.di.DaggerAppComponent
import com.crashlytics.android.Crashlytics
import com.facebook.stetho.Stetho
import com.microsoft.azure.mobile.MobileCenter
import com.microsoft.azure.mobile.analytics.Analytics
import com.microsoft.azure.mobile.crashes.Crashes
import com.microsoft.azure.mobile.distribute.Distribute
import io.fabric.sdk.android.Fabric

class TaxiApplication : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
        Stetho.initializeWithDefaults(this)
        setupGraph()
        setupCrashlytics()
        setupAzureMobileCenter()
    }

    private fun setupGraph() {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
        appComponent.inject(this)
    }

    private fun setupCrashlytics() {
        Fabric.with(this, Crashlytics())
    }

    private fun setupAzureMobileCenter() {
        MobileCenter.start(this, "6298f1c6-a21e-4ab7-b377-1ef1def7aa4c",
                Analytics::class.java, Crashes::class.java, Distribute::class.java)
    }

    companion object {
        operator fun get(context: Context): TaxiApplication {
            return context.applicationContext as TaxiApplication
        }
    }
}
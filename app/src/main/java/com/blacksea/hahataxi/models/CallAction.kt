package com.blacksea.hahataxi.models

enum class CallAction {
    ACCEPT, REJECT, CALL, NOT_WAIT, JOINED
}
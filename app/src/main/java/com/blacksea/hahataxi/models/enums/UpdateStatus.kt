package com.blacksea.hahataxi.models.enums

enum class UpdateStatus {
    No,
    Available,
    Mandatory
}
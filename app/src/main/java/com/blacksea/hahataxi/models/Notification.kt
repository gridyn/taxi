package com.blacksea.hahataxi.models

data class Notification(
        val channelId: String,
        val author: String,
        val message: String,
        val channelTitle: String,
        val isShow: Boolean
)
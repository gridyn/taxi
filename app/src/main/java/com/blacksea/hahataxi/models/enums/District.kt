package com.blacksea.hahataxi.models.enums

import com.blacksea.hahataxi.R

enum class District(val id: Long, val districtResId: Int, val zone: Int = 1) {
    BUMTHANG(1, R.string.bumthang, 2),
    CHUKHA(2, R.string.chukha),
    DAGANA(3, R.string.dagana, 3),
    GASA(4, R.string.gasa, 3),
    HAA(5, R.string.haa),
    LHUNTSE(6, R.string.lhuntse, 4),
    MONGAR(7, R.string.mongar, 4),
    PARO(8, R.string.paro),
    PEMAGATSHEL(9, R.string.pemagatshel, 4),
    PUNAKHA(10, R.string.punakha, 3),
    SAMDRUP(11, R.string.samdrup, 4),
    SAMTSE(12, R.string.samtse),
    SARPANG(13, R.string.sarpang, 2),
    THIMPHU(14, R.string.thimphu),
    TRASHIGANG(15, R.string.trashigang, 4),
    TRASHIYANGTSE(16, R.string.trashiyangtse, 4),
    TRONGSA(17, R.string.trongsa, 2),
    TSIRANG(18, R.string.tsirang, 3),
    WANGDUE(19, R.string.wangdue, 3),
    ZHEMGANG(20, R.string.zhemgang, 2)
}
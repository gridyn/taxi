package com.blacksea.hahataxi.models

import com.google.gson.annotations.SerializedName

data class Car(
        @SerializedName("RegistrationNumber") val regNumber: String?,
        @SerializedName("Manufacturer") val manufacturer: String?,
        @SerializedName("Model") val model: String?,
        @SerializedName("MaxPassengers") val maxPassengers: Int?
)
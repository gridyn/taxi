package com.blacksea.hahataxi.models

import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName

data class MoveEvent(
        @SerializedName("Id") val id: String,
        @SerializedName("Location") val location: LatLng,
        @SerializedName("Distance") val distanceInMeters: Float
)
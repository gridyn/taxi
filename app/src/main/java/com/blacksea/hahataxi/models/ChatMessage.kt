package com.blacksea.hahataxi.models

import java.util.*

data class ChatMessage(val senderId: String, val text: String, val time: Date)

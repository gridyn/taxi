package com.blacksea.hahataxi.models

import com.google.gson.annotations.SerializedName

data class RequestsChanged<out E>(
        @SerializedName("Matched") val matchedRequests: List<E>,
        @SerializedName("Unmatched") val unmatchedRequests: List<E>,
        @SerializedName("Updated") val updatedRequests: List<E>
)
package com.blacksea.hahataxi.models

enum class CallType {
    INCOMING, OUTGOING, EMPTY
}
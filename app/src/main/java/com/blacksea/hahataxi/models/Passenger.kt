package com.blacksea.hahataxi.models

import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName

data class Passenger(
        @SerializedName("Id") val id: String,
        @SerializedName("UserName") val userName: String,
        @SerializedName("Email") val email: String?,
        @SerializedName("PhoneNumber") val phone: String,
        @SerializedName("PictureUrl") val photoUrl: String?,
        @SerializedName("QuickbloxId") val qbId: String,
        @SerializedName("Location") val location: LatLng,
        @SerializedName("Distance") val distanceInMeters: Float
)
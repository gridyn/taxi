package com.blacksea.hahataxi.models

import android.util.Base64
import com.blacksea.hahataxi.rest.ServerConstants
import com.google.gson.annotations.SerializedName
import org.json.JSONObject
import java.nio.charset.Charset

data class JsonWebToken(
        @SerializedName("scope") val scope: String,
        @SerializedName("token_type") val tokenType: String,
        @SerializedName("access_token") val accessToken: String,
        @SerializedName("refresh_token") val refreshToken: String,
        @SerializedName("expires_in") val expiresIn: Int,
        @SerializedName("id_token") var idToken: String)
{

    val identityToken: JSONObject
        get() = JSONObject(String(Base64.decode(idToken.split('.')[1], Base64.DEFAULT)))

    val twAccessToken: JSONObject
        get() = identityToken.getJSONObject("tw_access_token")

    val isDriver: Boolean
        get() { return JSONObject(String(Base64.decode(idToken.split('.')[1], Base64.DEFAULT))).getBoolean(ServerConstants.KEY_IS_DRIVER) }

    val getPicture: String
        get() = JSONObject(String(Base64.decode(idToken.split('.')[1], Base64.DEFAULT))).getString(ServerConstants.KEY_PICTURE)

    fun changeValueInIdToken(key: String, value: String) {
        val splitToken = idToken.split('.').toMutableList()
        val json = JSONObject(String(Base64.decode(splitToken[1], Base64.DEFAULT)))
        json.put(key, value)
        splitToken[1] = Base64.encodeToString(json.toString().toByteArray(Charset.defaultCharset()), Base64.DEFAULT)
        idToken = splitToken.reduce { acc, tokenPart -> "$acc.$tokenPart" }
    }
}
package com.blacksea.hahataxi.models

import com.google.gson.annotations.SerializedName

data class RegisterResponse(
        @SerializedName("code") val code: String?,
        @SerializedName("description") var description: String?
)
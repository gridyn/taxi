package com.blacksea.hahataxi.view

import android.app.AlertDialog
import android.content.Context
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.activities.main.MainActivity
import com.blacksea.hahataxi.activities.main.MainPresenter

class ConfirmationDialog(context: Context, private val presenter: MainPresenter) : AlertDialog(context) {

    init {
        val rootView = LayoutInflater.from(context).inflate(R.layout.popup_confirm, null)
        (rootView.findViewById(R.id.email_confirm_code) as EditText)
                .setOnEditorActionListener { v, actionId, event ->
                        if (actionId == EditorInfo.IME_ACTION_SEND
                                    && event.action == KeyEvent.ACTION_DOWN) {
                                onConfirmedClicked()
                            true
                    } else {
                        false
                    }
                }

        (rootView.findViewById(R.id.send_email_confirm_code) as Button)
                .setOnClickListener { onConfirmedClicked() }

        (rootView.findViewById(R.id.cancel_email_confirm_code) as Button)
                .setOnClickListener { onCancelClicked() }

        setView(rootView)
    }

    fun onConfirmedClicked() {
        presenter.confirmByScheme((findViewById(R.id.email_confirm_code) as EditText).text.toString())
        cancel()
    }

    fun onCancelClicked() {
        presenter.turnOffEmailConfirmationCode()
        cancel()
    }
}
package com.blacksea.hahataxi.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.models.Driver
import com.blacksea.hahataxi.rest.ServerConstants
import com.blacksea.hahataxi.twilio.channels.ChannelManager
import com.blacksea.hahataxi.util.PreferencesHelper
import com.blacksea.hahataxi.util.TWILIO_VIDEO_UNIQUE
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.tbruyelle.rxpermissions.RxPermissions
import kotlinx.android.synthetic.main.popup_driver.view.*

class DriverPopupDialog(context: Context, rxPermissions: RxPermissions, driver: Driver, channelManager: ChannelManager, preferencesHelper: PreferencesHelper) : PopupDialog(context, rxPermissions, channelManager) {

    init {
        val rootView = LayoutInflater.from(context).inflate(R.layout.popup_driver, null)
        val channelName: String = "${preferencesHelper.login!!}.${driver.userName}"
        setView(rootView)

        with(rootView) {
            Picasso.with(context).load(
                    if (driver.photoUrl != null) ServerConstants.IMAGE_URL + driver.photoUrl
                    else ServerConstants.DEFAULT_IMAGE_URI)
                    .into(imPhoto, object : Callback {
                        override fun onSuccess() {
                            progress.visibility = View.GONE
                        }

                        override fun onError() {
                            progress.visibility = View.GONE
                            imPhoto.setImageResource(R.drawable.contact)
                        }
                    })

            tvUsername.text = driver.userName

            tvDistance.text = String.format(context.getString(R.string.distance_template), driver.distanceInMeters / 1000)
            tvCar.text = String.format(context.getString(R.string.car_format), driver.car.manufacturer, driver.car.model, driver.car.maxPassengers)
            if (driver.car.regNumber.isNullOrBlank())
                tvLicenseNumber.visibility = View.GONE
            else
                tvLicenseNumber.text = driver.car.regNumber

            if (driver.district.isNullOrBlank())
                tvDistrict.visibility = View.GONE
            else
                tvDistrict.text = driver.district

            btnVoip.setOnClickListener { onCallVoipClick(driver.userName, "$TWILIO_VIDEO_UNIQUE.$channelName", driver.photoUrl ?: "") }
            btnPhone.setOnClickListener { onCallPhoneClick(driver.phone) }

            if (driver.email != null) {
                btnEmail.setOnClickListener { onEmailClick(driver.email) }
            } else {
                btnEmail.visibility = View.GONE
            }

            btnChat.setOnClickListener { onChatClick(driver.userName, channelName, driver.photoUrl ?: "") }
        }
    }
}
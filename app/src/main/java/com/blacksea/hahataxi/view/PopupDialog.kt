package com.blacksea.hahataxi.view

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.widget.Toast
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.activities.chat.ChatActivity
import com.blacksea.hahataxi.activities.voip_call.VoipCallActivity
import com.blacksea.hahataxi.models.CallType
import com.blacksea.hahataxi.twilio.channels.ChannelManager
import com.tbruyelle.rxpermissions.RxPermissions
import com.twilio.chat.CallbackListener
import com.twilio.chat.Channel
import com.twilio.chat.ErrorInfo
import com.twilio.chat.StatusListener

abstract class PopupDialog(context: Context, private val rxPermissions: RxPermissions, private val channelManager: ChannelManager) : AlertDialog(context) {

    protected fun onCallVoipClick(userName: String, channelName: String, userImg: String) {
        if (channelManager.channelsObject != null) {
            val progressDialog = ProgressDialog.show(context, null, "Loading")
            channelManager.joinOrCreateGeneralChannelWithCompletion(userName, channelName, object : StatusListener() {
                override fun onSuccess() {
                    context.startActivity(Intent(context, VoipCallActivity::class.java)
                            .putExtra(VoipCallActivity.EXTRA_CALL_TYPE, CallType.OUTGOING)
                            .putExtra(VoipCallActivity.EXTRA_USER_NAME, userName)
                            .putExtra(VoipCallActivity.EXTRA_IMG, userImg)
                            .putExtra(VoipCallActivity.EXTRA_CHANNEL_NAME, channelName))
                    progressDialog.dismiss()
                }

                override fun onError(errorInfo: ErrorInfo?) {
                    Log.i("log", errorInfo!!.errorText)
                    progressDialog.dismiss()
                }
            })
        }
        cancel()
    }

    protected fun onCallPhoneClick(phone: String) {
        val callIntent = Intent(Intent.ACTION_DIAL)
        callIntent.data = Uri.parse("tel:$phone")
        try {
            context.startActivity(callIntent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(context, R.string.no_phone_app, Toast.LENGTH_SHORT).show()
        }
        cancel()
    }

    protected fun onEmailClick(email: String) {
        val emailIntent = Intent(Intent.ACTION_SEND)
        emailIntent.type = "text/plain"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name))
        emailIntent.putExtra(Intent.EXTRA_TEXT, "")
        try {
            context.startActivity(emailIntent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(context, R.string.no_email_client, Toast.LENGTH_SHORT).show()
        }
        cancel()
    }

    protected fun onChatClick(userName: String, channelName: String, userImg: String) {
        if (channelManager.channelsObject != null) {
            channelManager.joinOrCreateGeneralChannelWithCompletion(userName, channelName, object : StatusListener() {
                override fun onSuccess() {
                    channelManager.channelsObject!!.getChannel(channelName, object : CallbackListener<Channel>() {
                        override fun onSuccess(p0: Channel?) {
                            context.startActivity(Intent(context, ChatActivity::class.java)
                                    .putExtra(ChatActivity.EXTRA_CHANNEL, p0)
                                    .putExtra(ChatActivity.EXTRA_IMG, userImg)
                                    .putExtra(ChatActivity.EXTRA_NAME, channelName))
                        }

                    })
                }

                override fun onError(errorInfo: ErrorInfo?) {
                    Log.i("log", errorInfo!!.errorText)
                }
            })
        }
        else {
            Log.i("log", "channelsObject is null")
        }
        cancel()
    }
}
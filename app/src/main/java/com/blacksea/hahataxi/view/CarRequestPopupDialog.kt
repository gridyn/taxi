package com.blacksea.hahataxi.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.models.Passenger
import com.blacksea.hahataxi.rest.ServerConstants
import com.blacksea.hahataxi.twilio.channels.ChannelManager
import com.blacksea.hahataxi.util.PreferencesHelper
import com.blacksea.hahataxi.util.TWILIO_VIDEO_UNIQUE
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.tbruyelle.rxpermissions.RxPermissions
import kotlinx.android.synthetic.main.popup_person.view.*

class CarRequestPopupDialog(context: Context, rxPermissions: RxPermissions, carRequest: Passenger, channelManager: ChannelManager, preferencesHelper: PreferencesHelper) : PopupDialog(context, rxPermissions, channelManager) {

    init {
        val rootView = LayoutInflater.from(context).inflate(R.layout.popup_person, null)
        val channelName: String = "${carRequest.userName}.${preferencesHelper.login!!}"
        setView(rootView)

        with(rootView) {
            Picasso.with(context).load(
                    if (carRequest.photoUrl != null) ServerConstants.IMAGE_URL + carRequest.photoUrl
                    else ServerConstants.DEFAULT_IMAGE_URI)
                    .into(imPhoto, object : Callback {
                        override fun onSuccess() {
                            progress.visibility = View.GONE
                        }

                        override fun onError() {
                            progress.visibility = View.GONE
                            imPhoto.setImageResource(R.drawable.contact)
                        }

                    })

            tvUsername.text = carRequest.userName
            tvDistance.text = String.format(context.getString(R.string.distance_template), carRequest.distanceInMeters / 1000)
            btnCall.setOnClickListener { onCallVoipClick(carRequest.userName, "$TWILIO_VIDEO_UNIQUE.$channelName", carRequest.photoUrl ?: "") }
            if (carRequest.email != null) {
                btnEmail.setOnClickListener { onEmailClick(carRequest.email) }
            } else {
                btnEmail.visibility = View.GONE
            }

            btnChat.setOnClickListener { onChatClick(carRequest.userName, channelName, carRequest.photoUrl ?: "") }
        }
    }
}
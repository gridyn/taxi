package com.blacksea.hahataxi.signalr

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import com.blacksea.hahataxi.util.SignalRBus
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import microsoft.aspnet.signalr.client.hubs.HubConnection
import microsoft.aspnet.signalr.client.hubs.HubProxy
import pl.charmas.android.reactivelocation.ReactiveLocationProvider
import javax.inject.Inject

abstract class SignalRService : Service() {

    companion object {
        const val KEY_ACCESS_TOKEN = "access_token"
        const val KEY_LOCATION = "location"
        const val ACTION_SOCKET_CONNECTED = "com.blacksea.hahataxi.signalr.SOCKET_CONNECTED"
        const val EXTRA_SOCKET_CONNECTED = "EXTRA_SOCKET_CONNECTED"
    }

    lateinit var connection: HubConnection
    lateinit var hub: HubProxy
    @Inject lateinit var bus: SignalRBus
    @Inject lateinit var gson: Gson
    @Inject lateinit var locationProvider: ReactiveLocationProvider

    private lateinit var uiHandler: Handler
    private val binder = LocalBinder()

    override fun onCreate() {
        super.onCreate()
        uiHandler = Handler(Looper.getMainLooper())
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val result = super.onStartCommand(intent, flags, startId)
        startSignalR(intent.extras[KEY_ACCESS_TOKEN] as String, intent.extras[KEY_LOCATION] as LatLng, 10000000f)
        return result
    }

    override fun onBind(intent: Intent): IBinder {
        startSignalR(intent.extras[KEY_ACCESS_TOKEN] as String, intent.extras[KEY_LOCATION] as LatLng, 10000000f)
        return binder
    }

    override fun onDestroy() {
        connection.stop()
        bus.matchedPassengers.clear()
        bus.matchedDrivers.clear()
        bus.signalRStoppedBus.onNext(null)
        super.onDestroy()
    }

    abstract fun startSignalR(accessToken: String, location: LatLng, radiusInMeters: Float)

    inner class LocalBinder : Binder() {
        // Return this instance of SignalRService so clients can call public methods
        val service: SignalRService
            get() = this@SignalRService
    }

    fun <T> HubProxy.onWithUI(eventName: String, function: (t: T) -> Unit, paramType: Class<T>) {
        on<T>(eventName, { p1 -> uiHandler.post { function.invoke(p1) } }, paramType)
    }
}
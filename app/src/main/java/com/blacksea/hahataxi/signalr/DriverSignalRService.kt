package com.blacksea.hahataxi.signalr

import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.blacksea.hahataxi.TaxiApplication
import com.blacksea.hahataxi.models.MoveEvent
import com.blacksea.hahataxi.models.Passenger
import com.blacksea.hahataxi.models.RequestsChanged
import com.blacksea.hahataxi.rest.ServerConstants
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.maps.model.LatLng
import microsoft.aspnet.signalr.client.Platform
import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent
import microsoft.aspnet.signalr.client.hubs.HubConnection
import rx.Observable
import rx.Subscription
import rx.schedulers.Schedulers

class DriverSignalRService : SignalRService() {

    var moveSubscription: Subscription? = null

    override fun onCreate() {
        super.onCreate()
        TaxiApplication[this].appComponent.inject(this)
    }

    private val localBroadcastManager = lazy { LocalBroadcastManager.getInstance(this) }

    override fun startSignalR(accessToken: String, location: LatLng, radiusInMeters: Float) {
        Platform.loadPlatformComponent(AndroidPlatformComponent())
        connection = HubConnection(ServerConstants.SERVER_URL, null, true, { string, logLevel -> System.out.println(string) })
        connection.headers.put("Authorization", "Bearer $accessToken")
        connection.gson = gson
        connection.connected { onConnected(location, radiusInMeters) }
        connection.closed { onDisconnected() }
        connection.reconnecting { onDisconnected() }
        connection.reconnected { locationProvider.lastKnownLocation.subscribe { onConnected(LatLng(it.latitude, it.longitude), radiusInMeters) } }
        hub = connection.createHubProxy(ServerConstants.DRIVER_HUB)

        hub.onWithUI<Passenger>(ServerConstants.METHOD_MATCHED_PASSENGER, { bus.sendMatchedPassenger(it) }, Passenger::class.java)
        hub.onWithUI<String>(ServerConstants.METHOD_UNMATCHED_PASSENGER, { bus.sendUnmatchedPassenger(it) }, String::class.java)
//        hub.onWithUI<String>(ServerConstants.METHOD_PASSENGER_DISCONNECTED, { bus.sendDisconnectedPassenger(it) }, String::class.java)
//        hub.onWithUI<JsonObject>(ServerConstants.METHOD_PASSENGER_RECONNECTED, { bus.sendReconnectedPassenger(it) }, JsonObject::class.java)
        hub.onWithUI<MoveEvent>(ServerConstants.METHOD_PASSENGER_MOVED, { bus.sendMovedPassenger(it) }, MoveEvent::class.java)

        Observable.from(connection.start(), Schedulers.io())
                .subscribe({ localBroadcastManager.value.sendBroadcast(Intent(ACTION_SOCKET_CONNECTED).putExtra(EXTRA_SOCKET_CONNECTED, true)) },
                        { localBroadcastManager.value.sendBroadcast(Intent(ACTION_SOCKET_CONNECTED).putExtra(EXTRA_SOCKET_CONNECTED, false)) })
    }

    override fun onDestroy() {
        onDisconnected()
        super.onDestroy()
    }

    fun onConnected(location: LatLng, radiusInMeters: Float) {
        moveSubscription = Observable.from(hub.invoke(RequestsChanged::class.java, ServerConstants.METHOD_DRIVER_PUBLISH_LOCATION, location, radiusInMeters), Schedulers.io())
                .doOnNext {
                    it.matchedRequests.forEach { bus.sendMatchedPassenger(gson.fromJson(gson.toJsonTree(it).asJsonObject, Passenger::class.java)) }
                    it.unmatchedRequests.forEach { bus.sendUnmatchedPassenger(gson.fromJson(gson.toJsonTree(it).asJsonObject, Passenger::class.java).id) }
//                    it.updatedRequests.forEach { bus.sendUnmatchedPassenger(gson.fromJson(gson.toJsonTree(it).asJsonObject, Passenger::class.java).id) }
                }
                .retry()
                .flatMap { locationProvider.getUpdatedLocation(LocationRequest.create().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY).setSmallestDisplacement(7f).setInterval(2000).setFastestInterval(2000)).skip(1) }
                .subscribe({
                    Observable.from(hub.invoke<RequestsChanged<*>>(RequestsChanged::class.java, ServerConstants.METHOD_MOVE, LatLng(it.latitude, it.longitude)), Schedulers.io())
                            .retry()
                            .subscribe {
                                it.matchedRequests.forEach { bus.sendMatchedPassenger(gson.fromJson(gson.toJsonTree(it).asJsonObject, Passenger::class.java)) }
                                it.unmatchedRequests.forEach { bus.sendUnmatchedPassenger(gson.fromJson(gson.toJsonTree(it).asJsonObject, Passenger::class.java).id) }
//                                it.updatedRequests.forEach { bus.sendUnmatchedPassenger(gson.fromJson(gson.toJsonTree(it).asJsonObject, Passenger::class.java).id) }
                            }
                }, { Log.i("log", it.toString()) })
    }

    fun onDisconnected() {
        moveSubscription?.unsubscribe()
        moveSubscription = null
    }
}
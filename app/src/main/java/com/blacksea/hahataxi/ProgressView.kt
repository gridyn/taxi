package com.blacksea.hahataxi

import android.support.annotation.StringRes

interface ProgressView : BaseView {

    fun showRefreshing(message: String)

    fun showRefreshing(@StringRes stringId: Int = R.string.loading)

    fun hideRefreshing()
}
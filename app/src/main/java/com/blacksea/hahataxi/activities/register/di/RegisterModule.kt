package com.blacksea.hahataxi.activities.register.di

import android.content.Context
import com.blacksea.hahataxi.activities.register.RegisterActivity
import com.blacksea.hahataxi.activities.register.RegisterPresenter
import com.blacksea.hahataxi.di.ActivityScope
import com.blacksea.hahataxi.rest.*
import com.blacksea.hahataxi.util.PreferencesHelper
import com.tbruyelle.rxpermissions.RxPermissions
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.schedulers.Schedulers

@Module
class RegisterModule(private val registerActivity: RegisterActivity, private val context: Context) {

    @ActivityScope
    @Provides fun rxPermission(): RxPermissions {
        return RxPermissions(registerActivity)
    }

    @ActivityScope
    @Provides fun presenter(api: RegisterApi, twilioApi: TwilioApi, profileApi: ProfileApi, preferencesHelper: PreferencesHelper, loginApi: LoginApi) =
            RegisterPresenter(registerActivity, api, registerActivity.resources, twilioApi, profileApi, preferencesHelper, loginApi, context)

    @ActivityScope
    @Provides fun registerApi(okHttpBuilder: OkHttpClient.Builder): RegisterApi {
        return Retrofit.Builder()
                .client(okHttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .baseUrl("${ServerConstants.SERVER_URL}/1/")
                .build().create(RegisterApi::class.java)
    }
}
package com.blacksea.hahataxi.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.blacksea.hahataxi.BaseView
import com.blacksea.hahataxi.TaxiApplication
import com.blacksea.hahataxi.activities.login.LoginActivity
import com.blacksea.hahataxi.di.AppComponent

abstract class BaseActivity : AppCompatActivity(), BaseView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActivityComponent(TaxiApplication[this].appComponent)
    }

    abstract fun setupActivityComponent(appComponent: AppComponent)

    override fun showMessage(stringId: Int) {
        showMessage(getString(stringId))
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun startActivityAfterTokenExpiration() {
        startActivity(Intent(this, LoginActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
    }

    fun closeSoftKeyboard() {
        val inputManager: InputMethodManager? = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager?.hideSoftInputFromWindow(currentFocus?.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }
}
package com.blacksea.hahataxi.activities.resetpassword.di

import com.blacksea.hahataxi.activities.resetpassword.reset.ResetPasswordActivity
import com.blacksea.hahataxi.activities.resetpassword.reset.ResetPasswordLinkActivity
import com.blacksea.hahataxi.di.ActivityScope
import com.blacksea.hahataxi.di.AppComponent
import dagger.Component
import dagger.Subcomponent

@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(ResetPasswordModule::class))
interface ResetPasswordComponent {

    fun inject(activity: ResetPasswordActivity)

    fun inject(activity: ResetPasswordLinkActivity)
}



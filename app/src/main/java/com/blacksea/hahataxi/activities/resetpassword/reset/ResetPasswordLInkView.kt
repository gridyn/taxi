package com.blacksea.hahataxi.activities.resetpassword.reset

import com.blacksea.hahataxi.ProgressView

interface ResetPasswordLinkView : ProgressView {
    fun showError()
    fun showPasswordForm(searchTerm: String)
    fun goMainActivity()
}


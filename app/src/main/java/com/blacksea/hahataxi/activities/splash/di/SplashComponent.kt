package com.blacksea.hahataxi.activities.splash.di

import com.blacksea.hahataxi.activities.splash.SplashActivity
import com.blacksea.hahataxi.di.ActivityScope
import com.blacksea.hahataxi.di.AppComponent
import dagger.Component

@ActivityScope
@Component(modules = arrayOf(SplashModule::class), dependencies = arrayOf(AppComponent::class))
interface SplashComponent {

    fun inject(splashActivity: SplashActivity)
}
package com.blacksea.hahataxi.activities.login.di

import android.content.Context
import com.blacksea.hahataxi.activities.login.LoginPresenter
import com.blacksea.hahataxi.activities.login.LoginView
import com.blacksea.hahataxi.di.ActivityScope
import com.blacksea.hahataxi.rest.LoginApi
import com.blacksea.hahataxi.rest.UpdateApi
import com.blacksea.hahataxi.util.PreferencesHelper
import dagger.Module
import dagger.Provides

@Module
class LoginModule(private val loginView: LoginView, private val context: Context) {

    @ActivityScope
    @Provides fun presenter(updateApi: UpdateApi, loginApi: LoginApi, prefsHelper: PreferencesHelper) = LoginPresenter(loginView, loginApi, updateApi, prefsHelper, context)
}
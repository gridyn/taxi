package com.blacksea.hahataxi.activities.register.di

import com.blacksea.hahataxi.activities.register.RegisterActivity
import com.blacksea.hahataxi.di.ActivityScope
import com.blacksea.hahataxi.di.AppComponent
import dagger.Component
import dagger.Subcomponent

@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(RegisterModule::class))
interface RegisterComponent {
    fun inject(activity: RegisterActivity)
}
package com.blacksea.hahataxi.activities.voip_call.di

import com.blacksea.hahataxi.activities.voip_call.VoipCallActivity
import com.blacksea.hahataxi.di.ActivityScope
import com.blacksea.hahataxi.di.AppComponent
import dagger.Component

@ActivityScope
@Component(modules = arrayOf(VoipCallModule::class), dependencies = arrayOf(AppComponent::class))
interface VoipCallComponent {

    fun inject(voipCallActivity: VoipCallActivity)
}
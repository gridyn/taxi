package com.blacksea.hahataxi.activities.chat

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.models.ChatMessage
import com.blacksea.hahataxi.rest.ServerConstants
import com.blacksea.hahataxi.util.PreferencesHelper
import com.blacksea.hahataxi.util.getTimeFromTwilio
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.twilio.chat.Message
import java.text.SimpleDateFormat
import java.util.*

class ChatAdapter(private val context: Context,
                  private val onDataChatChangeListener: OnDataChatChangeListener,
                  private val preferences: PreferencesHelper,
                  private val userImg: String,
                  private val picasso: Picasso) : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {

    private var messages = ArrayList<ChatMessage>()
    private val dateFormat = SimpleDateFormat("MMM d, kk:mm", Locale.getDefault())
    private val VIEW_TYPE_OUT = 0
    private val VIEW_TYPE_IN = 1

    override fun getItemViewType(position: Int): Int {
        return if (messages[position].senderId == preferences.login!!) VIEW_TYPE_OUT else VIEW_TYPE_IN
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(
                if (viewType == VIEW_TYPE_OUT)
                    R.layout.item_message_out
                else
                    R.layout.item_message_in,
                parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val message = messages[position]
        if (message.senderId.length >= 2)
            holder.imPhoto.text = message.senderId.substring(startIndex = 0, endIndex = 2)
        holder.tvMessage.text = message.text
        holder.tvTime.text = dateFormat.format(message.time)

        if (getItemViewType(position) == VIEW_TYPE_OUT) {
            picasso.load(ServerConstants.IMAGE_URL + preferences.jwtToken?.getPicture).into(holder.imgPhoto)
        } else if (userImg.isNotEmpty()) {
            picasso.load(ServerConstants.SERVER_URL + userImg).into(holder.imgPhoto, object : Callback {
                override fun onSuccess() {}
                override fun onError() {
                    picasso.load(ServerConstants.IMAGE_URL + userImg).into(holder.imgPhoto)
                }
            })
        }
    }

    override fun getItemCount(): Int {
        return messages.size
    }

    fun addMessage(message: ChatMessage) {
        messages.add(message)
        notifyItemInserted(messages.size)
        onDataChatChangeListener.onDataChatChange()
    }

    fun addMessages(messages: List<Message>) {
        for (message in messages) {
            this.messages.add(ChatMessage(message.author, message.messageBody, message.timeStamp.getTimeFromTwilio()))
        }
        notifyItemInserted(messages.size)
        onDataChatChangeListener.onDataChatChange()

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imPhoto = itemView.findViewById(R.id.imPhoto) as TextView
        val tvMessage = itemView.findViewById(R.id.tvMessage) as TextView
        val tvTime = itemView.findViewById(R.id.tvTime) as TextView
        val imgPhoto = itemView.findViewById(R.id.imgPhoto) as ImageView
    }
}

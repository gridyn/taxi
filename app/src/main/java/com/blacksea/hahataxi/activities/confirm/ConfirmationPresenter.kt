package com.blacksea.hahataxi.activities.confirm

import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.rest.ProfileApi
import com.blacksea.hahataxi.subscribers.ErrorSubscriber
import com.blacksea.hahataxi.subscribers.ProgressSubscriber
import com.blacksea.hahataxi.util.PreferencesHelper
import retrofit2.Response
import retrofit2.adapter.rxjava.HttpException

class ConfirmationPresenter(private val view: ConfirmationView,
                            private val preferences: PreferencesHelper,
                            private val profileApi: ProfileApi)
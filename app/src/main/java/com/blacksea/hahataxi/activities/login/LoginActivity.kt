package com.blacksea.hahataxi.activities.login

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.activities.ProgressActivity
import com.blacksea.hahataxi.activities.login.di.DaggerLoginComponent
import com.blacksea.hahataxi.activities.login.di.LoginModule
import com.blacksea.hahataxi.activities.main.MainActivity
import com.blacksea.hahataxi.activities.register.RegisterActivity
import com.blacksea.hahataxi.activities.resetpassword.reset.ResetPasswordActivity
import com.blacksea.hahataxi.di.AppComponent
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : ProgressActivity(), LoginView {

    companion object {
        fun getStartIntent(context: Context, login: String, password: String): Intent {
            val intent = Intent(context, LoginActivity::class.java)
            intent.putExtra(EXTRA_LOGIN, login)
            intent.putExtra(EXTRA_PASSWORD, password)
            return intent
        }

        val EXTRA_LOGIN = "login"
        val EXTRA_PASSWORD = "password"
    }

    @Inject lateinit var presenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (intent.hasExtra(EXTRA_LOGIN) && intent.hasExtra(EXTRA_PASSWORD)) { //comes from RegisterActivity
            presenter.login(intent.getStringExtra(EXTRA_LOGIN), intent.getStringExtra(EXTRA_PASSWORD))
        }

        presenter.checkForUpdate()

        presenter.loginIfHasCreds()
        btnSignIn.setOnClickListener {
            closeSoftKeyboard()
            presenter.login(edLogin.text.toString(), edPassword.text.toString())
        }
        tvForgotPassword.setOnClickListener {
            closeSoftKeyboard()
            presenter.onForgotPasswordClick(edLogin.text.toString())
        }
        btnRestoreByEmail.setOnClickListener {
            val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(currentFocus!!.windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS)

            presenter.onRestoreByEmailClick(edLogin.text.toString())
        }
        btnRestoreByPhone.setOnClickListener {
            val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(currentFocus!!.windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS)

            presenter.onRestoreByPhoneClick(edLogin.text.toString())
        }
        nestGroup.setOnClickListener {
            closeSoftKeyboard()
            presenter.onCloseRestoreFormClick()
        }

        tvRegister.setOnClickListener { presenter.onRegisterClick() }
    }

    override fun setupActivityComponent(appComponent: AppComponent) {
        DaggerLoginComponent.builder()
                .appComponent(appComponent)
                .loginModule(LoginModule(this, this))
                .build().inject(this)
    }

    override fun showResetForm() {
        resetBox.visibility = View.VISIBLE
    }

    override fun hideResetForm() {
        resetBox.visibility = View.GONE
    }

    override fun goRegisterActivity() {
        startActivity(Intent(this, RegisterActivity::class.java))
    }

    override fun goResetPasswordActivity(login: String) {
        startActivity(ResetPasswordActivity.startIntent(this, login))
    }

    override fun goMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun showAvailableDialog() {
        var builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.update_title)
                .setMessage(R.string.update_available_text)
                .setPositiveButton(R.string.update_button, null)
                .setNegativeButton(R.string.dismiss_button, null)
                .setCancelable(false)

        val dialog = builder.create()
        dialog.show()

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener { openInMarket() }
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener { dialog.dismiss() }
    }

    override fun showMandatoryDialog() {
        var builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.update_title)
                .setMessage(R.string.update_mandatory_text)
                .setPositiveButton(R.string.update_button, null)
                .setNegativeButton(R.string.exit_button, null)
                .setCancelable(false)

        val dialog = builder.create()
        dialog.show()

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener { openInMarket() }
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener { finish() }
    }

    fun openInMarket(){
        try {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)))
        } catch (anfe: android.content.ActivityNotFoundException) {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + packageName)))
        }
    }
}
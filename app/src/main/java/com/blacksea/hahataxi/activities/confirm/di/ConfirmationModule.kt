package com.blacksea.hahataxi.activities.confirm.di

import com.blacksea.hahataxi.activities.confirm.ConfirmationPresenter
import com.blacksea.hahataxi.activities.confirm.ConfirmationView
import com.blacksea.hahataxi.di.ActivityScope
import com.blacksea.hahataxi.rest.ProfileApi
import com.blacksea.hahataxi.util.PreferencesHelper
import dagger.Module
import dagger.Provides

@Module
class ConfirmationModule(private val conformationView: ConfirmationView) {

        @ActivityScope
        @Provides fun presenter(preferences: PreferencesHelper,
                                profileApi: ProfileApi) =
                ConfirmationPresenter(conformationView, preferences, profileApi)
}
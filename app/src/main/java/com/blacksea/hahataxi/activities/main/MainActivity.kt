package com.blacksea.hahataxi.activities.main

import android.content.*
import android.location.Location
import android.os.Bundle
import android.os.IBinder
import android.support.design.widget.NavigationView
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.util.Log
import android.view.MenuItem
import android.widget.Switch
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.activities.ProgressActivity
import com.blacksea.hahataxi.activities.login.LoginActivity
import com.blacksea.hahataxi.activities.main.di.DaggerMainActivityComponent
import com.blacksea.hahataxi.activities.main.di.MainActivityModule
import com.blacksea.hahataxi.di.AppComponent
import com.blacksea.hahataxi.fragments.chat_list.ChatListFragment
import com.blacksea.hahataxi.fragments.map.MapFragment
import com.blacksea.hahataxi.fragments.profile.ProfileFragment
import com.blacksea.hahataxi.gcm.RegistrationIntentService
import com.blacksea.hahataxi.signalr.DriverSignalRService
import com.blacksea.hahataxi.signalr.PassengerSignalRService
import com.blacksea.hahataxi.signalr.SignalRService
import com.blacksea.hahataxi.twilio.ChatClientManager
import com.blacksea.hahataxi.twilio.channels.ChannelManager
import com.blacksea.hahataxi.twilio.listeners.TaskCompletionListener
import com.blacksea.hahataxi.view.ConfirmationDialog
import com.google.android.gms.maps.model.LatLng
import com.twilio.chat.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import javax.inject.Inject

class MainActivity : ProgressActivity(), MainView, NavigationView.OnNavigationItemSelectedListener, ChatClientListener {

    @Inject lateinit var presenter: MainPresenter

    @Inject lateinit var chatClientManager: ChatClientManager
    @Inject lateinit var channelManager: ChannelManager

    var signalRService: SignalRService? = null
    var confirmCode : String? = null
    var signalRSwitch: Switch? = null

    private val broadcastServiceConnected = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            signalRSwitch?.isEnabled = true
            if (intent?.getBooleanExtra(SignalRService.EXTRA_SOCKET_CONNECTED, false) == false) {
                signalRSwitch?.isChecked = false
            }
        }
    }

    fun getConfirmCodeValue() : String? {
        return confirmCode
    }

    override fun onSuccessfulConfirmEmail() {
        supportFragmentManager.fragments
                .filter { it is ProfileFragment }
                .forEach { (it as ProfileFragment).onSuccessfulConfirmEmail() }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkForConfirmationCode(intent)
        setContentView(R.layout.activity_main)
        presenter.attachView()
        signalRSwitch = this.nvDrawer.menu.findItem(R.id.nav_switch).actionView.findViewById(R.id.signalR_switch) as Switch
        signalRSwitch?.setOnCheckedChangeListener { compoundButton, isChecked ->
            if (isChecked) {
                compoundButton.isEnabled = false
                presenter.getLocationAndStartSignalR()
            } else {
                presenter.stopSignalR()
            }
        }
        setSupportActionBar(toolbar)
        val drawerToggle = ActionBarDrawerToggle(this, dlDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        dlDrawer.addDrawerListener(drawerToggle)
        drawerToggle.syncState()
        nvDrawer.setNavigationItemSelectedListener(this)

        nvDrawer.setCheckedItem(R.id.nav_map)

        initializeClient()

        showMap()
    }

    override fun onStart() {
        super.onStart()
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastServiceConnected, IntentFilter(SignalRService.ACTION_SOCKET_CONNECTED))
    }

    override fun onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastServiceConnected)
        super.onStop()
    }

    fun initializeClient() {
        chatClientManager.connectClient(object : TaskCompletionListener<Void, String> {
            override fun onSuccess(t: Void?) {
                chatClientManager.setClientListener(this@MainActivity)
                Log.i("log", "client initialized")
                channelManager.initChannelsObject()
                startService(Intent(this@MainActivity, RegistrationIntentService::class.java))
            }

            override fun onError(u: String?) {
                Log.i("log", "Error: $u")
            }
        })
    }

    fun checkForConfirmationCode(intent: Intent) {
        var code = intent.data?.getQueryParameter("emailcfmcode")
        if (code != null) {
            confirmCode = code
            presenter.confirmEmail(code)
            showMyProfile()
        } else presenter.checkForWaitingEmailConfirmationCode()
    }



    override fun showEmailConfirmationPopup() {
        ConfirmationDialog(this, presenter).show()
//                .set.show
    }

    override fun onDestroy() {
        super.onDestroy()
        stopSignalR()
    }

    override fun setupActivityComponent(appComponent: AppComponent) {
        DaggerMainActivityComponent.builder()
                .appComponent(appComponent)
                .mainActivityModule(MainActivityModule(this))
                .build().inject(this)
    }

    override fun showMyProfile() {
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, ProfileFragment()).commit()
    }

    override fun showMap() {
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, MapFragment()).commit()
    }

    override fun showChatList() {
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, ChatListFragment()).commit()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        presenter.navigationDrawerItemClick(item)
        dlDrawer.closeDrawer(GravityCompat.START)
        return true
    }

    override fun startDriverSignalR(accessToken: String, location: Location) {
        val serviceIntent = Intent().setClass(this, DriverSignalRService::class.java)
        serviceIntent.putExtra(SignalRService.KEY_ACCESS_TOKEN, accessToken)
        serviceIntent.putExtra(SignalRService.KEY_LOCATION, LatLng(location.latitude, location.longitude))
        bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

    override fun startPassengerSignalR(accessToken: String, location: Location) {
        val serviceIntent = Intent().setClass(this, PassengerSignalRService::class.java)
        serviceIntent.putExtra(SignalRService.KEY_ACCESS_TOKEN, accessToken)
        serviceIntent.putExtra(SignalRService.KEY_LOCATION, LatLng(location.latitude, location.longitude))
        bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

    override fun stopSignalR() {
        if(serviceConnection.isBinded) {
            serviceConnection.isBinded = false
            unbindService(serviceConnection)
        }
    }

    override fun goLoginScreen() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    private val serviceConnection = object : ServiceConnection {

        var isBinded = false

        override fun onServiceDisconnected(name: ComponentName) { }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            isBinded = true
            signalRService = (service as SignalRService.LocalBinder).service
        }
    }

    override fun onChannelDelete(p0: Channel?) {

    }

    override fun onToastFailed(p0: ErrorInfo?) {
    }

    override fun onClientSynchronization(p0: ChatClient.SynchronizationStatus?) {
    }

    override fun onToastSubscribed() {
    }

    override fun onChannelInvite(p0: Channel?) {
    }

    override fun onChannelSynchronizationChange(p0: Channel?) {
    }

    override fun onUserInfoChange(p0: UserInfo?, p1: UserInfo.UpdateReason?) {
    }

    override fun onConnectionStateChange(p0: ChatClient.ConnectionState?) {
    }

    override fun onError(p0: ErrorInfo?) {
    }

    override fun onChannelAdd(p0: Channel?) {
    }

    override fun onChannelChange(p0: Channel?) {
    }

    override fun onToastNotification(p0: String?, p1: String?) {
    }
}

package com.blacksea.hahataxi.activities.voip_call

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.AudioManager
import android.media.MediaPlayer
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.blacksea.hahataxi.gcm.GcmListenerService
import com.blacksea.hahataxi.models.CallAction
import com.blacksea.hahataxi.models.CallType
import com.blacksea.hahataxi.twilio.channels.ChannelManager
import com.blacksea.hahataxi.util.PreferencesHelper
import com.tbruyelle.rxpermissions.RxPermissions
import com.twilio.chat.*
import com.twilio.video.*
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.subscriptions.CompositeSubscription
import rx.subscriptions.Subscriptions
import java.util.concurrent.TimeUnit

class VoipCallPresenter(private val view: VoipCallView,
                        private val preferencesHelper: PreferencesHelper,
                        private val channelManager: ChannelManager,
                        private val localBroadcastManager: LocalBroadcastManager,
                        private val rxPermissions: RxPermissions) : Room.Listener, Media.Listener {

    private var localMedia = LocalMedia.create(view.getContext())

    private val localAudioTrack = localMedia.addAudioTrack(true)

    private val audioManager = view.getContext().getSystemService(Context.AUDIO_SERVICE) as AudioManager
    private var room: Room? = null
    private var previousAudioMode = 0
    private var participantIdentity = String()
    private var disconnectedFromOnDestroy = false

    private var channel: Channel? = null

    private var messages: Messages? = null

    private val composite = CompositeSubscription()
    private var subscription = Subscriptions.empty()
    private var awaitAcceptSubscription = Subscriptions.empty()

    private var startTime = 0L

    private val SECONDS_AWAIT_FOR_ACCEPT = 40L

    var callType: CallType = CallType.EMPTY
    var mediaPlayer = MediaPlayer()
    private var volumeOn = true
    private var canEnableBluetooth = true

    val broadcastCallAction = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            Log.i("log", "VoipCallPresenter: ${intent?.getStringExtra(GcmListenerService.EXTRA_CALL_STATE)}")
            when (intent?.getStringExtra(GcmListenerService.EXTRA_CALL_STATE)) {
                CallAction.ACCEPT.name -> view.acceptCall()
                CallAction.REJECT.name -> disconnectFromRoom()
                CallAction.NOT_WAIT.name -> disconnectFromRoom(CallAction.NOT_WAIT)
                CallAction.JOINED.name -> view.enableRejectButton(true)
            }
        }
    }

    fun attachView() {
        preferencesHelper.isStartedCall = true
        localBroadcastManager.registerReceiver(broadcastCallAction, IntentFilter(GcmListenerService.EVENT_CALL_STATE))

        subscription = rxPermissions.request(Manifest.permission.RECORD_AUDIO)
                .filter { it }
                .subscribe()
        composite.add(subscription)

        if (callType == CallType.OUTGOING) {
            view.beep()

            awaitAcceptSubscription = Observable.timer(SECONDS_AWAIT_FOR_ACCEPT, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        sendMessage(CallAction.NOT_WAIT)
                        view.finishCall()
                    }
            composite.add(awaitAcceptSubscription)
        } else if (callType == CallType.INCOMING) {
            view.ring()
            view.showNotificationCall()
            sendMessage(CallAction.JOINED)
        }
    }

    fun detachView() {
        if (room != null && room?.state != RoomState.DISCONNECTED) {
            room?.disconnect()
            disconnectedFromOnDestroy = true
        }
        if (localMedia != null) {
            localMedia.release()
            localMedia = null
        }
        if (mediaPlayer.isPlaying)
            mediaPlayer.stop()
        preferencesHelper.isStartedCall = false
        localBroadcastManager.unregisterReceiver(broadcastCallAction)
        composite.clear()
    }

    fun connectToRoom(roomName: String) {
        if (!awaitAcceptSubscription.isUnsubscribed)
            awaitAcceptSubscription.unsubscribe()
        if (mediaPlayer.isPlaying)
            mediaPlayer.stop()
        view.cancelNotification()
        setAudioFocus(true)
        val connectOptions = ConnectOptions.Builder(preferencesHelper.twilioAccessToken)
                .roomName(roomName)
                .localMedia(localMedia)
                .build()
        room = Video.connect(view.getContext(), connectOptions, this)
    }

    fun clickOnMicrophone() {
        localAudioTrack?.enable(!localAudioTrack.isEnabled)
    }

    fun clickOnVolume() {
        if (volumeOn) {
            audioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, 0, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE)
            volumeOn = false
        } else {
            audioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, audioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), AudioManager.FLAG_PLAY_SOUND)
            volumeOn = true
        }
    }

    private fun setAudioFocus(focus: Boolean) {
        if (focus) {
            previousAudioMode = audioManager.mode
            // Request audio focus before making any device switch.
            audioManager.requestAudioFocus(null, AudioManager.STREAM_VOICE_CALL,
                    AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
            /*
             * Use MODE_IN_COMMUNICATION as the default audio mode. It is required
             * to be in this mode when playout and/or recording starts for the best
             * possible VoIP performance. Some devices have difficulties with
             * speaker mode if this is not set.
             */
            audioManager.mode = AudioManager.MODE_IN_COMMUNICATION
            audioManager.isBluetoothScoOn = true
            audioManager.startBluetoothSco()
        } else {
            audioManager.mode = previousAudioMode
            audioManager.abandonAudioFocus(null)
            audioManager.isBluetoothScoOn = false
            audioManager.stopBluetoothSco()
        }
    }

    fun disconnectFromRoom() {
        if (room != null) {
            room?.disconnect()
        } else {
            sendMessage(CallAction.REJECT)
            onDisconnected(null, null)
        }
    }

    fun disconnectFromRoom(callAction: CallAction) {
        if (room != null) {
            room?.disconnect()
        } else {
            sendMessage(callAction)
            onDisconnected(null, null)
        }
    }

    /*
     * Called when participant joins the room
     */
    private fun addParticipant(participant: Participant) {
        /*
         * This app only displays video for one additional participant per Room
         */
        participantIdentity = participant.identity
        view.showMessage("Participant $participantIdentity joined")

        view.enableRejectButton(true)

        /*
         * Start listening for participant media events
         */
        participant.media.setListener(this)
    }

    private fun removeParticipant(participant: Participant) {
        view.showMessage("Participant " + participant.identity + " left.")
        if (participant.identity != participantIdentity) {
            return
        }

        /*
         * Remove participant renderer
         */
        participant.media.setListener(null)
        view.finishCall()
    }


    fun connectChannel(channelName: String) {
        channelManager.getChannel(channelName, {
            channel = it
            if (channel != null) {
                channel!!.synchronize(object : CallbackListener<Channel>() {
                    override fun onSuccess(p0: Channel?) {
                        messages = p0!!.messages
                        sendMessage(CallAction.CALL)
                    }

                    override fun onError(errorInfo: ErrorInfo?) {
                        Log.i("log", errorInfo?.errorText)
                    }
                })
            } else {
                Log.i("log", "VoipCallPresenter connectChannel")
            }
        })
    }

    fun sendMessage(message: CallAction) {
        var subscriptions = Subscriptions.empty()
        if (messages == null) {
            subscriptions = Observable.interval(1, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        if (messages != null) {
                            subscriptions.unsubscribe()
                            sendMessage(message)
                        }
                    }
        } else {
            messages!!.sendMessage(message.name, object : StatusListener() {
                override fun onSuccess() {
                    Log.i("log", "message sent")
                }

                override fun onError(errorInfo: ErrorInfo?) {
                    Log.i("log", errorInfo!!.errorText)
                }
            })
        }
    }

    fun clickOnBluetooth(isEnableBluetooth: Boolean) {
        canEnableBluetooth = isEnableBluetooth
    }

    /**
     * Room events listener
     */

    override fun onConnected(room: Room?) {
        if (room != null) {
            view.connectedTo(room.name)
            view.setStatus("Connected to room")
            for ((_, value) in room.participants) {
                view.enableRejectButton(true)
                addParticipant(value)
                break
            }

            startTime = System.currentTimeMillis()

            subscription = Observable.interval(1, TimeUnit.SECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        val millis = System.currentTimeMillis() - startTime
                        var seconds = (millis / 1000).toInt()
                        val minutes = seconds / 60
                        seconds %= 60
                        view.setStatus(String.format("%d:%02d", minutes, seconds))
                    }
            composite.add(subscription)
        }
    }

    override fun onConnectFailure(p0: Room?, p1: TwilioException?) {
        view.setStatus("Failed to connect")
        Log.i("log", "VoIP onConnectFailure: ${p1?.message} \n${p1?.code}")
    }

    override fun onDisconnected(room: Room?, p1: TwilioException?) {
        if (room != null) {
            if (!disconnectedFromOnDestroy) {
                setAudioFocus(false)
            }
        }
        view.finishCall()
    }

    override fun onParticipantConnected(p0: Room?, participant: Participant?) {
        if (participant != null)
            addParticipant(participant)
    }

    override fun onParticipantDisconnected(p0: Room?, participant: Participant?) {
        if (participant != null)
            removeParticipant(participant)
    }

    override fun onRecordingStarted(p0: Room?) {
        Log.i("log", "onRecordingStarted")
    }

    override fun onRecordingStopped(p0: Room?) {
        Log.i("log", "onRecordingStopped")
    }

    /**
     * Media events listener
     */

    override fun onVideoTrackAdded(p0: Media?, p1: VideoTrack?) {
        Log.i("log", "onVideoTrackAdded")
    }

    override fun onAudioTrackEnabled(p0: Media?, p1: AudioTrack?) {
        Log.i("log", "onAudioTrackEnabled")
    }

    override fun onAudioTrackDisabled(p0: Media?, p1: AudioTrack?) {
        Log.i("log", "onAudioTrackDisabled")
    }

    override fun onAudioTrackAdded(p0: Media?, p1: AudioTrack?) {
        Log.i("log", "onAudioTrackAdded")
    }

    override fun onAudioTrackRemoved(p0: Media?, p1: AudioTrack?) {
        Log.i("log", "onAudioTrackRemoved")
    }

    override fun onVideoTrackEnabled(p0: Media?, p1: VideoTrack?) {
        Log.i("log", "onVideoTrackEnabled")
    }

    override fun onVideoTrackDisabled(p0: Media?, p1: VideoTrack?) {
        Log.i("log", "onVideoTrackDisabled")
    }

    override fun onVideoTrackRemoved(p0: Media?, p1: VideoTrack?) {
        Log.i("log", "onVideoTrackRemoved")
    }
}

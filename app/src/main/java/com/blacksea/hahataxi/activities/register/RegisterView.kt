package com.blacksea.hahataxi.activities.register

import android.support.annotation.StringRes
import com.blacksea.hahataxi.ProgressView

interface RegisterView : ProgressView {

    fun loginWithCredentials(login: String, password: String)

    fun pictureDeleted()

    fun pictureUploaded()

    fun goMainActivity()

    fun showErrorWhenUserNameEmpty(@StringRes stringId: Int)

    fun showErrorWhenPhoneInvalid(@StringRes stringId: Int)

    fun showErrorWhenEmailInvalid(@StringRes stringId: Int)

    fun showErrorOnCarModel(@StringRes stringId: Int)

    fun removeErrorOnEmail()

    fun removeErrorOnCarModel()

    fun showErrorOnManufacturer(@StringRes stringId: Int)

    fun showErrorWhenPasswordInvalid(passError: String?)

    fun removeErrorOnUserName()

    fun removeErrorOnManufacturer()

    fun showErrorOnLicense(@StringRes stringId: Int)

    fun removeErrorOnLicense()
}
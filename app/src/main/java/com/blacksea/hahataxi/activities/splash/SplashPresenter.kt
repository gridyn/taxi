package com.blacksea.hahataxi.activities.splash

import android.content.Context
import android.util.Log
import com.blacksea.hahataxi.models.JsonWebToken
import com.blacksea.hahataxi.rest.LoginApi
import com.blacksea.hahataxi.subscribers.ProgressSubscriber
import com.blacksea.hahataxi.util.PreferencesHelper
import com.blacksea.hahataxi.util.getDeviceId
import org.json.JSONObject
import retrofit2.adapter.rxjava.HttpException
import rx.android.schedulers.AndroidSchedulers

class SplashPresenter(private val view: SplashMvpView,
                      private val preferences: PreferencesHelper,
                      private val api: LoginApi) {


    fun attachView(mvpView: SplashMvpView, context: Context) {
        if (preferences.jwtToken == null || preferences.jwtToken?.refreshToken == null) {
            mvpView.showLoginScreen()
        } else if (preferences.accessTokenExpireTimeMillis <= System.currentTimeMillis()) {
            login(preferences.jwtToken?.refreshToken, context)
        } else {
            mvpView.showMainScreen()
        }
    }

    fun login(refreshToken: String?, context: Context) {
        if (refreshToken == null)
            view.showLoginScreen()
        api.signInWithRefreshToken(refreshToken, getDeviceId(context))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ProgressSubscriber<JsonWebToken>(view) {
                    override fun onNext(token: JsonWebToken) {
                        preferences.jwtToken = token
                        preferences.twilioAccessToken = token.identityToken.getString("tw_access_token")
                        preferences.accessTokenExpireTimeMillis = System.currentTimeMillis() + token.expiresIn * 1000
                        view.showMainScreen()
                    }

                    override fun onError(e: Throwable?) {
                        if (e is HttpException && e.code() == 400) {
                            view.showMessage(JSONObject(e.response().errorBody().string()).getString("error_description"))
                            preferences.jwtToken = null
                            view.showLoginScreen()
                        } else {
                            Log.i("log", e.toString())
                        }
                    }
                })
    }
}
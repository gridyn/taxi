package com.blacksea.hahataxi.activities.resetpassword.reset

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.activities.ProgressActivity
import com.blacksea.hahataxi.activities.resetpassword.di.DaggerResetPasswordComponent
import com.blacksea.hahataxi.activities.resetpassword.di.ResetPasswordModule
import com.blacksea.hahataxi.di.AppComponent
import kotlinx.android.synthetic.main.activity_resetpassword.*
import javax.inject.Inject


class ResetPasswordActivity : ProgressActivity(), ResetPasswordView {
    @Inject lateinit var presenter: ResetPasswordPresenter

    companion object {
        val LOGIN_EXTRA = "login"
        fun startIntent(context: Context, login: String): Intent {
            val intent = Intent(context, ResetPasswordActivity::class.java)
            intent.putExtra(LOGIN_EXTRA, login)
            return intent
        }
    }

    override fun setupActivityComponent(appComponent: AppComponent) {
        DaggerResetPasswordComponent.builder()
                .appComponent(appComponent)
                .resetPasswordModule(ResetPasswordModule(this))
                .build().inject(this)

        presenter.view = this
    }

    override fun setSearchTerm(searchTerm: String) {
        searchTermField.setText(searchTerm)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_resetpassword)

        presenter.setSearchTerm(intent.getStringExtra(LOGIN_EXTRA))

        resetButton.setOnClickListener {
            closeSoftKeyboard()

            presenter.resetPassword(searchTermField.text.toString())
        }

        goMailButton.setOnClickListener {
            goEmailApp()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showSuccessResult() {
        questionBlock.visibility = View.INVISIBLE
        resultBlock.visibility = View.VISIBLE
    }

    fun goEmailApp() {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_APP_EMAIL)
        startActivity(intent)
        finish()
    }
}



package com.blacksea.hahataxi.activities.voip_call

import android.content.Context
import com.blacksea.hahataxi.BaseView

interface VoipCallView : BaseView {

    fun getContext(): Context

    fun connectedTo(name: String?)

    fun finishCall()

    fun setStatus(status: String)

    fun acceptCall()

    fun beep()

    fun ring()

    fun showNotificationCall()

    fun cancelNotification()

    fun setBluetoothState(state: Boolean)

    fun enableRejectButton(isEnable: Boolean)
}
package com.blacksea.hahataxi.activities.login

import com.blacksea.hahataxi.ProgressView

interface LoginView : ProgressView {

    fun goRegisterActivity()

    fun goResetPasswordActivity(login: String)

    fun goMainActivity()

    fun showResetForm()

    fun hideResetForm()

    fun showAvailableDialog()

    fun showMandatoryDialog()
}
package com.blacksea.hahataxi.activities.login

import android.content.Context
import android.util.Log
import android.util.Patterns
import com.blacksea.hahataxi.models.JsonWebToken
import com.blacksea.hahataxi.models.enums.UpdateStatus
import com.blacksea.hahataxi.rest.LoginApi
import com.blacksea.hahataxi.rest.UpdateApi
import com.blacksea.hahataxi.subscribers.ProgressSubscriber
import com.blacksea.hahataxi.util.PreferencesHelper
import com.blacksea.hahataxi.util.getDeviceId
import org.json.JSONObject
import retrofit2.adapter.rxjava.HttpException
import rx.android.schedulers.AndroidSchedulers

class LoginPresenter(private val view: LoginView,
                     private val api: LoginApi,
                     private val updateApi: UpdateApi,
                     private val preferences: PreferencesHelper,
                     private val context: Context) {

    var resetVisible = false

    fun loginIfHasCreds() {
        val refreshToken = preferences.jwtToken?.refreshToken
        if (refreshToken != null) {
            login(refreshToken)
        }
    }

    fun login(refreshToken: String) {
        api.signInWithRefreshToken(refreshToken, getDeviceId(context))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ProgressSubscriber<JsonWebToken>(view) {
                    override fun onNext(token: JsonWebToken) {
                        preferences.jwtToken = token
                        preferences.twilioAccessToken = token.identityToken.getString("tw_access_token")
                        preferences.accessTokenExpireTimeMillis = System.currentTimeMillis() + token.expiresIn * 1000
                        view.goMainActivity()
                    }

                    override fun onError(e: Throwable?) {
                        if (e is HttpException && e.code() == 400) {
                            view.showMessage(JSONObject(e.response().errorBody().string()).getString("error_description"))
                        } else {
                            Log.i("log", e.toString())
                        }
                    }
                })
    }

    fun login(login: String, password: String) {
        api.signInWithLoginAndPassword(login, password, getDeviceId(context))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ProgressSubscriber<JsonWebToken>(view) {
                    override fun onNext(token: JsonWebToken) {
                        preferences.login = login
                        preferences.jwtToken = token
                        preferences.twilioAccessToken = token.identityToken.getString("tw_access_token")
                        preferences.accessTokenExpireTimeMillis = System.currentTimeMillis() + token.expiresIn * 1000
                        Log.i("log", "refreshToken ${preferences.jwtToken?.refreshToken}")
                        view.goMainActivity()
                    }

                    override fun onError(e: Throwable?) {
                        if(e is HttpException && e.code()==400) {
                            view.showMessage(JSONObject(e.response().errorBody().string()).getString("error_description"))
                        } else {
                            Log.i("log", e.toString())
                        }
                    }
                })
    }

    fun checkForUpdate(){
        updateApi.сheckForUpdate()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ProgressSubscriber<Any>(view) {
                    override fun onNext(any: Any) {
                        when(UpdateStatus.valueOf(any.toString())){
                            UpdateStatus.Available -> view.showAvailableDialog()
                            UpdateStatus.Mandatory -> view.showMandatoryDialog()
                        }
                    }

                    override fun onError(e: Throwable?) {

                    }
                })
    }

    fun onRegisterClick() {
        view.goRegisterActivity()
    }

    fun onRestoreByEmailClick(login: String) {
        resetVisible = false
        view.hideResetForm()
        view.goResetPasswordActivity(login)
    }

    fun onRestoreByPhoneClick(login: String) {
        resetVisible = false
        view.hideResetForm()
        view.showMessage("not implemented") // TODO
    }

    fun onForgotPasswordClick(login: String) {
        if (resetVisible) {
            resetVisible = false
            view.hideResetForm()
        } else if (Patterns.EMAIL_ADDRESS.matcher(login).matches()) {
            onRestoreByEmailClick(login)
        } else if (Patterns.PHONE.matcher(login).matches()) {
            onRestoreByPhoneClick(login)
        } else {
            resetVisible = true
            view.showResetForm()
        }
    }

    fun onCloseRestoreFormClick() {
        resetVisible = false
        view.hideResetForm()
    }
}
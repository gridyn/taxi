package com.blacksea.hahataxi.activities.chat

interface OnDataChatChangeListener {
    fun onDataChatChange()
}
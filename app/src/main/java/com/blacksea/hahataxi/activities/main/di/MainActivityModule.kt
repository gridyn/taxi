package com.blacksea.hahataxi.activities.main.di

import com.blacksea.hahataxi.activities.main.MainActivity
import com.blacksea.hahataxi.activities.main.MainPresenter
import com.blacksea.hahataxi.di.ActivityScope
import com.blacksea.hahataxi.rest.ProfileApi
import com.blacksea.hahataxi.util.PreferencesHelper
import com.tbruyelle.rxpermissions.RxPermissions
import dagger.Module
import dagger.Provides
import pl.charmas.android.reactivelocation.ReactiveLocationProvider

@Module
class MainActivityModule(private val mainActivity: MainActivity) {

    @ActivityScope
    @Provides fun rxPermissions() = RxPermissions(mainActivity)

    @ActivityScope
    @Provides fun presenter(locationProvider: ReactiveLocationProvider, preferences: PreferencesHelper, rxPermissions: RxPermissions, profileApi: ProfileApi) = MainPresenter(mainActivity, locationProvider, preferences, rxPermissions, profileApi)
}
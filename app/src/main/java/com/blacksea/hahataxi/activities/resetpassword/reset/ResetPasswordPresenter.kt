package com.blacksea.hahataxi.activities.resetpassword.reset

import android.content.res.Resources
import android.util.Log
import com.blacksea.hahataxi.rest.PasswordApi
import com.blacksea.hahataxi.subscribers.ProgressSubscriber
import com.blacksea.hahataxi.util.PreferencesHelper
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.adapter.rxjava.HttpException
import rx.android.schedulers.AndroidSchedulers


class ResetPasswordPresenter(private val passwordApi: PasswordApi,
                             private val resources: Resources,
                             private val prefs: PreferencesHelper) {

    companion object {
        val RESET_SEARCH_TERM = "resetSearchTerm";
    }

    lateinit var view: ResetPasswordView

    fun resetPassword(searchTerm: String) {
        if (searchTerm.isEmpty()) {
            view.showMessage("Search term is empty")
            return
        }
        prefs.searchTerm = searchTerm

        val body = RequestBody.create(MediaType.parse("text/plain"), searchTerm)
        passwordApi.resetByEmail(body)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ProgressSubscriber<Response<Void>>(view) {
                    override fun onNext(response: Response<Void>) {

                        if (response.isSuccessful) {
                            view.showSuccessResult()
                        } else {
                            Log.d("happy", "reset result but unsuccess")

                            onError(HttpException(response))
                        }
                    }
                })



    }

    fun setSearchTerm(stringExtra: String?) {
        view.setSearchTerm(stringExtra ?: "")

    }

}



package com.blacksea.hahataxi.activities.login.di

import com.blacksea.hahataxi.activities.login.LoginActivity
import com.blacksea.hahataxi.di.ActivityScope
import com.blacksea.hahataxi.di.AppComponent
import dagger.Component

@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(LoginModule::class))
interface LoginComponent {

    fun inject(loginActivity: LoginActivity)
}
package com.blacksea.hahataxi.activities.voip_call

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.util.DisplayMetrics
import android.view.View
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.activities.BaseActivity
import com.blacksea.hahataxi.activities.voip_call.di.DaggerVoipCallComponent
import com.blacksea.hahataxi.activities.voip_call.di.VoipCallModule
import com.blacksea.hahataxi.di.AppComponent
import com.blacksea.hahataxi.gcm.CancelNotificationBroadcast
import com.blacksea.hahataxi.gcm.GcmListenerService
import com.blacksea.hahataxi.models.CallAction
import com.blacksea.hahataxi.models.CallType
import com.blacksea.hahataxi.rest.ProfileApi
import com.blacksea.hahataxi.rest.ServerConstants
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.call_activity.*
import rx.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class VoipCallActivity : BaseActivity(), VoipCallView {

    @Inject lateinit var presenter: VoipCallPresenter
    @Inject lateinit var picasso: Picasso
    @Inject lateinit var profileApi: ProfileApi

    private var userName: String? = null

    companion object {
        const val EXTRA_CALL_TYPE = "call_type"
        const val EXTRA_CHANNEL_NAME = "channel_name"
        const val EXTRA_USER_NAME = "user_name"
        const val EXTRA_IMG = "image_user"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.call_activity)
        presenter.callType = intent.extras.getSerializable(EXTRA_CALL_TYPE) as CallType
        presenter.attachView()

        btnAcceptCall.setOnClickListener { acceptCall() }

        userName = intent.extras.getString(EXTRA_USER_NAME)
        if (userName != null) {
            tvName.text = userName
            tvAva.text = userName?.substring(0, 2)
        }

        val channelName = intent.extras.getString(EXTRA_CHANNEL_NAME)
        if (channelName != null) {
            presenter.connectChannel(channelName)
        } else {
            showMessage("some trouble")
            finishCall()
        }

        if (presenter.callType == CallType.OUTGOING) {
            btnRejectCall.alpha = 0f
            btnRejectCall.animate().alpha(1f).setDuration(3000).start()
            btnRejectCall.postDelayed({ btnRejectCall.setOnClickListener { presenter.disconnectFromRoom() } }, 3000)
        } else {
            btnRejectCall.setOnClickListener { presenter.disconnectFromRoom() }
        }

        cbMic.setOnCheckedChangeListener { _, _ -> presenter.clickOnMicrophone() }
        cbVolume.setOnCheckedChangeListener { _, _ -> presenter.clickOnVolume() }
        cbBluetooth.setOnClickListener { presenter.clickOnBluetooth(cbBluetooth.isChecked) }

        if (presenter.callType == CallType.INCOMING) {
            flVolume.visibility = View.GONE
            flMic.visibility = View.GONE
        } else if (presenter.callType == CallType.OUTGOING) {
            val metrics = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(metrics)
            btnRejectCall.translationX = -metrics.widthPixels / 2f + resources.getDimension(R.dimen.reject_call_right_space) * 1.5f

            btnAcceptCall.visibility = View.GONE
        }

        if (intent.hasExtra(EXTRA_IMG)) {
            val img = intent.getStringExtra(EXTRA_IMG)
            if (img.isEmpty()) {
                tvAva.visibility = View.VISIBLE
                progress.visibility = View.GONE
            } else {
                picasso.load(ServerConstants.IMAGE_URL + img).into(imgPhoto, object : Callback {
                    override fun onSuccess() {
                        progress.visibility = View.GONE

                    }

                    override fun onError() {
                        progress.visibility = View.GONE
                    }
                })
            }
        } else {
            if (userName != null) {
                profileApi.getAccountPicture(userName!!)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            picasso.load(ServerConstants.SERVER_URL + it.headers().get("Location"))
                                    .into(imgPhoto, object : Callback {
                                        override fun onSuccess() {
                                            progress.visibility = View.GONE

                                        }

                                        override fun onError() {
                                            progress.visibility = View.GONE
                                        }
                                    })
                        }
            }
        }
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    override fun setupActivityComponent(appComponent: AppComponent) {
        DaggerVoipCallComponent.builder()
                .appComponent(appComponent)
                .voipCallModule(VoipCallModule(this, this))
                .build().inject(this)
    }

    override fun connectedTo(name: String?) {
//        tvName.text = name
    }

    override fun getContext(): Context = applicationContext

    override fun finishCall() {
        finish()
    }

    override fun setStatus(status: String) {
        tvStatus.text = status
    }

    override fun acceptCall() {
        enableRejectButton(false)
        if (presenter.callType == CallType.INCOMING) {
            flVolume.visibility = View.VISIBLE
            flMic.visibility = View.VISIBLE
            btnAcceptCall.visibility = View.GONE
            btnRejectCall.animate().translationXBy(-flRoot.width / 2f + resources.getDimension(R.dimen.reject_call_right_space) * 1.5f).start()
            presenter.sendMessage(CallAction.ACCEPT)
        }

        val roomName = intent.extras.getString(EXTRA_CHANNEL_NAME)
        if (roomName != null) {
            presenter.connectToRoom(roomName)
        }
    }

    override fun beep() {
        presenter.mediaPlayer = MediaPlayer.create(this, R.raw.beep)
        presenter.mediaPlayer.start()
    }

    override fun ring() {
        presenter.mediaPlayer = MediaPlayer()
        presenter.mediaPlayer.setDataSource(this, RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE))
        presenter.mediaPlayer.prepare()
        presenter.mediaPlayer.start()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        presenter.disconnectFromRoom()
    }

    override fun showNotificationCall() {
        val intent = Intent(this, ActionCallBroadcast::class.java).setAction(ActionCallBroadcast.ACTION_CALL)

        val acceptPendingIntent = PendingIntent.getBroadcast(this, 0, intent.putExtra(GcmListenerService.EXTRA_CALL_STATE, CallAction.ACCEPT.name), PendingIntent.FLAG_ONE_SHOT)
        val rejectPendingIntent = PendingIntent.getBroadcast(this, 1, intent.putExtra(GcmListenerService.EXTRA_CALL_STATE, CallAction.REJECT.name), PendingIntent.FLAG_ONE_SHOT)

        val notificationCompat = NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.marker_taxi)
                .setContentTitle(userName)
                .setContentText(getString(R.string.notification_call_you))
                .setAutoCancel(true)
                .addAction(R.drawable.ic_call, getString(R.string.notification_accept), acceptPendingIntent)
                .addAction(R.drawable.ic_clear_white_24dp, getString(R.string.notification_reject), rejectPendingIntent)
                .build()

        (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(0, notificationCompat)
    }

    override fun cancelNotification() {
        sendBroadcast(Intent(CancelNotificationBroadcast.ACTION_CANCEL_NOTIFICATION))
    }

    override fun setBluetoothState(state: Boolean) {
        if (state && cbBluetooth.visibility == View.INVISIBLE) {
            cbBluetooth.visibility = View.VISIBLE
        } else if (!state && cbBluetooth.visibility == View.VISIBLE) {
            cbBluetooth.visibility = View.INVISIBLE
        }
    }

    override fun enableRejectButton(isEnable: Boolean) {
        if (isEnable) {
            btnRejectCall.alpha = 1f
            btnRejectCall.setOnClickListener { presenter.disconnectFromRoom() }
        } else {
            btnRejectCall.alpha = .5f
            btnRejectCall.setOnClickListener(null)
        }
    }
}

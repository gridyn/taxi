package com.blacksea.hahataxi.activities.resetpassword.di

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import com.blacksea.hahataxi.activities.resetpassword.reset.ResetPasswordActivity
import com.blacksea.hahataxi.activities.resetpassword.reset.ResetPasswordLinkPresenter
import com.blacksea.hahataxi.activities.resetpassword.reset.ResetPasswordPresenter
import com.blacksea.hahataxi.di.ActivityScope
import com.blacksea.hahataxi.rest.LoginApi
import com.blacksea.hahataxi.rest.PasswordApi
import com.blacksea.hahataxi.rest.ServerConstants
import com.blacksea.hahataxi.util.PreferencesHelper
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.schedulers.Schedulers

@Module
class ResetPasswordModule(private val activity: Activity) {

    @ActivityScope
    @Provides fun resetPasswordPresenter(api: PasswordApi, prefs: PreferencesHelper)
            = ResetPasswordPresenter(api, activity.resources, prefs)

    @ActivityScope
    @Provides fun resetPasswordLinkPresenter(passwordApi: PasswordApi,
                                             loginApi: LoginApi,
                                             prefs: PreferencesHelper,
                                             context: Context)
            = ResetPasswordLinkPresenter(passwordApi, loginApi, activity.resources, prefs, context)

    @ActivityScope
    @Provides fun passwordApi(okHttpBuilder: OkHttpClient.Builder): PasswordApi {
        return Retrofit.Builder()
                .client(okHttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .baseUrl("${ServerConstants.SERVER_URL}/1/")
                .build().create(PasswordApi::class.java)
    }
}



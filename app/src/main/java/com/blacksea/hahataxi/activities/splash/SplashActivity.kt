package com.blacksea.hahataxi.activities.splash

import android.content.Intent
import android.os.Bundle
import com.blacksea.hahataxi.activities.ProgressActivity
import com.blacksea.hahataxi.activities.login.LoginActivity
import com.blacksea.hahataxi.activities.main.MainActivity
import com.blacksea.hahataxi.activities.splash.di.DaggerSplashComponent
import com.blacksea.hahataxi.activities.splash.di.SplashModule
import com.blacksea.hahataxi.di.AppComponent
import javax.inject.Inject

class SplashActivity : ProgressActivity(), SplashMvpView {

    @Inject lateinit var presenter: SplashPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attachView(this, this)
    }

    override fun showMainScreen() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun showLoginScreen() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    override fun setupActivityComponent(appComponent: AppComponent) {
        DaggerSplashComponent.builder()
                .appComponent(appComponent)
                .splashModule(SplashModule(this))
                .build().inject(this)
    }

}
package com.blacksea.hahataxi.activities.resetpassword.reset

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View.GONE
import android.view.View.VISIBLE
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.activities.ProgressActivity
import com.blacksea.hahataxi.activities.main.MainActivity
import com.blacksea.hahataxi.activities.resetpassword.di.DaggerResetPasswordComponent
import com.blacksea.hahataxi.activities.resetpassword.di.ResetPasswordComponent
import com.blacksea.hahataxi.activities.resetpassword.di.ResetPasswordModule
import com.blacksea.hahataxi.di.AppComponent
import kotlinx.android.synthetic.main.activity_resetpassword_link.*
import javax.inject.Inject

class ResetPasswordLinkActivity : ProgressActivity(), ResetPasswordLinkView {
    @Inject lateinit var presenter: ResetPasswordLinkPresenter

    override fun setupActivityComponent(appComponent: AppComponent) {
        DaggerResetPasswordComponent.builder()
                .appComponent(appComponent)
                .resetPasswordModule(ResetPasswordModule(this))
                .build().inject(this)

        presenter.view = this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resetpassword_link)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        resetButton.setOnClickListener {
            presenter.setNewPassword(
                    searchTerm = searchTermField.text.toString(),
                    newPasswordOne = newPasswordOneField.text.toString(),
                    newPasswordTwo = newPasswordTwoField.text.toString())
        }

    }

    override fun onResume() {
        super.onResume()

        presenter.uri = intent.data
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showPasswordForm(searchTerm: String) {
        Log.d("happy", "searchTerm $searchTerm")
        searchTermField.setText(searchTerm)
        newPasswordOneField.setText("")

        badLinkErrorBlock.visibility = GONE
        okBlock.visibility = VISIBLE
    }

    override fun showError() {
        badLinkErrorBlock.visibility = VISIBLE
        okBlock.visibility = GONE
    }

    public override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)
    }

    override fun goMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}



package com.blacksea.hahataxi.activities.chat

import android.app.NotificationManager
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.activities.ProgressActivity
import com.blacksea.hahataxi.activities.chat.di.DaggerChatComponent
import com.blacksea.hahataxi.di.AppComponent
import com.blacksea.hahataxi.models.ChatMessage
import com.blacksea.hahataxi.rest.ProfileApi
import com.blacksea.hahataxi.twilio.channels.ChannelManager
import com.blacksea.hahataxi.util.PreferencesHelper
import com.blacksea.hahataxi.util.getTimeFromTwilio
import com.blacksea.hahataxi.util.watchText
import com.squareup.picasso.Picasso
import com.twilio.chat.*
import kotlinx.android.synthetic.main.activity_chat.*
import rx.android.schedulers.AndroidSchedulers
import java.util.*
import javax.inject.Inject

class ChatActivity : ProgressActivity(), ChannelListener, OnDataChatChangeListener {

    @Inject lateinit var preferences: PreferencesHelper
    @Inject lateinit var channelManager: ChannelManager
    @Inject lateinit var picasso: Picasso
    @Inject lateinit var profileApi: ProfileApi

    private var channel: Channel? = null
    private var channelId: String? = null

    private var messages: Messages? = null
    private lateinit var adapter: ChatAdapter
    private var await = false

    companion object {
        val EXTRA_NAME = "driver_name"
        val EXTRA_CHANNEL = "channel"
        val EXTRA_CHANNEL_ID = "channel_id"
        val EXTRA_IMG = "image_user"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        showRefreshing(R.string.loading)
        preferences.isStartedChat = true

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = intent.extras.getString(EXTRA_NAME)

        channel = intent.getParcelableExtra(EXTRA_CHANNEL)
        channelId = intent.getStringExtra(EXTRA_CHANNEL_ID)

        if (channelId != null) {
            clearNotificationById(channelId!!)
        } else if (channel != null) {
            clearNotificationByName(channel!!.uniqueName)
        }

        if (intent.hasExtra(EXTRA_IMG)) {
            adapter = ChatAdapter(this, this, preferences, intent.getStringExtra(EXTRA_IMG), picasso)
            rvMessages.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            rvMessages.adapter = adapter
            onDataChatChange()
            goToLastMessage()
        } else {
            profileApi.getAccountPicture(intent.extras.getString(EXTRA_NAME))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        adapter = when (it.code()) {
                            302 -> ChatAdapter(this, this, preferences, it.headers().get("Location"), picasso)
                            else -> ChatAdapter(this, this, preferences, "", picasso)
                        }
                        rvMessages.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
                        rvMessages.adapter = adapter
                        onDataChatChange()
                        goToLastMessage()
                    }
        }

        imChangeInput.setOnClickListener { toggleInput() }

        btnHoldToTalk.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                Toast.makeText(this, "Recording started", Toast.LENGTH_SHORT).show()
            } else if (event.action == MotionEvent.ACTION_UP) {
                Toast.makeText(this, "Recording stopped", Toast.LENGTH_SHORT).show()
            }
            false
        }

        edMessage.watchText { channel?.typing() }

        btnSend.setOnClickListener {
            if (edMessage.text.isNotEmpty() && messages != null) {
                await = true
                val message = messages!!.createMessage(edMessage.text.toString().trim())
                adapter.addMessage(ChatMessage(preferences.login!!, edMessage.text.toString(), Date(System.currentTimeMillis())))
                edMessage.text = null
                goToLastMessage()

                messages!!.sendMessage(message, object : StatusListener() {
                    override fun onSuccess() {
                        Log.i("log", "message sent")
                    }

                    override fun onError(errorInfo: ErrorInfo?) {
                        Log.i("log", errorInfo!!.errorText)
                    }
                })
            }
        }

        if (channel != null) {
            runIfChannelNotNull()
        } else if (channelId != null) {
            channelManager.getChannel(channelId!!, {
                channel = it
                if (channel != null) {
                    runIfChannelNotNull()
                } else {
                    Log.i("log", "ChatActivity 102")
                }
            })
        } else throw IllegalStateException("Send channel object or channelId!")
    }

    override fun onDestroy() {
        preferences.isStartedChat = false
        super.onDestroy()
    }

    override fun setupActivityComponent(appComponent: AppComponent) {
        DaggerChatComponent.builder()
                .appComponent(appComponent)
                .build().inject(this)
    }

    private fun runIfChannelNotNull() {
        channel!!.addListener(this)
        channel!!.synchronize(object : CallbackListener<Channel>() {
            override fun onSuccess(p0: Channel?) {
                messages = p0!!.messages
            }
        })

        loadMessages()
    }

    private fun goToLastMessage() = if (adapter.itemCount != 0) rvMessages.smoothScrollToPosition(adapter.itemCount - 1) else Unit

    private fun toggleInput() {
        if (btnHoldToTalk.visibility == View.GONE) {
            btnHoldToTalk.visibility = View.VISIBLE
            edMessage.visibility = View.GONE
            imChangeInput.setImageResource(R.drawable.ic_keyboard)
        } else {
            btnHoldToTalk.visibility = View.GONE
            edMessage.visibility = View.VISIBLE
            imChangeInput.setImageResource(R.drawable.ic_microphone)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDataChatChange() {
        if (adapter.itemCount == 0) {
            tvNoMessage.visibility = View.VISIBLE
        } else {
            tvNoMessage.visibility = View.GONE
        }

        goToLastMessage()
    }

    override fun onSynchronizationChange(p0: Channel?) {
        loadMessages()
    }

    override fun onTypingStarted(member: Member?) {
        tvTyping.visibility = View.VISIBLE
        tvTyping.text = "${intent.extras.getString(EXTRA_NAME)} typing..."
    }

    override fun onTypingEnded(member: Member?) {
        tvTyping.visibility = View.GONE
    }

    override fun onMessageAdd(p0: Message?) {
        if (!await)
            if (preferences.jwtToken!!.isDriver)
                adapter.addMessage(ChatMessage(channel!!.uniqueName!!.split(".")[0], p0!!.messageBody, p0.timeStamp.getTimeFromTwilio()))
            else
                adapter.addMessage(ChatMessage(channel!!.uniqueName!!.split(".")[1], p0!!.messageBody, p0.timeStamp.getTimeFromTwilio()))
        else
            await = false
    }

    private fun loadMessages() {
        if (channel?.messages != null)
            channel?.messages?.getLastMessages(1000, object : CallbackListener<List<Message>>() {
                override fun onSuccess(p0: List<Message>?) {
                    adapter.addMessages(p0!!)
                    hideRefreshing()
                }

                override fun onError(errorInfo: ErrorInfo?) {
                    Log.i("log", errorInfo!!.errorText)
                    hideRefreshing()
                }
            })
    }

    private fun clearNotificationByName(channelName: String) {
        val notification = preferences.lastNotification ?: return
        if (channelName == notification.channelTitle && notification.isShow) {
            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).cancel(0)
            preferences.lastNotification = notification.copy(isShow = false)
        }
    }

    private fun clearNotificationById(channelId: String) {
        val notification = preferences.lastNotification ?: return
        if (channelId == notification.channelId && notification.isShow) {
            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).cancel(0)
            preferences.lastNotification = notification.copy(isShow = false)
        }
    }

    override fun onMemberJoin(p0: Member?) {
        Log.i("log", "onMemberJoin")
    }

    override fun onMessageDelete(p0: Message?) {
        Log.i("log", "onMemberDelete")
    }

    override fun onMemberChange(p0: Member?) {
        Log.i("log", "onMemberChange")
    }

    override fun onMessageChange(p0: Message?) {
        Log.i("log", "onMessageChange")
    }

    override fun onMemberDelete(p0: Member?) {
        Log.i("log", "onMemberDelete")
    }
}

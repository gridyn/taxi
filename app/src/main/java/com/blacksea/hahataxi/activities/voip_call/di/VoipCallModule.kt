package com.blacksea.hahataxi.activities.voip_call.di

import android.app.Activity
import android.support.v4.content.LocalBroadcastManager
import com.blacksea.hahataxi.activities.voip_call.VoipCallPresenter
import com.blacksea.hahataxi.activities.voip_call.VoipCallView
import com.blacksea.hahataxi.di.ActivityScope
import com.blacksea.hahataxi.twilio.channels.ChannelManager
import com.blacksea.hahataxi.util.PreferencesHelper
import com.tbruyelle.rxpermissions.RxPermissions
import dagger.Module
import dagger.Provides

@Module
class VoipCallModule(private val voipCallView: VoipCallView, private val activity: Activity) {

    @ActivityScope
    @Provides fun rxPermissions() = RxPermissions(activity)

    @Provides
    fun presenter(preferencesHelper: PreferencesHelper, channelManager: ChannelManager, localBroadcastManager: LocalBroadcastManager, rxPermissions: RxPermissions): VoipCallPresenter {
        return VoipCallPresenter(voipCallView, preferencesHelper, channelManager, localBroadcastManager, rxPermissions)
    }
}
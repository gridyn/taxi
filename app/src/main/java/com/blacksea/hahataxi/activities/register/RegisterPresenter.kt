package com.blacksea.hahataxi.activities.register

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.net.Uri
import android.util.Log
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.models.JsonWebToken
import com.blacksea.hahataxi.rest.*
import com.blacksea.hahataxi.subscribers.ProgressSubscriber
import com.blacksea.hahataxi.util.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Response
import retrofit2.adapter.rxjava.HttpException
import rx.android.schedulers.AndroidSchedulers
import java.io.File

class RegisterPresenter(private val view: RegisterView,
                        private val registerApi: RegisterApi,
                        private val resources: Resources,
                        private val twilioApi: TwilioApi,
                        private val profileApi: ProfileApi,
                        private val preferences: PreferencesHelper,
                        private val loginApi: LoginApi,
                        private val context: Context) : IStringProvider {

    val licenseRegions = arrayOf("BT", "BP")
    var selectionLicenseRegion: Int = 0

    fun registerPassenger(userName: String, password: String, email: String, phone: String, picture: Uri?) {
        if (checkMainFields(userName, password, email, phone)) {
            registerApi.registerPassenger(userName, password, email, phone)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : ProgressSubscriber<Response<Void>>(view) {
                        override fun onNext(response: Response<Void>) {
                            if (response.isSuccessful) {
                                login(userName, password, picture)
                            } else {
                                onError(HttpException(response))
                            }
                        }

                        override fun onError(e: Throwable?) {
                            if (e is HttpException && e.code() == 400) {
                                view.showMessage(JSONArray(e.response().errorBody().string()).getJSONObject(0).getString("description"))
                            } else {
                                super.onError(e)
                            }
                        }
                    })
        }
    }

    fun registerDriver(userName: String,
                       password: String,
                       license: String,
                       email: String,
                       phone: String,
                       district: String,
                       carManufacturer: String,
                       carModel: String,
                       carBodyStyle: String,
                       maxPassengers: Int,
                       picture: Uri?) {

        val licenseWith = "${licenseRegions[selectionLicenseRegion]} $license"

        if (checkMainFields(userName, password, email, phone) && checkLicenseAndManufacturer(licenseWith, carManufacturer, carModel)) {
            registerApi.registerDriver(userName, password, licenseWith, email, phone, district, carManufacturer, carModel, carBodyStyle, maxPassengers)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : ProgressSubscriber<Response<Void>>(view) {
                        override fun onNext(response: Response<Void>) {
                            if (response.isSuccessful) {
                                login(userName, password, picture)
                            } else {
                                onError(HttpException(response))
                            }
                        }

                        override fun onError(e: Throwable?) {
                            if (e is HttpException && e.code() == 400) {
                                view.showMessage(JSONArray(e.response().errorBody().string()).getJSONObject(0).getString("description"))
                            } else {
                                super.onError(e)
                            }
                        }
                    })
        }
    }


    fun login(login: String, password: String, picture: Uri?) {
        loginApi.signInWithLoginAndPassword(login, password, getDeviceId(context))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ProgressSubscriber<JsonWebToken>(view) {
                    override fun onNext(token: JsonWebToken) {
                        preferences.login = login
                        preferences.jwtToken = token
                        preferences.twilioAccessToken = token.identityToken.getString("tw_access_token")
                        preferences.accessTokenExpireTimeMillis = System.currentTimeMillis() + token.expiresIn * 1000
                        Log.i("log", "refreshToken ${preferences.jwtToken?.refreshToken}")
                        view.goMainActivity()

                        if (picture != null)
                            context.startService(Intent(context, UploadPhotoService::class.java).putExtra(UploadPhotoService.EXTRA_URI, picture))
                    }

                    override fun onError(e: Throwable?) {
                        if (e is HttpException && e.code() == 400) {
                            view.showMessage(JSONObject(e.response().errorBody().string()).getString("error_description"))
                        } else {
                            Log.i("log", e.toString())
                        }
                    }
                })
    }

    private fun updateToken(picture: String) {
        val newToken = preferences.jwtToken
        newToken?.changeValueInIdToken(ServerConstants.KEY_PICTURE, picture)
        preferences.jwtToken = newToken
        Log.i("log", preferences.jwtToken?.identityToken.toString())
    }

    fun checkMainFields(userName: String, password: String, email: String, phone: String): Boolean {
        var isValid = true
        if (userName.isBlank()) {
            view.showErrorWhenUserNameEmpty(R.string.blank_username)
            isValid = false
        } else view.removeErrorOnUserName()

        val passError = ValidationUtil.isValidPassword(password, this)
        if (passError != null) {
            view.showErrorWhenPasswordInvalid(passError)
            isValid = false
        }

        if (email.isNotBlank()) {
            if (!ValidationUtil.isValidEmail(email)) {
                view.showErrorWhenEmailInvalid(R.string.invalid_email)
                isValid = false
            } else view.removeErrorOnEmail()
        }

        if (!ValidationUtil.isValidPhone(phone)) {
            view.showErrorWhenPhoneInvalid(R.string.phone_must_be_in_format)
            isValid = false
        }
        return isValid
    }

    fun checkLicenseAndManufacturer(license: String, carManufacturer: String, carModel: String): Boolean {
        var isValid = true
        if (carManufacturer.isBlank()) {
            isValid = false
            view.showErrorOnManufacturer(R.string.blank_car_manufacturer)
        } else {
            view.removeErrorOnManufacturer()
        }
        if (carModel.isBlank()) {
            isValid = false
            view.showErrorOnCarModel(R.string.blank_car_model)
        } else {
            view.removeErrorOnCarModel()
        }
        if (license.length < 9) {
            isValid = false
            view.showErrorOnLicense(R.string.license_must_contain_number)
        } else view.removeErrorOnLicense()

        return isValid
    }

    override fun getStringRes(resId: Int): String = resources.getString(resId)

    fun getPhotoPart(uri: Uri): MultipartBody.Part {
        val file = File(uri.path)
        return MultipartBody.Part.createFormData("file", file.name, RequestBody.create(MediaType.parse("image/*"), file))
    }
}
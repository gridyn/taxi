package com.blacksea.hahataxi.activities.register

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Selection
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.activities.ProgressActivity
import com.blacksea.hahataxi.activities.login.LoginActivity
import com.blacksea.hahataxi.activities.main.MainActivity
import com.blacksea.hahataxi.activities.register.di.DaggerRegisterComponent
import com.blacksea.hahataxi.activities.register.di.RegisterModule
import com.blacksea.hahataxi.di.AppComponent
import com.blacksea.hahataxi.models.enums.District
import com.blacksea.hahataxi.util.ValidationUtil
import com.jakewharton.rxbinding.widget.RxTextView
import com.soundcloud.android.crop.Crop
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.tbruyelle.rxpermissions.RxPermissions
import io.ghyeok.stickyswitch.widget.StickySwitch
import kotlinx.android.synthetic.main.activity_register.*
import java.io.File
import javax.inject.Inject


class RegisterActivity : ProgressActivity(), RegisterView {

    private var picture: Uri? = null

    @Inject lateinit var presenter: RegisterPresenter
    @Inject lateinit var rxPermissions: RxPermissions

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        ssPassengerOrDriver.onSelectedChangeListener = object : StickySwitch.OnSelectedChangeListener {
            override fun onSelectedChange(direction: StickySwitch.Direction, text: String) {
                llDriverFields.visibility = if (text == "Passenger") View.GONE else View.VISIBLE
            }
        }
        spDistrict.adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
                District.values().map { it -> getString(it.districtResId) })
        spDistrict.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                var b: String = ""
                if (edLicense.text.length > 3) {
                    b = edLicense.text.toString().substring(4, edLicense.text.toString().length).replace(" ", "")
                }
                edLicense.mask = getString(R.string.license_format, District.values()[position].zone.toString()) + " ####"
                edLicense.setText(b)
                Selection.setSelection(edLicense.text, edLicense.text.length)
            }
        }
        spDistrict.setSelection(13)

        btnPhoto.setOnClickListener {
            rxPermissions.request(Manifest.permission.READ_EXTERNAL_STORAGE)
                    .filter { isGranted -> isGranted }
                    .subscribe {
                        Crop.pickImage(this)
                    }
        }
        btnDeletePhoto.setOnClickListener {
            Picasso.with(applicationContext).invalidate(picture)
            picture = null
            imRegPhoto.setImageDrawable(null)
            pictureDeleted()
        }

        RxTextView.textChanges(edMail)
                .map { if (it.isNotBlank()) !ValidationUtil.isValidEmail(it.toString()) else false }
                .distinctUntilChanged()
                .subscribe { next ->
                    run {
                        if (next) {
                            edMail_input_layout.error = presenter.getStringRes(R.string.invalid_email)
                            edMail_input_layout.isErrorEnabled = true
                        } else {
                            edMail_input_layout.isErrorEnabled = false
                        }
                    }
                }


        RxTextView.textChanges(edPhone)
                .map { it.isNotBlank() && !ValidationUtil.isValidPhone(it.toString()) }
                .distinctUntilChanged()
                .subscribe { next ->
                    run {
                        if (next) {
                            edPhone_input_layout.error = presenter.getStringRes(R.string.phone_must_be_in_format)
                            edPhone_input_layout.isErrorEnabled = true
                        } else {
                            edPhone_input_layout.isErrorEnabled = false
                        }
                    }
                }

        RxTextView.textChanges(edPassword)
                .map {
                    if (it.isNotBlank()) ValidationUtil.isValidPassword(it.toString(), presenter)
                    else null
                }
                .distinctUntilChanged()
                .subscribe { next ->
                    run {
                        if ((next != null) && (next.isNotBlank())) {
                            edPassword_input_layout.error = next
                            edPassword_input_layout.isErrorEnabled = true
                        } else {
                            edPassword_input_layout.isErrorEnabled = false
                        }
                    }
                }

        spCarBodyStyle.adapter = TypeOfCarAdapter(this)

        edLicense_input_layout.post {
            npLicense.apply { layoutParams = layoutParams.apply { height = edLicense_input_layout.height } }
            llDriverFields.visibility = View.GONE
        }

        npLicense.displayedValues = presenter.licenseRegions
        npLicense.setOnValueChangedListener { _, _, newVal -> presenter.selectionLicenseRegion = newVal }
    }

    override fun setupActivityComponent(appComponent: AppComponent) {
        DaggerRegisterComponent.builder()
                .appComponent(appComponent)
                .registerModule(RegisterModule(this, this))
                .build().inject(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_registration, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun pictureDeleted() {
        btnPhoto.visibility = View.VISIBLE
        btnDeletePhoto.visibility = View.INVISIBLE
    }

    override fun pictureUploaded() {
        btnPhoto.visibility = View.INVISIBLE
        btnDeletePhoto.visibility = View.VISIBLE
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.menu_confirm -> {
                if (ssPassengerOrDriver.getDirection().name == "LEFT")
                    presenter.registerPassenger(edUsername.text.toString(), edPassword.text.toString(), edMail.text.toString(), edPhone.text.toString(), picture)
                else
                    presenter.registerDriver(edUsername.text.toString(), edPassword.text.toString(), edLicense.text.toString(), edMail.text.toString(), edPhone.text.toString(),
                            spDistrict.selectedItem.toString(), edCarManufacturer.text.toString(), edCarModel.text.toString(),
                            spCarBodyStyle.selectedItem.toString(), edPassengersCount.value, picture)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun loginWithCredentials(login: String, password: String) {
        startActivity(LoginActivity.getStartIntent(this, login, password))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK)
                beginCrop(data.data)
            else if (requestCode == Crop.REQUEST_CROP)
                handleCrop(resultCode, data)
        }
    }

    fun beginCrop(source: Uri) {
        val destFile = File(cacheDir, "cropped.png")
        if (destFile.exists()) destFile.delete()
        val destination = Uri.fromFile(File(cacheDir, "cropped.png"))
        Crop.of(source, destination).asSquare().start(this)
    }

    fun handleCrop(resultCode: Int, result: Intent) {
        if (resultCode == RESULT_OK) {
            this.picture = Crop.getOutput(result)
            Picasso.with(this).load(picture).into(imRegPhoto, object : Callback {
                override fun onSuccess() {
                    pictureUploaded()
                }

                override fun onError() {}

            })
        } else if (resultCode == Crop.RESULT_ERROR) {
            val message = Crop.getError(result).message
            if (!message.isNullOrEmpty()) showMessage(message.orEmpty())
        }
    }

    override fun goMainActivity() {
        startActivity(Intent(this, MainActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        finish()
    }

    override fun showErrorWhenUserNameEmpty(stringId: Int) {
        edUsername_input_layout.error = presenter.getStringRes(stringId)
        edUsername_input_layout.isErrorEnabled = true
    }

    override fun removeErrorOnUserName() {
        edUsername_input_layout.isErrorEnabled = false
    }

    override fun removeErrorOnManufacturer() {
        edCarManufacturer_input_layout.isErrorEnabled = false
    }

    override fun showErrorWhenPhoneInvalid(stringId: Int) {
        edPhone_input_layout.error = presenter.getStringRes(stringId)
        edPhone_input_layout.isErrorEnabled = true
    }

    override fun showErrorWhenEmailInvalid(stringId: Int) {
        edMail_input_layout.error = presenter.getStringRes(stringId)
        edMail_input_layout.isErrorEnabled = true
    }

    override fun showErrorOnManufacturer(stringId: Int) {
        edCarManufacturer_input_layout.error = getString(stringId)
        edCarManufacturer_input_layout.isErrorEnabled = true
    }

    override fun showErrorWhenPasswordInvalid(passError: String?) {
        edPassword_input_layout.error = passError
        edPassword_input_layout.isErrorEnabled = true
    }

    override fun showErrorOnLicense(stringId: Int) {
        edLicense_input_layout.error = getString(stringId)
        edLicense_input_layout.isErrorEnabled = true
    }

    override fun showErrorOnCarModel(stringId: Int) {
        edCarModel_input_layout.error = getString(stringId)
        edCarModel_input_layout.isErrorEnabled = true
    }

    override fun removeErrorOnCarModel() {
        edCarModel_input_layout.isErrorEnabled = false
    }

    override fun removeErrorOnLicense() {
        edLicense_input_layout.isErrorEnabled = false
    }

    override fun removeErrorOnEmail() {
        edMail_input_layout.isErrorEnabled = false
    }
}
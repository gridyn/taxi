package com.blacksea.hahataxi.activities

import android.app.ProgressDialog
import android.os.Bundle
import com.blacksea.hahataxi.ProgressView

abstract class ProgressActivity : BaseActivity(), ProgressView {

    lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        progressDialog = ProgressDialog(this)
    }

    override fun showRefreshing(message: String) {
        progressDialog.setMessage(message)
        progressDialog.setCancelable(false)
        progressDialog.show()
    }

    override fun showRefreshing(stringId: Int) {
        showRefreshing(getString(stringId))
    }

    override fun hideRefreshing() {
        progressDialog.dismiss()
    }
}
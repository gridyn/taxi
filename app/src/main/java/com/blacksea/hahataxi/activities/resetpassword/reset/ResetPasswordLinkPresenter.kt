package com.blacksea.hahataxi.activities.resetpassword.reset

import android.content.Context
import android.content.res.Resources
import android.net.Uri
import android.util.Log
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.models.JsonWebToken
import com.blacksea.hahataxi.rest.LoginApi
import com.blacksea.hahataxi.rest.PasswordApi
import com.blacksea.hahataxi.subscribers.ProgressSubscriber
import com.blacksea.hahataxi.util.PasswordValidator
import com.blacksea.hahataxi.util.PreferencesHelper
import com.blacksea.hahataxi.util.getDeviceId
import retrofit2.Response
import rx.android.schedulers.AndroidSchedulers


class ResetPasswordLinkPresenter(private val passwordApi: PasswordApi,
                                 private val loginApi: LoginApi,
                                 private val resources: Resources,
                                 private val preferences: PreferencesHelper,
                                 private val context: Context) {

    companion object {
        val SCHEME = "hahataxi"
        val HOST = "resetpassword"
        val PARAM_CODE = "code"
    }

    lateinit var view: ResetPasswordLinkView
    var code: String? = null

    var uri: Uri? = null
        set(value) {
            field = value

            try {

                if (SCHEME != value?.scheme) throw BadUriException()
                if (HOST != value?.host) throw BadUriException()

                code = value?.getQueryParameter(PARAM_CODE)
                Log.d("happy", "code $code")

                val searchTerm = preferences.searchTerm
                Log.d("happy", "searchTerm $searchTerm")

                view.showPasswordForm(searchTerm)

            } catch (e: BadUriException) {
                uri = null
                code = null
                view.showError()
            }


        }

    fun setNewPassword(searchTerm: String, newPasswordOne: String, newPasswordTwo: String) {
        Log.d("happy", "$newPasswordOne $newPasswordTwo ${newPasswordOne.equals(newPasswordTwo)}")

        if (searchTerm.isEmpty()) {
            view.showMessage(R.string.search_item_is_empty)
            return
        }

        val sb = StringBuilder()
        PasswordValidator.checkPassword(resources, sb, newPasswordOne)
        if (!sb.isEmpty()) {
            view.showMessage(sb.toString())
            return
        }

        if (!newPasswordOne.equals(newPasswordTwo)) {
            view.showMessage(R.string.passwords_are_not_identical)
            return
        }

        if (code?.isEmpty() ?: false) {
            view.showMessage(R.string.code_is_empty)
            return
        }

        performChange(searchTerm, code!!, newPasswordOne)

    }

    fun performChange(searchTerm: String, resetCode: String, password: String) {
        passwordApi.confirmResetByEmail(searchTerm, resetCode, password)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ProgressSubscriber<Response<Void>>(view) {
                    override fun onNext(response: Response<Void>) {
                        when (response.code()) {
                            200 -> {
                                view.showMessage(R.string.password_changed_successfully)
                                performLogin(searchTerm, password)
                            }
                            400 -> view.showMessage(R.string.no_such_user)
                            else -> view.showMessage(R.string.error_unknown)
                        }
                    }

                    override fun onError(e: Throwable?) {
                        view.showMessage(R.string.error_network)
                    }
                })
    }

    fun performLogin(login: String, password: String) {
        loginApi.signInWithLoginAndPassword(login, password, getDeviceId(context))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ProgressSubscriber<JsonWebToken>(view) {
                    override fun onNext(token: JsonWebToken) {
                        preferences.login = login
                        preferences.jwtToken = token
                        view.goMainActivity()
                    }
                })
    }
}

class BadUriException : RuntimeException()


package com.blacksea.hahataxi.activities.confirm.di

import com.blacksea.hahataxi.activities.confirm.ConfirmationActivity
import com.blacksea.hahataxi.activities.confirm.di.ConfirmationModule
import com.blacksea.hahataxi.di.ActivityScope
import com.blacksea.hahataxi.di.AppComponent
import dagger.Component


@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(ConfirmationModule::class))
interface ConfirmationComponent {

    fun inject(confirmationActivity: ConfirmationActivity)

}
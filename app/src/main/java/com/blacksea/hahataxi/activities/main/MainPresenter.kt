package com.blacksea.hahataxi.activities.main

import android.Manifest
import android.view.MenuItem
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.rest.ProfileApi
import com.blacksea.hahataxi.subscribers.ProgressSubscriber
import com.blacksea.hahataxi.util.PreferencesHelper
import com.google.android.gms.location.LocationRequest
import com.tbruyelle.rxpermissions.RxPermissions
import pl.charmas.android.reactivelocation.ReactiveLocationProvider
import retrofit2.Response
import retrofit2.adapter.rxjava.HttpException
import rx.Observer

class MainPresenter(private val view: MainView, private val locationProvider: ReactiveLocationProvider,
                    private val preferences: PreferencesHelper, private val rxPermissions: RxPermissions,
                    private val profileApi: ProfileApi) {

    fun attachView() {
        preferences.isStartedCall = false
        preferences.isStartedChat = false
    }

    fun navigationDrawerItemClick(item: MenuItem) {
        when(item.itemId) {
            R.id.nav_profile -> view.showMyProfile()
            R.id.nav_map -> view.showMap()
            R.id.nav_chat -> view.showChatList()
            R.id.nav_logout -> {
                stopSignalR()
                preferences.login = null
                preferences.jwtToken = null
                view.goLoginScreen()
            }
        }
    }

    fun getLocationAndStartSignalR() {
        rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION)
                .filter { view.showMessage(it.toString()); it } //is granted
                .flatMap { locationProvider.getUpdatedLocation(LocationRequest()
                        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                        .setNumUpdates(1))
                }
                .first()
                .subscribe {
                    if(preferences.jwtToken!!.isDriver) {
                        view.startDriverSignalR(preferences.jwtToken!!.accessToken, it)
                    } else {
                        view.startPassengerSignalR(preferences.jwtToken!!.accessToken, it)
                    }
                }
    }

    fun stopSignalR() {
        view.stopSignalR()
    }

    fun confirmByScheme(scheme: String) {
        turnOffEmailConfirmationCode()
        confirmEmail(scheme.substringAfter('=')
//                , object : Observer<Response<Void>> {
//            override fun onCompleted() {
//            }
//
//            override fun onNext(t: Response<Void>?) {
//            }
//
//            override fun onError(e: Throwable?) {
//            }
//        }
        )
    }

    fun confirmEmail(queryParameter: String, listener: Observer<Response<Void>>? = null) {
        profileApi.confirmEmail(queryParameter).subscribe {
            object : ProgressSubscriber<Response<Void>>(view) {
                override fun onNext(response: Response<Void>) {
                    if (response.isSuccessful) {
                        preferences.isMailConfirmed = true
                        view.showMessage(R.string.email_succesfully_confirmed)
                        view.onSuccessfulConfirmEmail()
                    } else onError(HttpException(response))
                }
            }
        }
    }

    fun checkForWaitingEmailConfirmationCode() {
        if (preferences.isWaitingForConformationCode) {
            view.showEmailConfirmationPopup()
        }
    }

    fun turnOffEmailConfirmationCode() {
        preferences.isWaitingForConformationCode = false
    }
}

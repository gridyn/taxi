package com.blacksea.hahataxi.activities.chat.di

import com.blacksea.hahataxi.activities.chat.ChatActivity
import com.blacksea.hahataxi.di.ActivityScope
import com.blacksea.hahataxi.di.AppComponent
import dagger.Component

@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class))
interface ChatComponent {
    fun inject(chatActivity: ChatActivity)
}
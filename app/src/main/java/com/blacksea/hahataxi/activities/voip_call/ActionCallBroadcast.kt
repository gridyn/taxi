package com.blacksea.hahataxi.activities.voip_call

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.blacksea.hahataxi.gcm.CancelNotificationBroadcast
import com.blacksea.hahataxi.gcm.GcmListenerService

class ActionCallBroadcast : BroadcastReceiver() {

    companion object {
        const val ACTION_CALL = "com.blacksea.hahataxi.ACTION_CALL"
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.i("log", "ActionCallBroadcast onReceive: " + intent?.getStringExtra(GcmListenerService.EXTRA_CALL_STATE))
        LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(GcmListenerService.EVENT_CALL_STATE)
                .putExtra(GcmListenerService.EXTRA_CALL_STATE, intent?.getStringExtra(GcmListenerService.EXTRA_CALL_STATE)))

        context?.sendBroadcast(Intent(CancelNotificationBroadcast.ACTION_CANCEL_NOTIFICATION))
    }
}
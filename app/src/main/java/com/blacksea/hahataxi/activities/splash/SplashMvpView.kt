package com.blacksea.hahataxi.activities.splash

import com.blacksea.hahataxi.ProgressView

interface SplashMvpView : ProgressView {

    fun showLoginScreen()

    fun showMainScreen()
}
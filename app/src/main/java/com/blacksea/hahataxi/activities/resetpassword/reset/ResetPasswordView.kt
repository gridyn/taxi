package com.blacksea.hahataxi.activities.resetpassword.reset

import com.blacksea.hahataxi.ProgressView

interface ResetPasswordView : ProgressView {
    fun setSearchTerm(searchTerm: String)

    fun showSuccessResult()
}


package com.blacksea.hahataxi.activities.register

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView

import com.blacksea.hahataxi.R

class TypeOfCarAdapter(private val mContext: Context) : ArrayAdapter<String>(mContext, R.layout.type_of_car_adapter_item, mContext.resources.getStringArray(R.array.entries_car_body_styles)) {
    private val mIcons = arrayOf(R.drawable.ic_hatchback, R.drawable.ic_sedan, R.drawable.ic_minivan, R.drawable.ic_suv,
            R.drawable.ic_van, R.drawable.ic_pickup, R.drawable.ic_sportcar, R.drawable.ic_convertible)

    override fun getDropDownView(position: Int, convertView: View?,
                                 parent: ViewGroup): View {
        return getCustomView(position, convertView, parent)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getCustomView(position, convertView, parent)
    }

    fun getCustomView(position: Int, convertView: View?, parent: ViewGroup): View {
        val row = LayoutInflater.from(mContext).inflate(R.layout.type_of_car_adapter_item, parent, false)

        val textCarType = row.findViewById(R.id.text_car_type) as TextView
        val icon = row.findViewById(R.id.image_car_type) as ImageView

        textCarType.text = getItem(position)
        icon.setImageResource(mIcons[position])

        return row
    }
}

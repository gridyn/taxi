package com.blacksea.hahataxi.activities.main

import android.location.Location
import com.blacksea.hahataxi.ProgressView

interface MainView :ProgressView {

    fun showMyProfile()

    fun showMap()

    fun showChatList()

    fun startPassengerSignalR(accessToken: String, location: Location)

    fun startDriverSignalR(accessToken: String, location: Location)

    fun stopSignalR()

    fun goLoginScreen()

    fun onSuccessfulConfirmEmail()

    fun showEmailConfirmationPopup()
}
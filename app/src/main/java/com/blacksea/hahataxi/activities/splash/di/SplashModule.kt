package com.blacksea.hahataxi.activities.splash.di

import com.blacksea.hahataxi.activities.splash.SplashMvpView
import com.blacksea.hahataxi.activities.splash.SplashPresenter
import com.blacksea.hahataxi.di.ActivityScope
import com.blacksea.hahataxi.rest.LoginApi
import com.blacksea.hahataxi.util.PreferencesHelper
import dagger.Module
import dagger.Provides

@Module
class SplashModule(private val mvpView: SplashMvpView) {

    @ActivityScope
    @Provides fun provideSplashPresenter(preferencesHelper: PreferencesHelper, loginApi: LoginApi)
            = SplashPresenter(mvpView, preferencesHelper, loginApi)
}
package com.blacksea.hahataxi.activities.confirm

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.TaxiApplication
import com.blacksea.hahataxi.activities.ProgressActivity
import com.blacksea.hahataxi.activities.confirm.ConfirmationActivity.Constants.ACTION_EMAIL_CONFIRMED
import com.blacksea.hahataxi.activities.confirm.ConfirmationActivity.Constants.ACTION_EMAIL_CONFIRMED_SCHEME_RECEIVED
import com.blacksea.hahataxi.activities.confirm.ConfirmationActivity.Constants.EMAIL_CONFIRMATION_CODE
import com.blacksea.hahataxi.activities.confirm.di.ConfirmationModule
import com.blacksea.hahataxi.activities.confirm.di.DaggerConfirmationComponent
import com.blacksea.hahataxi.activities.main.MainActivity
import com.blacksea.hahataxi.di.AppComponent
import javax.inject.Inject

class ConfirmationActivity : ProgressActivity(), ConfirmationView {

    object Constants {
        const val ACTION_EMAIL_CONFIRMED: String = "com.blacksea.hahataxi.broadcast_receivers.confirmation.ConfirmationBroadcastReceiver.Constants.ACTION_EMAIL_CONFIRMED"
        const val ACTION_EMAIL_CONFIRMED_SCHEME_RECEIVED: String = "com.blacksea.hahataxi.broadcast_receivers.confirmation.ConfirmationBroadcastReceiver.Constants.ACTION_EMAIL_CONFIRMED_SCHEME_RECEIVED"
        const val ACTION_LISTEN_EMAIL_CONFIRMATION: String = "com.blacksea.hahataxi.broadcast_receivers.confirmation.ConfirmationBroadcastReceiver.Constants.ACTION_EMAIL_CONFIRMED"
        const val EMAIL_CONFIRMATION_CODE = "emailcfmcode"
    }

    @Inject lateinit var presenter: ConfirmationPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation)
        val i = Intent(this, MainActivity::class.java)
        i.putExtra(EMAIL_CONFIRMATION_CODE, intent.data.getQueryParameter("emailcfmcode"))
        startActivity(i)
        this.finish()
    }

    override fun setupActivityComponent(appComponent: AppComponent) {
        DaggerConfirmationComponent.builder()
                .appComponent(appComponent)
                .confirmationModule(ConfirmationModule(this))
                .build().inject(this)
    }
}

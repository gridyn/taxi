package com.blacksea.hahataxi.fragments.profile.di

import android.content.ContentResolver
import com.blacksea.hahataxi.di.ActivityScope
import com.blacksea.hahataxi.fragments.profile.ProfilePresenter
import com.blacksea.hahataxi.fragments.profile.ProfileView
import com.blacksea.hahataxi.rest.LoginApi
import com.blacksea.hahataxi.rest.ProfileApi
import com.blacksea.hahataxi.util.PreferencesHelper
import dagger.Module
import dagger.Provides

@Module
class ProfileModule(private val profileView: ProfileView) {

    @ActivityScope
    @Provides fun presenter(preferences: PreferencesHelper, profileApi: ProfileApi, loginApi: LoginApi, contentResolver: ContentResolver): ProfilePresenter {
        return ProfilePresenter(profileView, preferences, profileApi, loginApi, contentResolver)
    }
}
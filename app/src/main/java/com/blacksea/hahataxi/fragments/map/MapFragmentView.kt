package com.blacksea.hahataxi.fragments.map

import com.blacksea.hahataxi.ProgressView
import com.blacksea.hahataxi.models.Driver
import com.blacksea.hahataxi.models.Passenger
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker

interface MapFragmentView : ProgressView {

    fun addPassengerMarker(carRequest: Passenger): Marker

    fun addDriverMarker(driver: Driver): Marker

    fun clearMap()

    fun moveMarker(marker: Marker, toPosition: LatLng)

    fun hidePassengersButton()
}
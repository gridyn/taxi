package com.blacksea.hahataxi.fragments.chat_list

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.activities.chat.ChatActivity
import com.blacksea.hahataxi.di.AppComponent
import com.blacksea.hahataxi.fragments.ProgressFragment
import com.blacksea.hahataxi.fragments.chat_list.di.DaggerChatListComponent
import com.blacksea.hahataxi.rest.ProfileApi
import com.blacksea.hahataxi.twilio.ChatClientManager
import com.blacksea.hahataxi.twilio.channels.ChannelManager
import com.blacksea.hahataxi.twilio.listeners.LoadChannelListener
import com.blacksea.hahataxi.twilio.listeners.TaskCompletionListener
import com.blacksea.hahataxi.util.PreferencesHelper
import com.blacksea.hahataxi.util.TWILIO_VIDEO_UNIQUE
import com.squareup.picasso.Picasso
import com.twilio.chat.*
import kotlinx.android.synthetic.main.fragment_chat_list.*
import java.util.*
import javax.inject.Inject

class ChatListFragment : ProgressFragment(), ChatListAdapter.OnItemClickListener, ChatClientListener {

    @Inject lateinit var chatClientManager: ChatClientManager
    @Inject lateinit var channelManager: ChannelManager
    @Inject lateinit var preferencesHelper: PreferencesHelper
    @Inject lateinit var profileApi: ProfileApi
    @Inject lateinit var picasso: Picasso
    lateinit var chatListAdapter: ChatListAdapter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_chat_list, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showRefreshing(R.string.loading)
        checkTwilioClient()
    }

    fun addChannels(channels: ArrayList<Channel>) {

        channels.filter { it.uniqueName.split(".")[0] == TWILIO_VIDEO_UNIQUE }
                .forEach { channels.remove(it) }

        chatListAdapter = ChatListAdapter(this, channels, preferencesHelper, profileApi, picasso)
        rvChats.adapter = chatListAdapter
        rvChats.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        hideRefreshing()
    }

    private fun checkTwilioClient() {
        if (chatClientManager.getChatClient() == null) {
            initializeClient()
        } else {
            populateChannels()
        }
    }

    fun initializeClient() {
        chatClientManager.connectClient(object : TaskCompletionListener<Void, String> {
            override fun onSuccess(t: Void?) {
                populateChannels()
            }

            override fun onError(u: String?) {
                Log.i("log", "Error: $u")
            }
        })
    }

    fun populateChannels() {
        Log.i("log", "populateChannel")
        channelManager.setChannelListener(this)
        channelManager.populateChannels(LoadChannelListener {
            hideRefreshing()

            addChannels(ArrayList(it))
        })
    }

    override fun setupFragmentComponent(appComponent: AppComponent) {
        DaggerChatListComponent.builder()
                .appComponent(appComponent)
                .build().inject(this)
    }

    override fun onItemClick(channel: Channel, userImg: String) {
        val channelName = channel.uniqueName.split(".")
        startActivity(Intent(context, ChatActivity::class.java)
                .putExtra(ChatActivity.EXTRA_NAME, if (preferencesHelper.jwtToken!!.isDriver) channelName[0] else channelName[1])
                .putExtra(ChatActivity.EXTRA_IMG, userImg)
                .putExtra(ChatActivity.EXTRA_CHANNEL, channel))
    }

    override fun onChannelDelete(p0: Channel?) {
        chatListAdapter.notifyDataSetChanged()
        Log.i("log", "log")
    }

    override fun onToastFailed(p0: ErrorInfo?) {
        Log.i("log", "log")
    }

    override fun onClientSynchronization(p0: ChatClient.SynchronizationStatus?) {
        Log.i("log", "log")
    }

    override fun onToastSubscribed() {
        Log.i("log", "log")
    }

    override fun onChannelInvite(p0: Channel?) {
        Log.i("log", "log")
    }

    override fun onChannelSynchronizationChange(p0: Channel?) {
        Log.i("log", "log")
    }

    override fun onUserInfoChange(p0: UserInfo?, p1: UserInfo.UpdateReason?) {
        Log.i("log", "log")
    }

    override fun onConnectionStateChange(p0: ChatClient.ConnectionState?) {
        Log.i("log", "log")
    }

    override fun onError(p0: ErrorInfo?) {
        Log.i("log", "log")
    }

    override fun onChannelAdd(p0: Channel?) {
        Log.i("log", "log")
    }

    override fun onChannelChange(p0: Channel?) {
        Log.i("log", "log")
    }

    override fun onToastNotification(p0: String?, p1: String?) {
        Log.i("log", "log")
    }
}

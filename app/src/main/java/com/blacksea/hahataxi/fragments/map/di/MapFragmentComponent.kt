package com.blacksea.hahataxi.fragments.map.di

import com.blacksea.hahataxi.di.AppComponent
import com.blacksea.hahataxi.di.FragmentScope
import com.blacksea.hahataxi.fragments.map.MapFragment
import dagger.Component

@FragmentScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(MapFragmentModule::class))
interface MapFragmentComponent {

    fun inject(mapFragment: MapFragment)
}
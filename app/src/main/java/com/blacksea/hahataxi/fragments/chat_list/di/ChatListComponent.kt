package com.blacksea.hahataxi.fragments.chat_list.di

import com.blacksea.hahataxi.di.AppComponent
import com.blacksea.hahataxi.di.FragmentScope
import com.blacksea.hahataxi.fragments.chat_list.ChatListFragment
import dagger.Component

@FragmentScope
@Component(dependencies = arrayOf(AppComponent::class))
interface ChatListComponent {
    fun inject(chatListFragment: ChatListFragment)
}
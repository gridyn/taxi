package com.blacksea.hahataxi.fragments.chat_list

import android.content.Context
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.rest.ProfileApi
import com.blacksea.hahataxi.rest.ServerConstants
import com.blacksea.hahataxi.util.PreferencesHelper
import com.blacksea.hahataxi.util.getTimeFromTwilio
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.twilio.chat.Channel
import com.twilio.chat.ErrorInfo
import com.twilio.chat.StatusListener
import rx.android.schedulers.AndroidSchedulers
import java.text.SimpleDateFormat
import java.util.*


class ChatListAdapter(private val onItemClickListener: OnItemClickListener,
                      private val channels: ArrayList<Channel>,
                      private val preferencesHelper: PreferencesHelper,
                      private val profileApi: ProfileApi,
                      private val picasso: Picasso) : RecyclerView.Adapter<ChatListAdapter.ViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        this.context = parent.context
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_chat, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val interlocutorName = if (preferencesHelper.jwtToken?.isDriver ?: false)
            channels[position].uniqueName.split(".")[0]
        else
            channels[position].uniqueName.split(".")[1]

        profileApi.getAccountPicture(interlocutorName)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    val userImg: String? = it.headers().get("Location")
                    picasso.load(ServerConstants.SERVER_URL + userImg)
                            .into(holder.imgPhoto, object : Callback {
                                override fun onSuccess() {
                                    bind(holder, position, userImg ?: "")
                                }

                                override fun onError() {
                                    bind(holder, position, userImg ?: "")
                                    holder.imgPhoto.visibility = View.GONE
                                }
                            })
                }, { Log.i("log", it.message) })
    }

    private fun bind(holder: ViewHolder, position: Int, userImg: String) {
        holder.progress.visibility = View.GONE
        val channel = channels[position]
        val channelUserData = channel.uniqueName.split(".")
        holder.itemView.setOnLongClickListener { leaveChannel(channel, position) }
        holder.itemView.setOnClickListener { onItemClickListener.onItemClick(channels[position], userImg) }
        if (channel.uniqueName == "andriicsharp")
            holder.tvName.text = channel.uniqueName + "Fail"
        else
            if (preferencesHelper.jwtToken!!.isDriver)
                holder.tvName.text = channelUserData[0]
            else {
                if (channelUserData.size > 1) {
                    holder.tvName.text = channelUserData[1]
                }
            }

        if (holder.tvName.text.length >= 2)
            holder.imPhoto.text = holder.tvName.text.substring(startIndex = 0, endIndex = 2)

        holder.tvTime.text = SimpleDateFormat("dd MMM, kk:mm").format(channel.dateUpdated.getTimeFromTwilio())
    }

    override fun getItemCount(): Int = channels.size


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imPhoto = itemView.findViewById(R.id.imPhoto) as TextView
        var tvName = itemView.findViewById(R.id.tvName) as TextView
        var tvTime = itemView.findViewById(R.id.tvTime) as TextView
        var imgPhoto = itemView.findViewById(R.id.imgPhoto) as ImageView
        var progress = itemView.findViewById(R.id.progress) as ProgressBar
    }

    interface OnItemClickListener {
        fun onItemClick(channel: Channel, userImg: String)
    }

    private fun leaveChannel(channel: Channel, position: Int): Boolean {
        AlertDialog.Builder(context)
                .setMessage(R.string.chat_list_delete_channel)
                .setNegativeButton(context.getString(R.string.no)) { dialog, _ -> dialog.dismiss() }
                .setPositiveButton(context.getString(R.string.yes)) { _, _ ->
                    channel.destroy(object : StatusListener() {
                        override fun onSuccess() {
                            channels.removeAt(position)
                            Toast.makeText(context, "channel was removed", Toast.LENGTH_SHORT).show()
                            notifyDataSetChanged()
                        }

                        override fun onError(errorInfo: ErrorInfo?) {
                            Log.i("log", "${errorInfo!!.errorCode} ${errorInfo.errorText}")
                        }
                    })
                }
                .create()
                .show()
        return true
    }
}

package com.blacksea.hahataxi.fragments.map.di

import com.blacksea.hahataxi.di.FragmentScope
import com.blacksea.hahataxi.fragments.map.MapFragment
import com.blacksea.hahataxi.fragments.map.MapFragmentView
import com.blacksea.hahataxi.fragments.map.MapPresenter
import com.blacksea.hahataxi.twilio.channels.ChannelManager
import com.blacksea.hahataxi.util.PreferencesHelper
import com.blacksea.hahataxi.util.SignalRBus
import com.tbruyelle.rxpermissions.RxPermissions
import dagger.Module
import dagger.Provides
import pl.charmas.android.reactivelocation.ReactiveLocationProvider

@Module
class MapFragmentModule(private val mapFragment: MapFragment) {

    @FragmentScope
    @Provides fun rxPermission(): RxPermissions {
        return RxPermissions(mapFragment.activity)
    }

    @FragmentScope
    @Provides fun presenter(locationProvider: ReactiveLocationProvider, rxPermissions: RxPermissions, rxBus: SignalRBus, channelManager: ChannelManager, preferencesHelper: PreferencesHelper): MapPresenter {
        return MapPresenter(mapFragment as MapFragmentView, locationProvider, rxPermissions, rxBus, channelManager, preferencesHelper)
    }
}
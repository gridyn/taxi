package com.blacksea.hahataxi.fragments

import android.app.ProgressDialog
import android.os.Bundle
import com.blacksea.hahataxi.ProgressView

abstract class ProgressFragment : BaseFragment(), ProgressView {

    lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        progressDialog = ProgressDialog(context)
    }

    override fun showRefreshing(message: String) {
        progressDialog.setMessage(message)
        progressDialog.setCancelable(false)
        progressDialog.show()
    }

    override fun showRefreshing(stringId: Int) {
        showRefreshing(getString(stringId))
    }

    override fun hideRefreshing() {
        progressDialog.dismiss()
    }
}
package com.blacksea.hahataxi.fragments.edit_email

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.R.string.*
import com.blacksea.hahataxi.util.ValidationUtil
import com.jakewharton.rxbinding.widget.RxCompoundButton
import com.jakewharton.rxbinding.widget.RxTextView
import kotlinx.android.synthetic.main.edit_email.*
import rx.Observable
import android.widget.Toast
import com.blacksea.hahataxi.di.AppComponent
import com.blacksea.hahataxi.fragments.ProgressFragment
import com.blacksea.hahataxi.fragments.profile.ProfileFragment
import com.blacksea.hahataxi.fragments.profile.ProfilePresenter
import com.blacksea.hahataxi.fragments.profile.di.DaggerProfileComponent
import com.blacksea.hahataxi.fragments.profile.di.ProfileModule
import com.blacksea.hahataxi.rest.ProfileApi
import com.blacksea.hahataxi.util.IStringProvider
import com.jakewharton.rxbinding.view.RxView
import javax.inject.Inject


class EditEmailFragment : ProgressFragment(), IStringProvider {

    @Inject lateinit var profileApi: ProfileApi

    override fun setupFragmentComponent(appComponent: AppComponent) {
        DaggerEditEmailComponent.builder()
                .appComponent(appComponent)
                .build().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.edit_email, container!!, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        RxTextView.textChanges(new_email)
                .map { it.isNotBlank() && !ValidationUtil.isValidEmail(it) }
                .subscribe {
                    if (it) {
                        new_email_layout.error = getString(invalid_email)
                        new_email_layout.isErrorEnabled = true
                    } else {
                        new_email_layout.isErrorEnabled = false
                    }
                }
        Observable.combineLatest(RxTextView.textChanges(new_email),
                RxTextView.textChanges(new_email_password),
                { email, password ->
                        ValidationUtil.isValidPassword(password.toString(), this) == null &&
                        ValidationUtil.isValidEmail(email.toString())
                } )
                .subscribe {
                    change_email_button.isEnabled = it
                    if (it) {
                        new_email_password.setOnEditorActionListener { v, actionId, event ->
                            var handled = false
                            if (actionId == EditorInfo.IME_ACTION_DONE) {
                                val errors = ValidationUtil.isValidPassword(new_email_password.text.toString(), this)
                                if (errors == null) {
                                    submit(new_email.text.toString())
                                } else {
                                    showMessage(errors)
                                }
                                handled = true
                            }
                            handled
                        }
                    } else {
                        new_email_password.setOnEditorActionListener { v, actionId, event ->
                            var handled = false
                            if (actionId == EditorInfo.IME_ACTION_DONE) {
                                Toast.makeText(context, error_emails, Toast.LENGTH_SHORT).show()
                                handled = true
                            }
                            handled
                        }
                    }
                }

        change_email_button.setOnClickListener { submit(new_email.text.toString()) }
        new_email_password.setOnEditorActionListener { v, actionId, event ->
            var handled = false
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                val errors = ValidationUtil.isValidPassword(new_email_password.text.toString(), this)
                if (errors == null) {
                    submit(new_email.text.toString())
                } else {
                    showMessage(errors)
                }
                handled = true
            }
            handled
        }
    }

    override fun getStringRes(resId: Int): String {
        return context.getString(resId)
    }


    private fun submit(newEmail: String) {
        if (ValidationUtil.isValidPassword(new_email_password.text.toString(), this) == null) {
            profileApi.changeEmail(new_email_password.text.toString(), newEmail)
                    .subscribe {
                        if (it.isSuccessful) {
                            showMessage(email_changed_succesfuly)
                            fragmentManager.beginTransaction().replace(R.id.fragment_container, ProfileFragment()).commit()
                        } else {
                            if (it.code() == 304) {
                                showMessage(R.string.new_email_equals_old_one)
                            }
                        }
                    }
        }
    }
}
package com.blacksea.hahataxi.fragments.edit_email

import com.blacksea.hahataxi.di.AppComponent
import com.blacksea.hahataxi.di.FragmentScope
import dagger.Component

@FragmentScope
@Component(dependencies = arrayOf(AppComponent::class))
interface EditEmailComponent {

    fun inject(editEmailFragment: EditEmailFragment)
}
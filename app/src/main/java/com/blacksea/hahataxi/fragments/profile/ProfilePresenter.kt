package com.blacksea.hahataxi.fragments.profile

import android.content.ContentResolver
import android.net.Uri
import android.os.Handler
import android.provider.Settings
import android.util.Base64
import android.util.Log
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.models.JsonWebToken
import com.blacksea.hahataxi.rest.LoginApi
import com.blacksea.hahataxi.rest.ProfileApi
import com.blacksea.hahataxi.rest.ServerConstants
import com.blacksea.hahataxi.subscribers.ProgressSubscriber
import com.blacksea.hahataxi.util.PreferencesHelper
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Response
import retrofit2.adapter.rxjava.HttpException
import rx.android.schedulers.AndroidSchedulers
import java.io.File
import java.nio.charset.Charset

class ProfilePresenter(private val view: ProfileView, private val preferences: PreferencesHelper, private val profileApi: ProfileApi,
                       private val loginApi: LoginApi, private val contentResolver: ContentResolver) {


    fun showProfile() {
        try {
            Log.i("log", "showProfile: ${preferences.jwtToken}")
            val userData = JSONObject(Base64.decode(preferences.jwtToken?.idToken?.split('.')!![1], Base64.DEFAULT).toString(Charset.defaultCharset()))
            if (userData.has(ServerConstants.KEY_NAME)) view.setNickname(userData.getString(ServerConstants.KEY_NAME))
            if (userData.has(ServerConstants.KEY_PICTURE)) view.setAvatar(userData.getString(ServerConstants.KEY_PICTURE)) else view.setAvatar("")
            if (userData.has(ServerConstants.KEY_EMAIL)) view.setEmail(userData.getString(ServerConstants.KEY_EMAIL))
            if (userData.has(ServerConstants.KEY_PHONE)) view.setPhone(userData.getString(ServerConstants.KEY_PHONE))
            if (preferences.isMailConfirmed) view.setEmailConfirmed()
        } catch (e: IndexOutOfBoundsException) {
            Handler().postDelayed(this::showProfile, 1000)
        }
    }

    fun uploadPhoto(uri: Uri) {
        val file = File(uri.path)
        val imagePart = MultipartBody.Part.createFormData("file", file.name, RequestBody.create(MediaType.parse("image/*"), file))


        profileApi.postAccountPicture(imagePart)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ProgressSubscriber<Response<Void>>(view) {
                    override fun onNext(response: Response<Void>) {
                        if (response.isSuccessful) {
                            view.showMessage(R.string.picture_upload_success)
                            view.setAvatar(uri)

                            updateToken()
                        }
                        else onError(HttpException(response))
                    }
                })
    }

    /**
     * Login for update token
     */
    private fun updateToken() {
        val progressSubscriber = object : ProgressSubscriber<JsonWebToken>(view) {
            override fun onNext(token: JsonWebToken) {
                preferences.jwtToken = token
            }

            override fun onError(e: Throwable?) {
                view.showMessage(e?.message!!)
            }
        }

        loginApi.signInWithRefreshToken(preferences.jwtToken?.refreshToken,
                Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID))
                .observeOn(AndroidSchedulers.mainThread()).subscribe(progressSubscriber)
    }

    fun deletePhoto() {
        profileApi.deleteAccountPicture()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : ProgressSubscriber<Response<Void>>(view) {
                    override fun onNext(response: Response<Void>) {
                        Log.i("log", Gson().toJson(response))
                        if (response.isSuccessful) {
                            view.showMessage(R.string.picture_deleted_success)
                            view.pictureDeleted()
                            view.removeCroppedImage()
                            updateToken()
                        }
                        else onError(HttpException(response))
                    }
                })
    }

    fun sendEmailConfirmation() {
        profileApi.sendEmailConfirmation()
                .subscribe(object : ProgressSubscriber<Response<Void>>(view) {
                    override fun onNext(response: Response<Void>) {
                        if (response.code() == 200) {
                            view.notifyEmailConformation()
                            preferences.isWaitingForConformationCode = true
                        } else if (response.code() == 304) {
                            preferences.isMailConfirmed = true
                            preferences.isWaitingForConformationCode = false
                            view.setEmailConfirmed()
                        }
                        else onError(HttpException(response))
                    }
                } )
    }
}
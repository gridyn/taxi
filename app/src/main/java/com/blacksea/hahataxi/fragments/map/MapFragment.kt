package com.blacksea.hahataxi.fragments.map

import android.Manifest
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.di.AppComponent
import com.blacksea.hahataxi.fragments.ProgressFragment
import com.blacksea.hahataxi.fragments.map.di.DaggerMapFragmentComponent
import com.blacksea.hahataxi.fragments.map.di.MapFragmentModule
import com.blacksea.hahataxi.models.Driver
import com.blacksea.hahataxi.models.Passenger
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.tbruyelle.rxpermissions.RxPermissions
import kotlinx.android.synthetic.main.fragment_map.*
import javax.inject.Inject

class MapFragment : ProgressFragment(), MapFragmentView {

    val START_ZOOM_AMOUNT: Float = 15F

    @Inject lateinit var presenter: MapPresenter
    @Inject lateinit var rxPermissions: RxPermissions

    private lateinit var mapView: MapView
    private var googleMap: GoogleMap? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater?.inflate(R.layout.fragment_map, container, false)
        mapView = rootView?.findViewById(R.id.mapView) as MapView
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync {
            googleMap = it
            googleMap!!.mapType = GoogleMap.MAP_TYPE_HYBRID
            rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION)
                    .filter { isGranted -> isGranted }
                    .subscribe {
                        googleMap!!.isMyLocationEnabled = true
                        presenter.getLastLocation()?.subscribe { initMap(it) }
                    }
            presenter.fillMapAndSubscribeOnUpdates()
            googleMap!!.setOnMarkerClickListener { presenter.onMarkerClick(context, it); false }
        }
        return rootView
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView()
        btnHidePassenger.setOnClickListener { presenter.hidePassenger() }
    }

    override fun hidePassengersButton() {
        btnHidePassenger.visibility = View.GONE
    }

    private fun initMap(latlng: LatLng) {
        googleMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, START_ZOOM_AMOUNT))

    }

    override fun setupFragmentComponent(appComponent: AppComponent) {
        DaggerMapFragmentComponent.builder()
                .appComponent(appComponent)
                .mapFragmentModule(MapFragmentModule(this))
                .build().inject(this)
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun addPassengerMarker(carRequest: Passenger): Marker {
        val markerOptions = MarkerOptions()
                .position(carRequest.location)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.marker_passenger))
        return googleMap!!.addMarker(markerOptions)
    }

    override fun addDriverMarker(driver: Driver): Marker {
        val markerOptions = MarkerOptions()
                .position(driver.location)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.marker_taxi))
        return googleMap!!.addMarker(markerOptions)
    }

    override fun clearMap() {
        googleMap?.clear()
    }

    override fun moveMarker(marker: Marker, toPosition: LatLng) {
        if(googleMap != null) {
            val handler = Handler()
            val startTime = SystemClock.uptimeMillis()
            val duration = 500
            val interpolator = LinearInterpolator()
            val fromPosition = googleMap!!.projection.fromScreenLocation(googleMap!!.projection.toScreenLocation(marker.position))
            handler.post(object : Runnable {
                override fun run() {
                    val elapsed = SystemClock.uptimeMillis() - startTime
                    val t = interpolator.getInterpolation(elapsed.toFloat() / duration)
                    val lat = t * toPosition.latitude + (1 - t) * fromPosition.latitude
                    val lng = t * toPosition.longitude + (1 - t) * fromPosition.longitude
                    marker.position = LatLng(lat, lng)
                    if (t < 1f) {
                        handler.postDelayed(this, 16)
                    }
                }
            })
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }
}
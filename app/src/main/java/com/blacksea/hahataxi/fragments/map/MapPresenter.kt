package com.blacksea.hahataxi.fragments.map

import android.content.Context
import com.blacksea.hahataxi.models.Driver
import com.blacksea.hahataxi.models.Passenger
import com.blacksea.hahataxi.twilio.channels.ChannelManager
import com.blacksea.hahataxi.util.PreferencesHelper
import com.blacksea.hahataxi.util.SignalRBus
import com.blacksea.hahataxi.view.CarRequestPopupDialog
import com.blacksea.hahataxi.view.DriverPopupDialog
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.tbruyelle.rxpermissions.RxPermissions
import pl.charmas.android.reactivelocation.ReactiveLocationProvider
import rx.Observable
import rx.android.schedulers.AndroidSchedulers

class MapPresenter(private val view: MapFragmentView, private val locationProvider: ReactiveLocationProvider,
                   private val rxPermissions: RxPermissions, private val bus: SignalRBus, private val channelManager: ChannelManager,
                   private val preferencesHelper: PreferencesHelper) {

    val carRequestsMarkers = mutableMapOf<Marker, Passenger>()
    val driverMarkers = mutableMapOf<Marker, Driver>()

    val savedAfterDeleteCarRequestMarker = mutableMapOf<Marker, Passenger>()
    var hidePassenger = false

    fun attachView() {
        if (preferencesHelper.jwtToken?.isDriver == false) {
            view.hidePassengersButton()
        }
    }

    fun fillMapAndSubscribeOnUpdates() {
        bus.matchedDrivers.forEach { driverMarkers.put(view.addDriverMarker(it), it) }
        bus.matchedPassengers.forEach { carRequestsMarkers.put(view.addPassengerMarker(it), it) }
        bus.matchedDriverBus.observeOn(AndroidSchedulers.mainThread()).subscribe { driverMarkers.put(view.addDriverMarker(it), it) }
        bus.unmatchedDriverBus.observeOn(AndroidSchedulers.mainThread()).subscribe { id -> val marker = driverMarkers.filter { it.value.id == id }.keys.first(); driverMarkers.remove(marker); marker.remove() }
        bus.matchedPassengerBus.observeOn(AndroidSchedulers.mainThread()).subscribe { carRequestsMarkers.put(view.addPassengerMarker(it), it) }
        bus.unmatchedPassengerBus.observeOn(AndroidSchedulers.mainThread()).subscribe { id -> val marker = carRequestsMarkers.filter { it.value.id == id }.keys.first(); carRequestsMarkers.remove(marker); marker.remove() }
        bus.driverMovedBus.observeOn(AndroidSchedulers.mainThread()).subscribe { moveEvent ->
            val marker = driverMarkers.filter { it.value.id == moveEvent.id }.keys.first()
            driverMarkers[marker] = driverMarkers[marker]!!.copy(location = moveEvent.location, distanceInMeters = moveEvent.distanceInMeters)
            view.moveMarker(marker, moveEvent.location)
        }
        bus.passengerMovedBus.observeOn(AndroidSchedulers.mainThread()).subscribe { moveEvent ->
            val marker = carRequestsMarkers.filter { it.value.id == moveEvent.id }.keys.first()
            carRequestsMarkers[marker] = carRequestsMarkers[marker]!!.copy(location = moveEvent.location, distanceInMeters = moveEvent.distanceInMeters)
            view.moveMarker(marker, moveEvent.location)
        }
        bus.signalRStoppedBus.observeOn(AndroidSchedulers.mainThread()).subscribe { view.clearMap(); carRequestsMarkers.clear(); driverMarkers.clear() }


    }

    fun onMarkerClick(context: Context, marker: Marker) {
        val carRequest = carRequestsMarkers[marker]
        if(carRequest != null) {
            CarRequestPopupDialog(context, rxPermissions, carRequest, channelManager, preferencesHelper).show()
            return
        }
        val driver = driverMarkers[marker]
        if(driver != null) {
            DriverPopupDialog(context, rxPermissions, driver, channelManager, preferencesHelper).show()
        }
    }

    fun  getLastLocation(): Observable<LatLng>? {
        val locationObservable = locationProvider.lastKnownLocation
        return locationObservable?.map { LatLng(it.latitude, it.longitude)}
    }

    fun hidePassenger() {
        if (!hidePassenger) {
            savedAfterDeleteCarRequestMarker.putAll(carRequestsMarkers)
            bus.sendUnmatchedAllPassenger()
        } else {
            savedAfterDeleteCarRequestMarker
                    .forEach { bus.sendMatchedPassenger(it.value) }
            savedAfterDeleteCarRequestMarker.clear()
        }
        hidePassenger = !hidePassenger
    }
}
package com.blacksea.hahataxi.fragments.profile

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.GestureDetectorCompat
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.blacksea.hahataxi.R
import com.blacksea.hahataxi.activities.main.MainActivity
import com.blacksea.hahataxi.di.AppComponent
import com.blacksea.hahataxi.fragments.ProgressFragment
import com.blacksea.hahataxi.fragments.edit_email.EditEmailFragment
import com.blacksea.hahataxi.fragments.profile.di.DaggerProfileComponent
import com.blacksea.hahataxi.fragments.profile.di.ProfileModule
import com.blacksea.hahataxi.rest.ServerConstants
import com.soundcloud.android.crop.Crop
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile.*
import java.io.File
import javax.inject.Inject


class ProfileFragment : ProgressFragment(), ProfileView {

    @Inject lateinit var presenter: ProfilePresenter

    @Inject lateinit var picasso: Picasso

    private var receiver: BroadcastReceiver? = null

    private val mailGestureDetector = lazy { onViewGesture(swMail) }
    private val phoneGestureDetector = lazy { onViewGesture(swPhone) }

    private val REQUEST_PERMISSION: Int = 1

    private val imageCallBack = object : Callback {
        override fun onSuccess() {
            progress?.visibility = View.GONE
        }

        override fun onError() {
            progress?.visibility = View.GONE
            imPhoto.setImageResource(R.drawable.contact)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as MainActivity).getConfirmCodeValue()?.let { receivedConfirmationCode(it) }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_profile, container!!, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        swMailWrapper.post { swMailWrapper.translationX = swMailWrapper.width.toFloat() }
        swPhoneWrapper.post { swPhoneWrapper.translationX = swPhoneWrapper.width.toFloat() }
        swMail.setOnTouchListener { _, event -> mailGestureDetector.value.onTouchEvent(event) }
        swPhone.setOnTouchListener { _, event -> phoneGestureDetector.value.onTouchEvent(event) }

        presenter.showProfile()
        profilePhoto.setOnClickListener {
            //Crop.pickImage(activity)
            startActivityForResult(Intent(Intent.ACTION_PICK).setType("image/*"), Crop.REQUEST_PICK)
        }
        deleteProfilePhoto.setOnClickListener {
            presenter.deletePhoto()
        }

        btnVerifyMail.setOnClickListener {
            showMessage(R.string.check_your_email_for_conformation)
            presenter.sendEmailConfirmation()
        }

        btnEditMail.setOnClickListener {
            fragmentManager.beginTransaction().replace(R.id.fragment_container, EditEmailFragment(), "Edit Email").commit()
        }
    }

    private fun onViewGesture(view: View): GestureDetectorCompat {
        return GestureDetectorCompat(context, object : GestureListener {
            override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {
                val sensitvity = 50f
                if (e1 != null && e2 != null) {
                    if (e1.x - e2.x > sensitvity) {
                        // left
                        if (view.id == R.id.swMail) {
                            swMailWrapper.animate().translationX(0f)
                            llMail.animate().translationX(-swMailWrapper.width.toFloat()).start()
                        } else if (view.id == R.id.swPhone) {
                            swPhoneWrapper.animate().translationX(0f)
                            llPhone.animate().translationX(-swPhoneWrapper.width.toFloat()).start()
                        }

                    } else if (e2.x - e1.x > sensitvity) {
                        // right
                        if (view.id == R.id.swMail) {
                            swMailWrapper.animate().translationX(swMail.width.toFloat())
                            llMail.animate().translationX(0f).start()
                        } else if (view.id == R.id.swPhone) {
                            swPhoneWrapper.animate().translationX(swPhone.width.toFloat())
                            llPhone.animate().translationX(0f).start()
                        }
                    }
                }

                return true
            }
        })

    }

    private fun receivedConfirmationCode(code: String) {
        (activity as MainActivity).presenter.confirmEmail(code)
    }


    override fun notifyEmailConformation() {

    }

    override fun onSuccessfulConfirmEmail() {
        onEmailConfirmed()
    }

    var activityResultData: Uri? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if (requestCode == Crop.REQUEST_PICK && resultCode == AppCompatActivity.RESULT_OK) {
                activityResultData = data.data
                askPermission()
                //context.grantUriPermission(context.packageName, data.data, Intent.FLAG_GRANT_READ_URI_PERMISSION)
            } else if (requestCode == Crop.REQUEST_CROP)
                handleCrop(resultCode, data)
        }
    }

    private fun askPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    REQUEST_PERMISSION)
            return
        } else {
            beginCrop(activityResultData!!)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSION) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                beginCrop(activityResultData!!)
                // Permission granted.
            } else {
                // User refused to grant permission.
            }
        }
    }

    override fun setupFragmentComponent(appComponent: AppComponent) {
        DaggerProfileComponent.builder()
                .appComponent(appComponent)
                .profileModule(ProfileModule(this))
                .build().inject(this)
    }

    override fun setEmail(email: String) {
        tvMail.text = email
    }

    override fun setPhone(phone: String) {
        tvPhone.text = phone
    }

    override fun setNickname(nickName: String) {
        tvNickname.text = nickName
    }

    override fun setAvatar(avatarUrl: String) {
        if (avatarUrl.isNotBlank()) {
            picasso.load("${ServerConstants.IMAGE_URL}$avatarUrl").fit().into(imPhoto, imageCallBack)
            pictureUploaded()
        } else {
            imageCallBack.onError()
        }
    }

    override fun setAvatar(avatarUrl: Uri) {
        picasso.load(avatarUrl).fit().into(imPhoto, imageCallBack)
        pictureUploaded()
    }

    override fun pictureDeleted() {
        picasso.load(ServerConstants.DEFAULT_IMAGE_URI).fit().into(imPhoto, imageCallBack)
        profilePhoto.visibility = View.VISIBLE
        deleteProfilePhoto.visibility = View.GONE
    }

    override fun pictureUploaded() {
        profilePhoto.visibility = View.GONE
        deleteProfilePhoto.visibility = View.VISIBLE
    }

    fun beginCrop(source: Uri) {
        val destFile = File(activity.cacheDir, "cropped.png")
        if (destFile.exists()) destFile.delete()
        val destination = Uri.fromFile(File(activity.cacheDir, "cropped.png"))
        Crop.of(source, destination).asSquare().start(activity, this)
    }

    fun handleCrop(resultCode: Int, result: Intent) {
        if (resultCode == AppCompatActivity.RESULT_OK) {
            val uri = Crop.getOutput(result)
            presenter.uploadPhoto(uri)
        } else if (resultCode == Crop.RESULT_ERROR) {
            val message = Crop.getError(result).message
            if (!message.isNullOrEmpty()) showMessage(message.orEmpty())
        }
    }

    private fun onEmailConfirmed() {
        email_conformation_status.text = getString(R.string.email_confirmed_status_yes)
        email_conformation_status.setTextColor(R.color.email_confirmed_status_yes)
        btnVerifyMail.setBackgroundColor(R.color.button_confirmed_color)
        btnEditMail.setOnClickListener { showMessage(R.string.allready_confirmed_message) }
    }

    override fun onDestroy() {
        super.onDestroy()
        receiver?.let { context.unregisterReceiver(receiver!!) }
    }

    override fun setEmailConfirmed() {
        onEmailConfirmed()
    }

    override fun removeCroppedImage() {
        val destFile = File(activity.cacheDir, "cropped.png")
        if (destFile.exists()) destFile.delete()
    }
}

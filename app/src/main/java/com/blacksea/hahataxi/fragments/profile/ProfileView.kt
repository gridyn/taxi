package com.blacksea.hahataxi.fragments.profile

import android.net.Uri
import com.blacksea.hahataxi.ProgressView

interface ProfileView :ProgressView {

    fun setEmail(email: String)

    fun setPhone(phone: String)

    fun setAvatar(avatarUrl: Uri)

    fun setAvatar(avatarUrl: String)

    fun setNickname(nickName: String)

    fun pictureDeleted()

    fun pictureUploaded()

    fun notifyEmailConformation()

    fun setEmailConfirmed()
    fun onSuccessfulConfirmEmail()

    fun removeCroppedImage()
}
package com.blacksea.hahataxi.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.Toast
import com.blacksea.hahataxi.BaseView
import com.blacksea.hahataxi.TaxiApplication
import com.blacksea.hahataxi.activities.login.LoginActivity
import com.blacksea.hahataxi.di.AppComponent

abstract class BaseFragment : Fragment(), BaseView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupFragmentComponent(TaxiApplication[context].appComponent)
    }

    abstract fun setupFragmentComponent(appComponent: AppComponent)

    override fun showMessage(stringId: Int) {
        showMessage(getString(stringId))
    }

    override fun showMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun startActivityAfterTokenExpiration() {
        startActivity(Intent(context, LoginActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
    }
}
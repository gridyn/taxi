package com.blacksea.hahataxi.fragments.profile.di

import com.blacksea.hahataxi.di.ActivityScope
import com.blacksea.hahataxi.di.AppComponent
import com.blacksea.hahataxi.di.FragmentScope
import com.blacksea.hahataxi.fragments.profile.ProfileFragment
import dagger.Component

@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(ProfileModule::class))
interface ProfileComponent {

    fun inject(profileFragment: ProfileFragment)
}